#ifndef HARDWARE_INCLUDE_HEATLOCK_H
#define HARDWARE_INCLUDE_HEATLOCK_H

#include <stdint.h>

#include <hardware_status.h>

namespace wer {
namespace hal {

enum class HeatlockStatus {
  NONINITIALIZED = 0,
  INITIALIZED,
  CONNECTED,
  DISCONNECTED
};
/**
 * Heatlock class is the interface for any type of heatlock.
 */
class Heatlock {
 public:
  /**
   * [open function should open the heatlock for mosquitos to get in]
   */
  virtual void open() = 0;
  /**
   * [close function should close the door after mosquitos are loaded inside a
   * heatlock]
   */
  virtual void close() = 0;
  /**
   * [release is releasing mosquitos from a heatlock]
   */
  virtual void release() = 0;

  virtual ~Heatlock(){};

 protected:
  HardwareStatus status_;
};
}
}

#endif  // HARDWARE_INCLUDE_HEATLOCK_H