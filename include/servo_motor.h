#ifndef HARDWARE_INCLUDE_SERVO_MOTOR_H
#define HARDWARE_INCLUDE_SERVO_MOTOR_H

#include <stdint.h>

namespace wer {
namespace hal {
/**
 * This class is the interface for the servo motors. It cannot be used for
 * Dynamixel servo motors, but for all other ones.
 */

class ServoMotor {
 public:
  virtual uint8_t setTargetAngle(uint8_t angle) = 0;

  virtual ~ServoMotor(){};

 protected:
  /**
   * Hardware limitations in minimal and maximal angles.
   */
  double max_angle_;
  double min_angle_;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_SERVO_MOTOR_H