#ifndef HARDWARE_INCLUDE_VIBRATION_MOTOR_H
#define HARDWARE_INCLUDE_VIBRATION_MOTOR_H

#include <stdint.h>

namespace wer {
namespace hal {
/**
 * This class is the interface for vibration motors.
 */

class VibrationMotor {
 public:
  virtual void setVibration(uint8_t on_off, double intensity = 0) = 0;

  virtual ~VibrationMotor(){};
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_VIBRATION_MOTOR_H