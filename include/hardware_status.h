#ifndef HARDWARE_INCLUDE_HARDWARE_STATUS_H
#define HARDWARE_INCLUDE_HARDWARE_STATUS_H

namespace wer {
namespace hal {

enum class HardwareStatus {
  CONNECTED = 0,
  DISCONNECTED,
};

inline const char* HardwareStatusToString(const HardwareStatus s) {
  switch (s) {
    case HardwareStatus::CONNECTED:
      return "Hardware connected";
    case HardwareStatus::DISCONNECTED:
      return "Hardware disconnected";
    default:
      return "default";
  }
  return "Non existing status";
}

}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_HARDWARE_STATUS_H
