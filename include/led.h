#pragma once

#include <inttypes.h>

#include "hardware_definition.h"

namespace wer {
namespace hal {

    class Led {
    public:
        virtual ~Led() {};

        virtual uint8_t set(WER::HW::Color color, WER::HW::BlinkType blinkType) = 0;

    private:

    };
}
}