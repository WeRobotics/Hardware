#pragma once

#include <inttypes.h>

#include <atomic>
#include <functional>
#include <map>
#include <mutex>
#include <thread>

#include "hardware_definition.h"

namespace wer {
namespace hal {

enum class GpioMode { INPUT = 0, OUTPUT = 1 };

enum class GpioEvent {
  RISING_EDGE = 0,
  FALLING_EDGE = 1,
  BOTH_EDGE = 2,
  NONE = 3
};

class Gpio {
 public:
  virtual ~Gpio(){};

  // virtual uint8_t init(IoPin gpio, GpioMode mode) = 0;
  virtual uint8_t init(uint8_t gpio, GpioMode mode) = 0;
  // virtual uint8_t digitalRead(IoPin gpio, uint8_t* value) = 0;
  virtual uint8_t digitalRead(uint8_t gpio, uint8_t* value) = 0;
  // virtual uint8_t digitalWrite(IoPin gpio, uint8_t value) = 0;
  virtual uint8_t digitalWrite(uint8_t gpio, uint8_t value) = 0;

  virtual uint8_t registerCallback(
      IoPin gpio, GpioEvent event,
      std::function<void(IoPin, GpioEvent)> callback) = 0;
};

}  // namespace hal
}  // namespace wer
