#ifndef HARDWARE_INCLUDE_I2C_H_
#define HARDWARE_INCLUDE_I2C_H_

#include <inttypes.h>

#include <functional>

namespace wer {
namespace hal {

class I2c {
 public:
  virtual ~I2c(){};

  virtual uint8_t set(uint8_t address, uint8_t data) = 0;
  virtual uint8_t set(uint8_t address, const uint8_t* data,
                      uint8_t dataLength) = 0;
  virtual uint8_t get(uint8_t address, uint8_t* data, uint8_t dataLength) = 0;

  virtual void set(uint8_t address, uint8_t data,
                   std::function<void(uint8_t)> callback) = 0;
  virtual void set(uint8_t address, const uint8_t* data, uint8_t dataLength,
                   std::function<void(uint8_t)> callback) = 0;
  virtual void get(
      uint8_t address, uint8_t dataLength,
      std::function<void(uint8_t, uint8_t*, uint8_t)> callback) = 0;
};

}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_I2C_H_