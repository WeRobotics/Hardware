#ifndef HARDWARE_INCLUDE_PWM_H
#define HARDWARE_INCLUDE_PWM_H

#include <stdint.h>

#include "hardware_status.h"

namespace wer {
namespace hal {

/**
 * This class is the interface for anykind of PWM in the system;
 */
class Pwm {
 public:
  virtual void setChannel(uint8_t channel, uint8_t dutyCycle) = 0;
  virtual void enable() = 0;
  virtual void disable() = 0;
  virtual void setFrequency(uint8_t channel, uint16_t desiredFreq) = 0;
  // virtual void selectFreq(uint8_t channel, uint16_t desiredFreq) = 0;
  // virtual void setBaseFrequency(uint8_t channel, uint16_t baseFrequency) = 0;
  virtual void setChannelOn(uint8_t channel) = 0;
  virtual void setChannelOff(uint8_t channel) = 0;
  // virtual void setSpinUpRoutine(uint8_t channel, bool kick, uint8_t driveLevel,
  //                       uint16_t spinUpTime) = 0;

  virtual ~Pwm(){};

 protected:
  // HardwareStatus getStatus() { return status_; }
  // HardwareStatus status_;
  uint8_t maxPwm_;
  uint8_t minPwm_;
};
}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_INCLUDE_PWM_H