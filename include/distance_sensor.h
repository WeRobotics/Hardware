#pragma once

#include <chrono>

#include <Library/process_result.h>

namespace wer {
    namespace hal {

		struct distance_report_s {
			std::chrono::time_point<std::chrono::steady_clock> timestamp;
			float distance;
		};

		class DistanceSensor
		{
		public:
			virtual lib::ProcessResult init() = 0;
			virtual lib::ProcessResult status() = 0;

			virtual lib::ProcessResult enable() = 0;
			virtual lib::ProcessResult disable() = 0;

			virtual float getMeasure() = 0;
		};
	}
}