#pragma once

#include <memory>
#include <functional>

#include "connection_result.h"

namespace wer {
    namespace hal {

        class Connection {
        public:
            Connection();
            virtual ~Connection();

            // Non-copyable
            Connection(const Connection &) = delete;
            const Connection &operator=(const Connection &) = delete;

            void registerCallback(std::function<void(uint8_t*, uint16_t)> callback);
			void registerTimeoutCallback(std::function<void()> timeoutCallback);

            virtual ConnectionResult start() = 0;
            virtual ConnectionResult stop() = 0;

            virtual bool sendMessage(const uint8_t* data, uint16_t dataLength) = 0;

            virtual bool diagnose() {
                return true;
            };

            virtual bool hasDiagnose() {
                return false;
            };

        protected:
            void receiveMessage(uint8_t* data, uint16_t dataLength);

            std::function<void(uint8_t* data, uint16_t dataLength)> _callback;
			std::function<void()> _timeoutCallback;
        };
    }
}
