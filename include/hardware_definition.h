#pragma once

namespace wer {
	namespace hal {
	
	enum class IoPin {
		IO_2 = 27,
		PWR_FAIL = 26,
		PG_4V = 25,
		ON_OFF_4V = 23,
		PWR_BUT = 22,
		BCK_ON = 21,
		CHG_STAT = 20,
		MOTOR_DIR = 19,
		IO_MCU = 18,
		IO_1 = 17,
		CHG_FAIL = 16
	};

	enum class LedId {
		BUTTON_LED = 0x23,
		OTHER_LED = 0x24
	};

	enum class Color {
        RED 	= 0xFF0000,
        GREEN 	= 0x00FF00,
        BLUE 	= 0x0000FF,
        ORANGE 	= 0xF04000,
        YELLOW 	= 0x907000,
        MARRON 	= 0x3030FF,
        BLACK 	= 0x000000,
        WHITE 	= 0xFFFFFF
    };

    enum class BlinkType {
    	CONTINOUS = 0,
    	FAST = 1,
    	SLOW = 2,
    	BURST = 3
    };
}
}