#ifndef HARDWARE_INCLUDE_CAMERA_H
#define HARDWARE_INCLUDE_CAMERA_H

#include <inttypes.h>

#include <string>

#include <opencv2/core.hpp>

#include <hardware_status.h>

namespace wer {
namespace hal {

/**
 * Class camera is an interface for any camera component. Any other type of
 * camera should be derived from this class.
 */

class Camera {
 public:
  /**
   * [takePicture description]
   * @param  picture       [address of the variable where picture will be put]
   *
   * @param  save_pictures [default value is 0, which means that pictures will
   * not be saved, if the picture should be saved than this value needs to be 1
   * and the path of the folder should be also put in the next argument]
   *
   * @param  path          [path to the folder where picture should be saved]
   *
   * @return               [if the picture is taken and there is no error than
   * return is 0, otherwise it is 1]
   */
  virtual uint8_t takePicture(cv::Mat& picture, uint8_t save_pictures = 0,
                              char* path = NULL) = 0;
  /**
   * [setSavePictures this function tells the class to save every picture it
   * takes to the given path]
   *
   * @param path [path to the folder where the pictures are saved]
   */
  void setSavePictures(char* path) {
    save_pictures_ = 1;
    path_ = path;
  };
  virtual ~Camera(){};

  /**
   * [setBrightness description]
   * @param brightness [value between 0 and 100]
   */
  virtual void setBrightness(uint8_t brightness) = 0;

  /**
   * [setContrast description]
   * @param contrast [value between 0 and 100]
   */
  virtual void setContrast(uint8_t contrast) = 0;

  /**
   * [setISO description]
   * @param ISO [value between 0 and 100]
   */
  virtual void setISO(uint8_t ISO) = 0;

  /**
   * [setExposure description]
   * @param exposure [value between 0 and 100, where 100 is 33ms]
   */
  virtual void setExposure(uint8_t exposure) = 0;

  /**
  * [setGrayscale function will setup camera to take only grayslace
  * photos]
  */
  virtual void setGrayscale() = 0;

 protected:
  std::string getCameraModel() { return camera_model_; }

  uint8_t save_pictures_ = 0;
  char* path_;
  HardwareStatus status_;
  std::string camera_model_;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_CAMERA_H