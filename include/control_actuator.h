#ifndef HARDWARE_INCLUDE_CONTROL_ACTUATOR_H
#define HARDWARE_INCLUDE_CONTROL_ACTUATOR_H

#include <inttypes.h>

#include <string>

#include "hardware_status.h"

namespace wer {
namespace hal {

/**
 * Class ControlActuator is the interface for any actuator component in a
 * control loop. All further actuator components used in a contorl loop should
 * be derived from this class.
 */
class ControlActuator {
 public:
  /**
   * [setRate gives back data from sensor]
   * @param  rate [float]
   * @return      [0 if there are no errors and 1 if there are one or more
   * errors]
   */
  virtual uint8_t setRate(float rate) = 0;
  /**
   * [getStatus function could either keep track in status_ and simply
   * return status_ or use instance of another class which keeps track just read
   * its status]
   * @return [HardwareStatus]
   */
  virtual uint8_t enable() = 0;
  virtual uint8_t disable() = 0;
  
  virtual HardwareStatus getStatus() = 0;
  virtual ~ControlActuator(){};

 protected:
  HardwareStatus status_;

};
}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_INCLUDE_CONTROL_ACTUATOR_H