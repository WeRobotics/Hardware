#ifndef HARDWARE_INCLUDE_SENSOR_H
#define HARDWARE_INCLUDE_SENSOR_H

#include <inttypes.h>

#include <string>

#include "hardware_status.h"

namespace wer {
namespace hal {

enum class SensorType {
  TEMPERATURE = 0,
  DISTANCE,
  OUTPUT,
  HUMIDITY,
  CANISTER_FILL
};

enum class SensorId {
  TOP_IN,
  TOP_OUT,
  TRAY_IN,
  TRAY_OUT,
  CONVEYOR_IN,
  CONVEYOR_OUT,
  TOP,
  TRAY_OUT_FORE,
  TRAY_OUT_AFT,
  TRAY_IN_FORE,
  TRAY_IN_AFT,
  CONV_OUT_FORE,
  CONV_OUT_AFT,
  CONV_IN_FORE,
  CONV_IN_AFT
};

inline const char* SensorIdToString(const SensorId id) {
  switch (id) {
    case SensorId::TOP_IN:
      return "TOP_IN";
    case SensorId::TOP_OUT:
      return "TOP_OUT";
    case SensorId::TRAY_IN:
      return "TRAY_IN";
    case SensorId::TRAY_OUT:
      return "TRAY_OUT";
    case SensorId::CONVEYOR_IN:
      return "CONVEYOR_IN";
    case SensorId::CONVEYOR_OUT:
      return "CONVEYOR_OUT:";
    case SensorId::TOP:
      return "TOP";
    case SensorId::TRAY_OUT_FORE:
      return "TRAY_OUT_FORE";
    case SensorId::TRAY_OUT_AFT:
      return "TRAY_OUT_AFT";
    case SensorId::TRAY_IN_FORE:
      return "TRAY_IN_FORE";
    case SensorId::TRAY_IN_AFT:
      return "TRAY_IN_AFT";
    case SensorId::CONV_OUT_FORE:
      return "CONV_OUT_FORE";
    case SensorId::CONV_OUT_AFT:
      return "CONV_OUT_AFT";
    case SensorId::CONV_IN_FORE:
      return "CONV_IN_FORE";
    case SensorId::CONV_IN_AFT:
      return "CONV_IN_AFT";
    default:
      return "Non existing SensorId";
  }
}

/**
 * Class sensor is the interface for any sensor component. All
 * further sensor components should be derived from this class.
 */
class Sensor {
 public:
  /**
   * [getData gives back data from sensor]
   * @param  Data [float]
   * @return      [0 if there are no errors and 1 if there are one or more
   * errors]
   */
  virtual uint8_t getData(float& Data) = 0;
  /**
   * [getStatus function could either keep track in status_ and simply
   * return status_ or use instance of another class which keeps track just read
   * its status]
   * @return [SensorStatus]
   */
  virtual HardwareStatus getStatus() = 0;
  virtual ~Sensor(){};

 protected:
  SensorType getType() { return type_; };
  std::string getUnits() { return units_; }

  HardwareStatus status_;
  SensorType type_;
  /**
   * Units in which sensor is returning the value. For example CELSIUS, when you
   * measure temperature. It should be set in constructor and always be in
   * uppercase.
   */
  std::string units_;
};
}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_INCLUDE_SENSOR_H