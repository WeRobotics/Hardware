#ifndef HARDWARE_INCLUDE_DOOR_H
#define HARDWARE_INCLUDE_DOOR_H

#include <stdint.h>

#include "hardware_status.h"

namespace wer {
namespace hal {
/**
 * This class is the interface for anykind of door in the system;
 */
class Door {
 public:
  virtual void open() = 0;
  virtual void close() = 0;

  virtual ~Door(){};

 protected:
  HardwareStatus getStatus() { return status_; }
  HardwareStatus status_;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_DOOR_H