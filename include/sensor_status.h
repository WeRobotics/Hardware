#ifndef HARDWARE_INCLUDE_SENSOR_STATUS_H
#define HARDWARE_INCLUDE_SENSOR_STATUS_H

namespace wer {
namespace hal {

enum class SensorStatus {
  NONINITIALIZED = 0,
  INITIALIZED,
  CONNECTED,
  DISCONNECTED
};

inline const char* SensorStatusToString(const SensorStatus s) {
  switch (s) {
    case SensorStatus::NONINITIALIZED:
      return "Noninitilized";
    case SensorStatus::INITIALIZED:
      return "Initialized";
    case SensorStatus::CONNECTED:
      return "Sensor connected";
    case SensorStatus::DISCONNECTED:
      return "Sensor disconnected";
  }
  return "Non existing status";
}
enum class SensorType {
  TEMPERATURE = 0,
  DISTANCE,
  OUTPUT,
  HUMIDITY,
  CANISTER_FILL
};

}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_SENSOR_STATUS_H