#pragma once

#include <map>
#include <mutex>

#include "i2c.h"
#include "gpio.h"
#include "led.h"

namespace WER {

class Hardware
{
    public:
        ~Hardware();

        static Hardware* getInstance()
        {
            static Hardware instance; // Guaranteed to be destroyed and instantiated on first use.

            return &instance;
        }

        static Hardware* get() {
            return getInstance();
        }

        void init();

        WER::HW::INTERFACE::Gpio* gpio();

    private:
        // Private Constructor
        Hardware();

        // Stop the compiler generating methods of copy the object
        Hardware(Hardware const& copy);            // Not Implemented
        Hardware& operator=(Hardware const& copy); // Not Implemented

        WER::HW::INTERFACE::Gpio* _gpio;
};

}