#ifndef HARDWARE_INCLUDE_VIBRATION_MOTOR_ARDUINO_I2C_H
#define HARDWARE_INCLUDE_VIBRATION_MOTOR_ARDUINO_I2C_H

#include <vibration_motor.h>

#include <stdint.h>

#include <memory>

#include <i2c.h>

namespace wer {
namespace hal {
/**
 * This class uses i2c to communicate with arduino, which will communicate after
 * with servo motor through the PWM.
 */
class VibrationMotorArduinoI2c : public VibrationMotor {
 public:
  VibrationMotorArduinoI2c(uint8_t address, uint8_t arduino_address,
                           std::shared_ptr<wer::hal::I2c> i2c);
  ~VibrationMotorArduinoI2c();

  void setVibration(uint8_t on_off, double intensity = 0) override;

 private:
  std::shared_ptr<wer::hal::I2c> i2c_;
  /*
  arduino_address_ is a command for arduino to know with whom and how it should
  communicate.
   */
  uint8_t arduino_address_;
  uint8_t address_;  // i2c address
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_VIBRATION_MOTOR_ARDUINO_I2C_H