#include "hardware.h"

#include "hardware_definition.h"

#include "../drivers/gpio.h"
#include <iostream>

WER::Hardware::Hardware() : _gpio(nullptr) {}

WER::Hardware::~Hardware() {
  if (_gpio) {
    delete (_gpio);
    _gpio = nullptr;
  }
}

void WER::Hardware::init() {
	_gpio = new WER::HW::DRIVER::Gpio();

	uint8_t ret;
	ret = _gpio->init(WER::HW::IO::IO_2, 		WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure IO_2. Error : " << unsigned(ret) << std::endl;
	}
	
	ret = _gpio->init(WER::HW::IO::PWR_FAIL, 	WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure PWR_FAIL. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::PG_4V, 		WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure PG_4V. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::PWR_BUT, 	WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure PWR_BUT. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::CHG_STAT,	WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure CHG_STAT. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::IO_MCU, 		WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure IO_MCU. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::IO_1, 		WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure IO_1. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::CHG_FAIL, 	WER::HW::INTERFACE::GpioDirection::INPUT);
	if(ret) {
		std::cout << "Could not configure CHG_FAIL. Error : " << unsigned(ret) << std::endl;
	}


	ret = _gpio->init(WER::HW::IO::BCK_ON, 	WER::HW::INTERFACE::GpioDirection::OUTPUT);
	if(ret) {
		std::cout << "Could not configure BCK_ON. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::MOTOR_DIR, WER::HW::INTERFACE::GpioDirection::OUTPUT);
	if(ret) {
		std::cout << "Could not configure MOTOR_DIR. Error : " << unsigned(ret) << std::endl;
	}

	ret = _gpio->init(WER::HW::IO::ON_OFF_4V, WER::HW::INTERFACE::GpioDirection::OUTPUT);
	if(ret) {
		std::cout << "Could not configure ON_OFF_4V. Error : " << unsigned(ret) << std::endl;
	}

}

WER::HW::INTERFACE::Gpio* WER::Hardware::gpio() { return _gpio; }