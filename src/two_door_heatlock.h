#ifndef HARDWARE_INCLUDE_TWO_DOOR_HEATLOCK_H
#define HARDWARE_INCLUDE_TWO_DOOR_HEATLOCK_H

#include <heatlock.h>

#include <stdint.h>

#include <door.h>

namespace wer {
namespace hal {
/**
 * TwoDoorHeatlock is a heatlock with top and bottom door. Mosquitos should
 * entor through the top door and them should be released through the bottom
 * door.
 */

class TwoDoorHeatlock : public Heatlock {
 public:
  static TwoDoorHeatlock* createTwoDoorHeatlock(Door* top, Door* bottom,
                                                int mosquito_expilsion_time_ms);
  ~TwoDoorHeatlock();

  void open() override;
  void close() override;
  void release() override;

 private:
  TwoDoorHeatlock(Door* top, Door* bottom, int mosquito_expilsion_time_ms);
  Door* top_;
  Door* bottom_;

  const int mosquito_expilsion_time_ms_;  // 1500
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_TWO_DOOR_HEATLOCK_H