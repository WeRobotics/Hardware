#include <two_door_heatlock.h>

#include <boost/thread.hpp>

#include <door.h>
#include <log.h>

#include <hardware_status.h>

namespace wer {
namespace hal {
TwoDoorHeatlock* TwoDoorHeatlock::createTwoDoorHeatlock(
    Door* top, Door* bottom, int mosquito_expilsion_time_ms) {
  if (top && bottom) {
    return new TwoDoorHeatlock(top, bottom, mosquito_expilsion_time_ms);
  } else {
    return nullptr;
  }
}

TwoDoorHeatlock::TwoDoorHeatlock(Door* top, Door* bottom,
                                 int mosquito_expilsion_time_ms)
    : mosquito_expilsion_time_ms_(mosquito_expilsion_time_ms) {
  top_ = top;
  bottom_ = bottom;
  status_ = HardwareStatus::CONNECTED;
}

TwoDoorHeatlock::~TwoDoorHeatlock() {
  delete top_;
  delete bottom_;
}

void TwoDoorHeatlock::open() { top_->open(); }

void TwoDoorHeatlock::close() { top_->close(); }

void TwoDoorHeatlock::release() {
  bottom_->open();
  boost::this_thread::sleep_for(
      boost::chrono::milliseconds(mosquito_expilsion_time_ms_));
  bottom_->close();
}

}  // hal
}  // wer