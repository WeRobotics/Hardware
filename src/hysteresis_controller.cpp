#include "hysteresis_controller.h"

namespace wer {
namespace hal {

HysteresisController::HysteresisController(
    std::shared_ptr<lib::CallEveryHandler> call_every_handler, uint8_t id,
    float maxThreshold, float minThreshold, uint8_t averageSize,
    uint16_t updatePeriod)
    : id_(id),
      call_every_handler_(call_every_handler),
      max_threshold_(maxThreshold),
      min_threshold_(minThreshold),
      average_size_(averageSize),
      update_period_(updatePeriod),
      average_value_(0.0),
      sample_counter_(0),
      samples_init_(true) {
  log_ = lib::LogManager::createLogMsg(
      true,
      "/home/pi/WMP-Belt/communication/javascript_webinterface/interface/logs/"
      "cooling");

  measurement_samples_ = new float[average_size_];
  log_->info_ << "HysteresisController created" << '\n';
}

HysteresisController::~HysteresisController() {
  call_every_handler_->remove(cookie_);

  if (measurement_samples_) {
    delete measurement_samples_;
    measurement_samples_ = nullptr;
  }
}

void HysteresisController::setCallbackMeasurementInput(
    std::function<uint8_t(float&)> callback) {
  callback_measurement_input_ = callback;
}

void HysteresisController::setCallbackAboveMax(
    std::function<void(void)> callback) {
  callback_above_max_ = callback;
}

void HysteresisController::setCallbackBelowMin(
    std::function<void(void)> callback) {
  callback_below_min_ = callback;
}

void HysteresisController::enable() {
  call_every_handler_->add(std::bind(&HysteresisController::cycle, this),
                           update_period_, &cookie_);
  control_state_ = true;

  for (int i = 0; i < average_size_; i++) {
    measurement_samples_[i] = 0.0f;
  }
}

void HysteresisController::disable() { control_state_ = false; }

void HysteresisController::cycle() {
  float measurement_value = 0.0f;
  // log_->info_ << "cycle hysteresis \n";
  // Get new measurement
  callback_measurement_input_(measurement_value);

  // Updating average
  average_value_ = average_value_ +
                   (measurement_value - measurement_samples_[sample_counter_]) /
                       average_size_;

  // Adding new sample to arrays
  measurement_samples_[sample_counter_] = measurement_value;
  sample_counter_++;

  // No control when filling the samples array for the first time
  if (samples_init_ && (sample_counter_ >= average_size_)) {
    samples_init_ = false;
    //		BOOST_LOG_SEV(_slg, info) << "Measurements array
    // initialized";
  }
  if (samples_init_) {
    return;
  }

  // Circular buffer
  if (sample_counter_ == average_size_) {
    sample_counter_ = 0;
  }

  log_->info_ << "Control thread: measurement_value = " << measurement_value
              << ", average_value_ = " << average_value_ << '\n';

  // Controller updated independently of measurement value
  if (control_state_) {
    log_->info_ << "Controlling with thresholds: " << min_threshold_ << "/"
                << max_threshold_ << '\n';

    // Hysteresis controller
    if (measurement_value /*average_value_*/ > max_threshold_) {
      callback_above_max_();
    } else if (measurement_value /*average_value_*/ < min_threshold_) {
      callback_below_min_();
    }
  }
}

void HysteresisController::setMaxThreshold(float value) {
  max_threshold_ = value;
}

void HysteresisController::setMinThreshold(float value) {
  min_threshold_ = value;
}

uint8_t HysteresisController::getId() { return id_; }

}  // namespace hal
}  // namespace wer