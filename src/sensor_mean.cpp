#include <sensor_mean.h>

#include <iostream>
#include <limits>

#include "log.h"

namespace wer {
namespace hal {

SensorMean::SensorMean(std::vector<std::shared_ptr<hal::Sensor>> sensors)
    : sensors_(sensors) {}
// SensorMean::SensorMean(std::shared_ptr<hal::Sensor> sensor1,
//                        std::shared_ptr<hal::Sensor> sensor2)
//     : sensor1_(sensor1), sensor2_(sensor2) {}

SensorMean::~SensorMean() {}

uint8_t SensorMean::getData(float& data) {
  float value = 0;
  // std::cout << "sensor mean " << sensors_.size() << std::endl;
  for (auto& it : sensors_) {
    float data;
    it->getData(data);
    value += data;
  }
  data = value / sensors_.size();

  return false;
}
// uint8_t SensorMean::getData(float& data) {
//   float data1;
//   sensor1_->getData(data1);
//   float data2;
//   sensor2_->getData(data2);
//   // std::cout << "sensor mean" << (data1 + data2) / 2.0 << std::endl;
//   data = (data1 + data2) / 2.0;
//   return false;
// }

HardwareStatus SensorMean::getStatus() {
  // TODO
  return HardwareStatus::CONNECTED;
}

}  // namespace hal
}  // namespace wer
