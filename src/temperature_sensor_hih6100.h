#ifndef HARDWARE_INCLUDE_TEMPERATURE_SENSOR_Hih6100_H
#define HARDWARE_INCLUDE_TEMPERATURE_SENSOR_Hih6100_H

#include "sensor.h"

#include <sys/types.h>

#include <atomic>
#include <chrono>

#include <call_every_handler.h>

#include "hardware_status.h"
#include "hih6100.h"
#include "i2c.h"

namespace wer {
namespace hal {
/**
 * This class is used to get only temperature from the Hih6100 sensor component,
 * it implements the Sensor interface and uses Hih6100 component.
 */

class TemperatureSensorHih6100 : public Sensor {
 public:
  static std::shared_ptr<Sensor> createInstance(
      std::shared_ptr<Hih6100> hih6100, float sample_rate);
  TemperatureSensorHih6100(std::shared_ptr<Hih6100> hih6100, float sample_rate);

  ~TemperatureSensorHih6100();

  uint8_t getData(float& data) override;

  HardwareStatus getStatus() override;

 private:
  // TemperatureSensorHih6100(std::shared_ptr<Hih6100> hih6100, float sample_rate);

  std::shared_ptr<Hih6100> hih6100_;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_TEMPERATURE_SENSOR_Hih6100_H
