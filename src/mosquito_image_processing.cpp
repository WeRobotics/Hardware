#include "mosquito_image_processing.h"

#include <sys/statvfs.h>

#include <iostream>
#include <list>
#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

namespace wer {
namespace hal {

MosquitoImageProccesing::MosquitoImageProccesing(int diff_threshold,
                                                 int small_opening_size,
                                                 int bigger_closing_size,
                                                 bool use_biggest_cluster,
                                                 bool display, bool save) {
  save_ = save;
  display_ = display;
  // use the biggest cluster or all changing images
  use_biggest_cluster_ = use_biggest_cluster;

  // define the parameters for image processing
  diff_threshold_ = diff_threshold;
  small_opening_size_ = small_opening_size;
  bigger_closing_size_ = bigger_closing_size;
}
MosquitoImageProccesing::~MosquitoImageProccesing() {}

void MosquitoImageProccesing::init(cv::Mat background) {
  const int cropping_left = 180;
  const int cropping_right = 1200;
  const int cropping_top = 240;
  const int cropping_bottom = 930;
  cv::Rect cropped(cropping_left, cropping_top, cropping_right - cropping_left,
                   cropping_bottom - cropping_top);
  background_circular_index_ = 0;
  prev_raw_backgrounds_[background_circular_index_] = background(cropped);
  for (int i = 1; i < 5; i++) {
    prev_raw_backgrounds_[i] =
        prev_raw_backgrounds_[background_circular_index_];
  }
  background_ = prev_raw_backgrounds_[background_circular_index_];
}

void MosquitoImageProccesing::update(cv::Mat picture,
                                     MosquitoImageProccesingType type) {
  const int cropping_left = 180;
  const int cropping_right = 1200;
  const int cropping_top = 240;
  const int cropping_bottom = 930;
  cv::Rect cropped(cropping_left, cropping_top, cropping_right - cropping_left,
                   cropping_bottom - cropping_top);
  switch (type) {
    case wer::hal::MosquitoImageProccesingType::BACKGROUND:
      background_circular_index_++;
      if (background_circular_index_ == 5) {
        background_circular_index_ = 0;
      }
      prev_raw_backgrounds_[background_circular_index_] = picture(cropped);
      for (int j = 0; j < background_.rows; j++) {
        for (int i = 0; i < background_.cols; i++) {
          background_.at<uchar>(j, i) = std::max(
              prev_raw_backgrounds_[0].at<uchar>(j, i),
              std::max(
                  prev_raw_backgrounds_[1].at<uchar>(j, i),
                  std::max(
                      prev_raw_backgrounds_[2].at<uchar>(j, i),
                      std::max(prev_raw_backgrounds_[3].at<uchar>(j, i),
                               prev_raw_backgrounds_[4].at<uchar>(j, i)))));
        }
      }
      break;

    case wer::hal::MosquitoImageProccesingType::FOREGROUND:
      foreground_ = picture(cropped);
      break;
  }
}

double MosquitoImageProccesing::compute_pixel_count() {
  if (background_.rows == 0 || background_.cols == 0)
    std::cerr << "invalid background size" << std::endl;

  // calculate the difference between the current image and the reference image
  cv::Mat Im_diff(foreground_.rows, foreground_.cols, CV_8UC1, cv::Scalar(128));
  for (int j = 0; j < foreground_.rows; j++) {
    for (int i = 0; i < foreground_.cols; i++) {
      if (ROI_x_min(j) <= i && i <= ROI_x_max(j))  // inside the funnel
      {
        Im_diff.at<uchar>(j, i) = std::min(
            255,
            abs(foreground_.at<uchar>(j, i) - background_.at<uchar>(j, i)));
      } else  // outside the funnel : we zero all pixels
      {
        Im_diff.at<uchar>(j, i) = 0;
      }
    }
  }

  // apply a threshold to detect were differences occured
  cv::Mat Im_diff_with_threshold;
  cv::threshold(Im_diff, Im_diff_with_threshold, diff_threshold_, 255,
                cv::THRESH_BINARY);

  // apply a oppening with a small kernel (enables to remove litle white parts
  // (while = difference above threshold))
  cv::Mat Im_with_removal_of_small_differences;
  cv::Mat openning_kernel = cv::getStructuringElement(
      cv::MORPH_RECT, cv::Size(small_opening_size_, small_opening_size_),
      cv::Point(small_opening_size_ / 2, small_opening_size_ / 2));
  cv::morphologyEx(Im_diff_with_threshold, Im_with_removal_of_small_differences,
                   cv::MORPH_OPEN, openning_kernel);

  // apply a bigger closing to fill the gaps in the differences
  cv::Mat Im_with_filling_of_diff_gaps;
  cv::Mat closing_kernel = cv::getStructuringElement(
      cv::MORPH_RECT, cv::Size(bigger_closing_size_, bigger_closing_size_),
      cv::Point(bigger_closing_size_ / 2, bigger_closing_size_ / 2));
  cv::morphologyEx(Im_with_removal_of_small_differences,
                   Im_with_filling_of_diff_gaps, cv::MORPH_CLOSE,
                   closing_kernel);

  // warning : matrice coordinates are (j,i)=(row, column)
  // x=i, y=j
  int nbr_white_pixels = 0;
  for (int y = 0; y < Im_with_filling_of_diff_gaps.rows; y++) {
    int xmin = ROI_x_min(y);
    int xmax = ROI_x_max(y);
    for (int x = xmin; x <= xmax; x++)  // nb : the restriction at the ROI
                                        // shouldn't be needed anymore, but it
                                        // speeds up the computation
    {
      if (Im_with_filling_of_diff_gaps.at<uchar>(y, x) !=
          0)  // there is a difference between this image and thebackground at
              // this position
        nbr_white_pixels++;
    }
  }

  // do clustering
  int tot_nbr_pixels_from_clustering, nbr_pixels_second_biggest_cluster,
      nbr_clusters, ID_of_biggest_cluster;
  int nbr_pixels_of_biggest_cluster = find_biggest_cluster(
      Im_with_filling_of_diff_gaps, &tot_nbr_pixels_from_clustering,
      &nbr_pixels_second_biggest_cluster, &nbr_clusters,
      &ID_of_biggest_cluster);

  if (display_) {
    std::cout << "the number of changing pixels is : " << nbr_white_pixels
              << std::endl;
    std::cout << "clustering : biggest: " << nbr_pixels_of_biggest_cluster
              << " second: " << nbr_pixels_second_biggest_cluster
              << " total: " << tot_nbr_pixels_from_clustering
              << " clusters: " << nbr_clusters
              << " biggest_cluster_ID: " << ID_of_biggest_cluster << std::endl;
  }

  int estimated_nbr_of_valid_pixels;
  if (use_biggest_cluster_) {
    estimated_nbr_of_valid_pixels = nbr_pixels_of_biggest_cluster;
  } else {
    estimated_nbr_of_valid_pixels = nbr_white_pixels;
  }

  if (display_)
    std::cout << "the estimated number of changing pixels is :"
              << estimated_nbr_of_valid_pixels << std::endl;

  /*if (save) {
    cv::imwrite("../originalImageIsoComponsated.bmp", foreground_);
    cv::imwrite("../background.bmp", this->background);
    cv::imwrite("../Im_diff.bmp", Im_diff);
    cv::imwrite("../Im_diff_with_threshold.bmp", Im_diff_with_threshold);
    cv::imwrite("../Im_with_removal_of_small_differences.bmp",
                Im_with_removal_of_small_differences);
    cv::imwrite("../Im_with_filling_of_diff_gaps.bmp",
                Im_with_filling_of_diff_gaps);
  }*/
  if (display_) {
    cv::namedWindow("Initial Image",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::namedWindow("background",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::namedWindow("Im_diff",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::namedWindow("Im_diff_with_threshold",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::namedWindow("Im_with_removal_of_small_differences",
                    cv::WINDOW_AUTOSIZE);  // Create a window for display.
    cv::namedWindow("Im_with_filling_of_diff_gaps",
                    cv::WINDOW_AUTOSIZE);      // Create a window for display.
    cv::imshow("Initial Image", foreground_);  // Show our image inside it.
    cv::imshow("background", background_);     // Show our image inside it.
    cv::imshow("Im_diff", Im_diff);            // Show our image inside it.
    cv::imshow("Im_diff_with_threshold",
               Im_diff_with_threshold);  // Show our image inside it.
    cv::imshow(
        "Im_with_removal_of_small_differences",
        Im_with_removal_of_small_differences);  // Show our image inside it.
    cv::imshow("Im_with_filling_of_diff_gaps",
               Im_with_filling_of_diff_gaps);  // Show our image inside it.
    cv::waitKey(0);  // Wait for a keystroke in the window
  }

  return estimated_nbr_of_valid_pixels;
}
int MosquitoImageProccesing::compute_nice_mosquito_number_from_raw(
    double mosquitoes) {
  if (mosquitoes <= 0)
    return 0;
  else
    return round(mosquitoes);
}

double MosquitoImageProccesing::compute_mosquitoes_from_pixels(
    int nbr_white_pixels, double regression_slope,
    double regression_intersect) {
  return nbr_white_pixels * regression_slope + regression_intersect;
}
int MosquitoImageProccesing::find_biggest_cluster(
    cv::Mat image, int *tot_nbr_pixels, int *nbr_pixels_second_biggest_cluster,
    int *nbr_clusters, int *ID_of_biggest_cluster) {
  /***********************************************
  * Basic algorithm :
  * 1) we create an array of all rows of the image, each row beeing the list
  *of the white segments of this row (a segment beeing defined by start and end
  *x coordinates)
  * 2) we cluster the segments in the following way
  *   a) we pick a segment that wasn't visited yet (we iterate on all
  *segments and check if they were already visited)
  *   b) we add it to a new cluster (which marks it as visited), and put
  *it into the waiting list (beeing in the waiting list means the segment was
  *visited but we haven't checked its neighbohrs yet)
  *   c) while the waiting list is not empty, we pick an element, and
  *iterate through the segments of the rows above and bellow to find its
  *neighbohrs : for each of those, we add it to the cluster (mark is as visited
  *and part of the cluster, and increase the number of pixels of the cluster)
  *and put it into the waiting list
  *
  ******************************************/
  typedef struct row_segment row_segment;
  struct row_segment {
    int start_x;
    int end_x;
    int row_nbr;
    int cluster;  //-1 if not affected
  };

  typedef struct row row;
  struct row {
    int nbr_segments;
    std::list<row_segment> *segments;
  };

  int nbr_rows = image.rows;

  // list segments of all rows
  row *rows = new row[nbr_rows];  // array of all rows
  for (int y = 0; y < nbr_rows; y++) {
    rows[y].nbr_segments = 0;
    rows[y].segments = new std::list<row_segment>();
    int x = ROI_x_min(y);
    int x_max = ROI_x_max(y);
    while (x < x_max) {
      if (image.at<uchar>(y, x) != 0) {
        int min_segment = x;
        while (x + 1 < x_max && image.at<uchar>(y, x + 1) != 0) {
          x++;
        }
        int max_segment = x;
        row_segment new_segment = {.start_x = min_segment,
                                   .end_x = max_segment,
                                   .row_nbr = y,
                                   .cluster = -1};
        rows[y].segments->push_back(new_segment);
        rows[y].nbr_segments++;
      }
      x++;
    }
  }

  // compute clusters and there sizes
  int clusterID = 0;
  std::vector<int> cluster_size;  // contains the size of the cluster at the
                                  // corresponding position
  for (int y = 0; y < nbr_rows;
       y++)  // iterate on rows to find unaffected segment
  {
    for (std::list<row_segment>::iterator it = rows[y].segments->begin();
         it != rows[y].segments->end();
         it++)  // iterating on the segments of the row
    {
      if (it->cluster == -1)  // the segment is not assigned : we will try to
                              // determine its cluster
      {
        it->cluster = clusterID;
        int nbr_pixels_in_cluster = it->end_x - it->start_x + 1;
        std::list<row_segment>
            waiting_list;  // list of segments of the clusters for
                           // which we haven't looked yet for
                           // neighbohrs
        waiting_list.push_back(
            *it);  // add the initial segment to the waitng list

        while (!waiting_list.empty())  // process the waiting list
        {
          row_segment current = waiting_list.back();  // get last element
          waiting_list.pop_back();  // and remove it from the list

          int r = current.row_nbr;
          // look for neighbohring segments in the row above
          if (r != 0)  // if we are not in the top row
          {
            for (std::list<row_segment>::iterator it_above =
                     rows[r - 1].segments->begin();
                 it_above != rows[r - 1].segments->end();
                 it_above++)  // iterating on the segments of the row above
            {
              if (it_above->cluster == -1 &&
                  it_above->start_x <= current.end_x &&
                  it_above->end_x >= current.start_x)  // the segment hasn't
                                                       // been assigned yet and
                                                       // the x of the 2
                                                       // segments overlap :
                                                       // they are neighbors
              {
                it_above->cluster = clusterID;  // afect it to the cluster
                nbr_pixels_in_cluster += it_above->end_x - it_above->start_x +
                                         1;  // updating the pixel count
                waiting_list.push_back(*it_above);
              }
            }
          }
          // look for neighbohring segments in the row bellow
          if (r < image.rows - 1)  // if we are not in the bottom row
          {
            for (std::list<row_segment>::iterator it_bellow =
                     rows[r + 1].segments->begin();
                 it_bellow != rows[r + 1].segments->end();
                 it_bellow++)  // iterating on the segments of the row bellow
            {
              if (it_bellow->cluster == -1 &&
                  it_bellow->start_x <= current.end_x &&
                  it_bellow->end_x >= current.start_x)  // the x of the 2
                                                        // segments overlap :
                                                        // they are neighbors
              {
                it_bellow->cluster = clusterID;  // afect it to the cluster
                nbr_pixels_in_cluster += it_bellow->end_x - it_bellow->start_x +
                                         1;  // updating the pixel count
                waiting_list.push_back(*it_bellow);
              }
            }
          }
        }

        cluster_size.push_back(nbr_pixels_in_cluster);
        clusterID++;
      }
    }
  }

  int nbr_of_clusters = clusterID;
  int max_nbr_of_pixels = 0;
  int biggest_cluster_ID = -1;
  int total_number_of_pixels = 0;
  int second_max_nbr_of_pixels = 0;

  for (int i = 0; i < nbr_of_clusters; i++) {
    total_number_of_pixels += cluster_size[i];
    if (cluster_size[i] >= max_nbr_of_pixels) {
      second_max_nbr_of_pixels = max_nbr_of_pixels;
      max_nbr_of_pixels = cluster_size[i];
      biggest_cluster_ID = i;
    } else if (cluster_size[i] > second_max_nbr_of_pixels) {
      second_max_nbr_of_pixels = cluster_size[i];
    }
  }

  // return additional information if return variables are provided
  if (tot_nbr_pixels != NULL) *tot_nbr_pixels = total_number_of_pixels;
  if (nbr_pixels_second_biggest_cluster != NULL)
    *nbr_pixels_second_biggest_cluster = second_max_nbr_of_pixels;
  if (nbr_clusters != NULL) *nbr_clusters = nbr_of_clusters;
  if (ID_of_biggest_cluster != NULL)
    *ID_of_biggest_cluster = biggest_cluster_ID;

  // clean up
  for (int y = 0; y < nbr_rows; y++) delete rows[y].segments;
  delete[] rows;

  return max_nbr_of_pixels;
}

void MosquitoImageProccesing::linearRegression(double *x, double *y,
                                               int nbr_samples, double &a,
                                               double &b, double &r) {
  double x_mean = 0;
  double y_mean = 0;
  double Sx2 = 0;
  double Sy2 = 0;
  double Sxy = 0;
  for (int i = 0; i < nbr_samples; i++) {
    x_mean += x[i];
    y_mean += y[i];
  }
  x_mean /= nbr_samples;
  y_mean /= nbr_samples;
  for (int i = 0; i < nbr_samples; i++) {
    Sx2 += pow(x[i] - x_mean, 2);
    Sy2 += pow(y[i] - y_mean, 2);
    Sxy += (x[i] - x_mean) * (y[i] - y_mean);
  }
  Sx2 /= (nbr_samples - 1);
  Sy2 /= (nbr_samples - 1);
  Sxy /= (nbr_samples - 1);

  if (Sx2 * Sy2 != 0) {
    r = Sxy / sqrt(Sx2 * Sy2);
  } else  // if we have no variance at all along one axis : the special case is
          // needed to avoid division by 0
  {
    r = 1;
  }

  a = Sxy / Sx2;
  b = y_mean - a * x_mean;
}
// define the precise boundaries of the region of interest

int MosquitoImageProccesing::interpolate(int y, int a1, int f1, int a2,
                                         int f2) {
  return f1 + (y - a1) * (f2 - f1) / (a2 - a1);
}
int MosquitoImageProccesing::ROI_x_min(int y) {
  switch (mechanism_setup_) {
    case 1:
      if (y <= 180) return 0;
      if (y <= 300) return 0 + 30 * (y - 180) / (300 - 180);
      if (y <= 400) return 30 + (75 - 30) * (y - 300) / 100;
      if (y <= 500) return 75 + (130 - 75) * (y - 400) / 100;
      ;
      if (y <= 550) return 130 + (162 - 130) * (y - 500) / 50;
      if (y <= 575) return 162 + (350 - 162) * (y - 550) / 25;
      if (y <= 580) return 350 + (1000 - 350) * (y - 575) / 5;
      ;
      return 1020;
      break;
    case 2:
      if (y <= 50) return 0;
      if (y <= 100) return interpolate(y, 50, 0, 100, 11);
      if (y <= 200) return interpolate(y, 100, 11, 200, 60);
      if (y <= 300) return interpolate(y, 200, 60, 300, 113);
      if (y <= 410) return interpolate(y, 300, 113, 410, 190);
      if (y <= 432) return interpolate(y, 410, 190, 432, 450);
      return 1000;
      break;
    case 3:
      if (y <= 45) return 0;
      if (y <= 100) return interpolate(y, 50, 0, 100, 11);
      if (y <= 200) return interpolate(y, 100, 11, 200, 60);
      if (y <= 300) return interpolate(y, 200, 60, 300, 113);
      if (y <= 410) return interpolate(y, 300, 113, 410, 190);
      if (y <= 432) return interpolate(y, 410, 190, 432, 450);
      return 1000;
      break;
    case 4:
      if (y <= 100) return 1000;
      if (y <= 200) return interpolate(y, 100, 0, 200, 32);
      if (y <= 300) return interpolate(y, 200, 32, 300, 70);
      if (y <= 410) return interpolate(y, 300, 70, 410, 126);
      if (y <= 525) return interpolate(y, 410, 126, 525, 205);
      if (y <= 540) return interpolate(y, 525, 205, 540, 300);
      if (y <= 545) return interpolate(y, 540, 300, 545, 400);
      return 1000;
      break;
  }
}
int MosquitoImageProccesing::ROI_x_max(int y) {
  switch (mechanism_setup_) {
    case 1:
      if (y < 140) return 915;
      if (y <= 200) return 915 + (896 - 915) * (y - 140) / 60;
      if (y <= 300) return 896 + (862 - 896) * (y - 200) / 100;
      if (y <= 400) return 862 + (814 - 862) * (y - 300) / 100;
      if (y <= 500) return 814 + (760 - 814) * (y - 400) / 100;
      if (y <= 535) return 760 + (735 - 760) * (y - 500) / 35;
      if (y <= 545) return 735 + (700 - 735) * (y - 535) / 10;
      if (y <= 575) return 700 + (575 - 700) * (y - 545) / 30;
      return 0;
      break;
    case 2:
      if (y <= 0) return 895;
      if (y <= 100) return interpolate(y, 0, 895, 100, 870);
      if (y <= 200) return interpolate(y, 100, 870, 200, 830);
      if (y <= 300) return interpolate(y, 200, 830, 300, 780);
      if (y <= 390) return interpolate(y, 300, 780, 390, 720);
      if (y <= 410) return interpolate(y, 390, 712, 410, 705);
      if (y <= 432) return interpolate(y, 410, 705, 432, 450);
      return 0;
      break;
    case 3:
      if (y <= 0) return 895;
      if (y <= 100) return interpolate(y, 0, 895, 100, 870);
      if (y <= 200) return interpolate(y, 100, 870, 200, 830);
      if (y <= 300) return interpolate(y, 200, 830, 300, 780);
      if (y <= 390) return interpolate(y, 300, 780, 390, 720);
      if (y <= 410) return interpolate(y, 390, 712, 410, 705);
      if (y <= 432) return interpolate(y, 410, 705, 432, 450);
      return 0;
      break;
    case 4:
      if (y <= 100) return 0;
      if (y <= 150) return 900;
      if (y <= 300) return interpolate(y, 150, 900, 300, 832);
      if (y <= 400) return interpolate(y, 300, 832, 400, 782);
      if (y <= 530) return interpolate(y, 400, 782, 530, 695);
      if (y <= 540) return interpolate(y, 530, 695, 540, 580);
      if (y <= 545) return interpolate(y, 540, 580, 545, 400);
      return 0;
      break;
  }
}
void MosquitoImageProccesing::save_image(cv::Mat image, std::string base_path,
                                         int number) {
  struct statvfs buffer;
  int ret = statvfs(".", &buffer);
  double MB = 1024 * 1024;

  if (!ret) {
    const double available = (double)(buffer.f_bavail * buffer.f_bsize) / MB;
    if (available > 100)  // more than 100MB available
    {
      std::string full_name = base_path + std::to_string(number) + ".jpg";
      imwrite(full_name, image);
    }
  }
}

}  // hal
}  // wer