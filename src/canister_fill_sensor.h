#ifndef HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_H
#define HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_H

#include "sensor.h"

#include <thread>
#include <vector>

#include <call_every_handler.h>

#include "canister_fill_sensor_calibration.h"
#include "hardware_status.h"
#include "i2c.h"

namespace wer {
namespace hal {
/**
 * This class is estimating a fullness of the canister, based on the distance
 * sensor SHARP GP2Y0A51SK0F.
 */

class CanisterFillSensor : public Sensor {
 public:
  static CanisterFillSensor* createCanisterFillSensor(
      std::shared_ptr<lib::CallEveryHandler> callEveryHandler,
      std::shared_ptr<I2c> i2c, uint8_t address,
      std::vector<double> interpolationCoefficients, float sample_rate = 0.0);
  ~CanisterFillSensor();

  /**
   * @param Data is the buffer where the data will be written. It is a value
   * between 0 and 1. 0 means that a canister is empty and 1 that
   * canister is full.
   * @return 0 if there are no errors and 1 if there is an error.
   */
  uint8_t getData(float& Data) override;

  HardwareStatus getStatus() override;

 private:
  CanisterFillSensor(std::shared_ptr<lib::CallEveryHandler> callEveryHandler,
                     std::shared_ptr<I2c> i2c, uint8_t address,
                     std::vector<double> interpolationCoefficients,
                     float sample_rate);

  void generateLogName();
  void cycle();
  double estimate(uint8_t raw_value);

  std::shared_ptr<wer::lib::CallEveryHandler> callEveryHandler_;
  std::shared_ptr<wer::hal::I2c> i2c_;
  uint8_t address_;

  float fill_percentage_;
  std::vector<double> interpolationCoefficients_;

  void* cookie_;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_H
