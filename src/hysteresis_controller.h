#ifndef HARDWARE_INCLUDE_HYSTERESIS_CONTROLLER_H
#define HARDWARE_INCLUDE_HYSTERESIS_CONTROLLER_H

#include <sys/types.h>
#include <atomic>
#include <iostream>
#include <list>
#include <thread>

#include <call_every_handler.h>
#include <Library/log/log_manager.h>

namespace wer {
namespace hal {

class HysteresisController {
 public:
  // TODO generalize to work with sensor and actuator interface in stead of call
  // backs
  HysteresisController(
      std::shared_ptr<lib::CallEveryHandler> call_every_handler, uint8_t id,
      float maxThreshold, float minThreshold, uint8_t averageSize,
      uint16_t updatePeriod);  // TODO: add string parameter for logging/ID ?
  ~HysteresisController();

  // TODO: callbacks as parameters in Constructor ?
  void setCallbackMeasurementInput(
      std::function<uint8_t(float&)>
          callback);  // will already be mean across all considered
                      // tempHumSensors = 1 float value only
  void setCallbackAboveMax(std::function<void(void)> callback);
  void setCallbackBelowMin(std::function<void(void)> callback);

  void enable();  // TODO: start without enabling ?
  void disable();

  void setMaxThreshold(float value);
  void setMinThreshold(float value);

  uint8_t getId();

 private:
  lib::LogMsg* log_;

  std::function<uint8_t(float&)> callback_measurement_input_;
  std::function<void(void)> callback_above_max_;
  std::function<void(void)> callback_below_min_;

  std::shared_ptr<lib::CallEveryHandler> call_every_handler_;
  void* cookie_;

  void cycle();

  float* measurement_samples_ = nullptr;

  uint8_t id_;
  float max_threshold_;
  float min_threshold_;
  uint8_t average_size_;
  uint16_t update_period_;
  float average_value_;
  uint8_t sample_counter_;
  bool samples_init_;
  std::atomic<bool> control_state_{false};
};

}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_INCLUDE_HYSTERESIS_CONTROLLER_H
