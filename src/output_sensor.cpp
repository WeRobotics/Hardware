#include "output_sensor.h"

#include <functional>

#include <yaml-cpp/yaml.h>
#include <opencv2/core.hpp>

namespace wer {
namespace hal {
OutputSensor* OutputSensor::createOutputSensor(std::shared_ptr<Camera> camera) {
  if (camera) {
    return new OutputSensor(camera);
  } else {
    return nullptr;
  }
}

OutputSensor::OutputSensor(std::shared_ptr<Camera> camera) {
  YAML::Node config = YAML::LoadFile("../../../config/hal.yaml");
  YAML::Node config_camera = config["output_sensor"]["camera"];
  mosquitoes_count_ = -1;
  camera_ = camera;
  status_ = HardwareStatus::CONNECTED;

  camera_->setGrayscale();
  camera_->setBrightness(config_camera["brightness"].as<int>());
  camera_->setISO(config_camera["ISO"].as<int>());
  camera_->setExposure(config_camera["exposure"].as<int>());
  camera_->setContrast(config_camera["contrast"].as<int>());

  YAML::Node config_mosquito_image_proccesing =
      config["output_sensor"]["mosquito_image_processing"];
  mosquito_image_proccesing_ = new MosquitoImageProccesing(
      config_mosquito_image_proccesing["diff_threshold"].as<int>(),
      config_mosquito_image_proccesing["small_opening_size"].as<int>(),
      config_mosquito_image_proccesing["bigger_closing_size"].as<int>(),
      config_mosquito_image_proccesing["use_biggest_cluster"].as<bool>(),
      config_mosquito_image_proccesing["display"].as<bool>());
  cv::Mat dataPicture;
  camera_->takePicture(dataPicture);
  mosquito_image_proccesing_->init(dataPicture);
}
OutputSensor::~OutputSensor() { camera_ = NULL; }

uint8_t OutputSensor::getData(int& Data) {
  if (status_ == HardwareStatus::CONNECTED) {
    Data = mosquitoes_count_;
    return 0x01;
  } else {
    return 0x00;
  }
}

HardwareStatus OutputSensor::getStatus() { return status_; }

uint8_t OutputSensor::backgroundProcessPicture() {
  updateBackground();
  return 0x00;
}

uint8_t OutputSensor::foregroundProcessPicture() {
  updateForeground();
  processForeground();
  return 0x00;
}

uint8_t OutputSensor::updateBackground() {
  cv::Mat dataPicture;
  camera_->takePicture(dataPicture);
  mosquito_image_proccesing_->update(dataPicture,
                                     MosquitoImageProccesingType::BACKGROUND);
  return 0x00;
}

uint8_t OutputSensor::updateForeground() {
  cv::Mat dataPicture;
  camera_->takePicture(dataPicture);
  mosquito_image_proccesing_->update(dataPicture,
                                     MosquitoImageProccesingType::FOREGROUND);
  return 0x00;
}

uint8_t OutputSensor::processForeground() {
  mosquitoes_count_ = -1;

  int pixels = mosquito_image_proccesing_->compute_pixel_count();
  double raw_mosquitoes =
      mosquito_image_proccesing_->compute_mosquitoes_from_pixels(
          pixels, 0.00166866, -11.94);  // setup 4
  mosquitoes_count_ =
      mosquito_image_proccesing_->compute_nice_mosquito_number_from_raw(
          raw_mosquitoes);
  return 0x00;
}

}  // hal
}  // wer
