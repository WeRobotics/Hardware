#include <hindge_door.h>

namespace wer {
namespace hal {

HindgeDoor* HindgeDoor::createHindgeDoor(ServoMotor* motor,
                                         int angle_position_deg_opened,
                                         int angle_position_deg_closed) {
  if (motor != nullptr) {
    return new HindgeDoor(motor, angle_position_deg_opened,
                          angle_position_deg_closed);
  } else {
    return nullptr;
  }
}

HindgeDoor::HindgeDoor(ServoMotor* motor, int angle_position_deg_opened,
                       int angle_position_deg_closed)
    : angle_position_deg_opened_(angle_position_deg_opened),
      angle_position_deg_closed_(angle_position_deg_closed) {
  motor_ = motor;
  status_ = HardwareStatus::CONNECTED;
}

HindgeDoor::~HindgeDoor() { delete motor_; }

void HindgeDoor::open() { motor_->setTargetAngle(angle_position_deg_opened_); }

void HindgeDoor::close() { motor_->setTargetAngle(angle_position_deg_closed_); }

}  // hal
}  // wer