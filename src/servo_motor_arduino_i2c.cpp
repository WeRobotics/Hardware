#include <servo_motor_arduino_i2c.h>

#include <iostream>
#include <string>

#include <yaml-cpp/yaml.h>

namespace wer {
namespace hal {

ServoMotorArduinoI2c::ServoMotorArduinoI2c(uint8_t address,
                                           uint8_t arduino_address,
                                           std::shared_ptr<I2c> i2c) {
  i2c_ = i2c;
  address_ = address;
  arduino_address_ = arduino_address;
  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");
  std::string max_angle_string =
      config["servo_arduino_heatlock"]["max_angle"].as<std::string>();
  max_angle_ = std::stod(max_angle_string);
  std::string min_angle_string =
      config["servo_arduino_heatlock"]["min_angle"].as<std::string>();
  min_angle_ = std::stod(min_angle_string);
}

ServoMotorArduinoI2c::~ServoMotorArduinoI2c() {}

uint8_t ServoMotorArduinoI2c::setTargetAngle(uint8_t angle) {
  if (angle > min_angle_ && angle < max_angle_) {
    uint8_t data[2] = {arduino_address_, angle};
    i2c_->set(address_, data, 2);
    return 0x00;
  } else {
    return 0x01;
  }
}

}  // hal
}  // wer