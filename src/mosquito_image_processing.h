#ifndef HARDWARE_INCLUDE_MOSQUITO_IMAGE_PROCESSING_H
#define HARDWARE_INCLUDE_MOSQUITO_IMAGE_PROCESSING_H

#include <future>
#include <string>

#include <opencv2/core.hpp>

namespace wer {
namespace hal {
enum class MosquitoImageProccesingType { BACKGROUND = 0, FOREGROUND };
class MosquitoImageProccesing {
 public:
  MosquitoImageProccesing(int diff_threshold, int small_opening_size,
                          int bigger_closing_size, bool use_biggest_cluster,
                          bool display = false, bool save = false);
  ~MosquitoImageProccesing();

  void init(cv::Mat background);

  void update(cv::Mat picture, MosquitoImageProccesingType type);

  /**
   * [compute_pixel_count takes a foreground image and calculate the number of
   * changing pixels]
   * @return [number of different pixels between background and foreground]
   */
  double compute_pixel_count();

  /**
   * [compute_mosquitoes_from_pixels calculates the estimated number of
   * mosquitoes based on the number of pixels that change between image and
   * background]
   * @param  nbr_white_pixels     []
   * @param  regression_slope     []
   * @param  regression_intersect []
   * @return                      []
   */
  double compute_mosquitoes_from_pixels(int nbr_white_pixels,
                                        double regression_slope,
                                        double regression_intersect);

  /**
   * [compute_nice_mosquito_number_from_raw transforms the result from
   * compute_mosquitoes_from_pixels into an integer with saturation at 0]
   * @param  mosquitoes []
   * @return            []
   */
  int compute_nice_mosquito_number_from_raw(double mosquitoes);

 private:
  /**
   * [find_biggest_cluster image is supposed to be a black white image (every
   * pixel that hasn't value 0 is assumend to be white). The 4 pointers are
   * optionnal (enables to get additionnal information)]
   * @param  image                             []
   * @param  tot_nbr_pixels                    []
   * @param  nbr_pixels_second_biggest_cluster []
   * @param  nbr_clusters                      []
   * @param  ID_of_biggest_cluster             []
   * @return                                   []
   */
  int find_biggest_cluster(cv::Mat image, int *tot_nbr_pixels,
                           int *nbr_pixels_second_biggest_cluster,
                           int *nbr_clusters, int *ID_of_biggest_cluster);
  static void linearRegression(
      double *x, double *y, int nbr_samples, double &a, double &b,
      double &r);  // fit y=ax+b    //explanations and notations in 3.1 of
  // https://www.math.univ-toulouse.fr/~besse/Wikistat/pdf/st-l-inf-regsim.pdf
  static int ROI_x_min(
      int y);  // define the precise boundaries of the region of interest
  static int ROI_x_max(
      int y);  // define the precise boundaries of the region of inte$
  static int interpolate(int y, int a1, int f1, int a2, int f2);

  void save_image(cv::Mat image, std::string base_path, int number);

  cv::Mat background_;
  cv::Mat foreground_;
  cv::Mat prev_raw_backgrounds_[5];
  int background_circular_index_;

  bool save_;
  int diff_threshold_;
  int small_opening_size_;
  int bigger_closing_size_;
  bool use_biggest_cluster_;
  /**
   * if true, we display the images corresponding to all processing steps and
   * wait for any key to be pressed to continue : to be used only for debugging.
   */
  bool display_;
  static const int mechanism_setup_ = 4;
};
}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_MOSQUITO_IMAGE_PROCESSING_H