#include <humidity_sensor_hih6100.h>

#include <iostream>
#include <limits>

#include "log.h"

namespace wer {
namespace hal {
  
std::shared_ptr<hal::Sensor> HumiditySensorHih6100::createInstance(
    std::shared_ptr<Hih6100> hih6100, float sample_rate) {
  return std::make_shared<HumiditySensorHih6100>(hih6100, sample_rate);
}

HumiditySensorHih6100::HumiditySensorHih6100(std::shared_ptr<Hih6100> hih6100,
                                             float sample_rate)
    : hih6100_(hih6100) {
  hih6100_->enable(sample_rate);
  type_ = SensorType::HUMIDITY;
  status_ = HardwareStatus::CONNECTED;
  units_ = "%";
}

HumiditySensorHih6100::~HumiditySensorHih6100() { hih6100_->disable(); }

uint8_t HumiditySensorHih6100::getData(float& data) {
  if (this->getStatus() == HardwareStatus::CONNECTED) {
    data = hih6100_->humidity();
    return 0x00;
  } else
    return 0xFF;
}

HardwareStatus HumiditySensorHih6100::getStatus() {
  if (hih6100_->connected()) {
    return HardwareStatus::CONNECTED;
  } else {
    return HardwareStatus::DISCONNECTED;
  }
}

}  // namespace hal
}  // namespace wer
