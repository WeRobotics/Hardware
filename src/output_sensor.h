#ifndef HARDWARE_INCLUDE_OUTPUT_SENSOR_H
#define HARDWARE_INCLUDE_OUTPUT_SENSOR_H

#include "sensor.h"

#include <functional>

#include <camera.h>
#include <hardware_status.h>
#include <mosquito_image_processing.h>

namespace wer {
namespace hal {
/**
* This class is implementing the output sensor. It uses camera.h interface to
* communicate with hardware camera. It estimates a number of mosquitos released
* after heatlock compartment, based on the image processing. It is developed to
* be used inside the OutputUnit mechanism. It is not implementing Sensor class,
* but it is using HardwareStatus flags.
*/
class OutputSensor {
 public:
  static OutputSensor* createOutputSensor(std::shared_ptr<Camera> camera);
  ~OutputSensor();

  uint8_t getData(int& Data);
  HardwareStatus getStatus();

  uint8_t backgroundProcessPicture();
  uint8_t foregroundProcessPicture();

 private:
  OutputSensor(std::shared_ptr<Camera> camera);
  uint8_t updateBackground();

  uint8_t updateForeground();
  uint8_t processForeground();

  std::shared_ptr<Camera> camera_;

  int32_t mosquitoes_count_;
  MosquitoImageProccesing* mosquito_image_proccesing_;

  HardwareStatus status_;
};
}
}

#endif  // HARDWARE_INCLUDE_OUTPUT_SENSOR_H