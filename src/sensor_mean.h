#ifndef HARDWARE_INCLUDE_SENSOR_MEAN_H
#define HARDWARE_INCLUDE_SENSOR_MEAN_H

#include "sensor.h"

#include <sys/types.h>

#include <atomic>
#include <chrono>

#include <call_every_handler.h>

#include "hardware_status.h"
#include "i2c.h"
#include "sensor.h"

namespace wer {
namespace hal {

/**
 * This class is used to average multiple sensor classes, it
 * implements the Sensor interface.
 */
class SensorMean : public Sensor {
 public:
 	// TODO generalise for >= 2 sensors
  SensorMean(std::vector<std::shared_ptr<hal::Sensor>> sensors);
  // SensorMean(std::shared_ptr<hal::Sensor> sensor1,
  //                        std::shared_ptr<hal::Sensor> sensor2);
  ~SensorMean();

  uint8_t getData(float& data) override;

  HardwareStatus getStatus() override;

 private:
  std::vector<std::shared_ptr<hal::Sensor>> sensors_;
  // std::shared_ptr<hal::Sensor> sensor1_ = nullptr;
  // std::shared_ptr<hal::Sensor> sensor2_ = nullptr;
};
}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_INCLUDE_SENSOR_MEAN_H
