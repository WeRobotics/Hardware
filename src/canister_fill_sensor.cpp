#include <canister_fill_sensor.h>

#include <iostream>
#include <limits>

#include "log.h"

namespace wer {
namespace hal {

CanisterFillSensor* CanisterFillSensor::createCanisterFillSensor(
    std::shared_ptr<wer::lib::CallEveryHandler> callEveryHandler,
    std::shared_ptr<I2c> i2c, uint8_t address,
    std::vector<double> interpolationCoefficients, float sample_rate) {
  uint8_t buffer[1];
  if (i2c->get(address, buffer, 1) == 0x00) {
    return new CanisterFillSensor(callEveryHandler, i2c, address,
                                  interpolationCoefficients, sample_rate);
  } else {
    return nullptr;
  }
}
CanisterFillSensor::CanisterFillSensor(
    std::shared_ptr<wer::lib::CallEveryHandler> callEveryHandler,
    std::shared_ptr<I2c> i2c, uint8_t address,
    std::vector<double> interpolationCoefficients, float sample_rate)
    : callEveryHandler_(callEveryHandler),
      i2c_(i2c),
      address_(address),
      interpolationCoefficients_(interpolationCoefficients) {
  type_ = SensorType::CANISTER_FILL;
  units_ = "%";
  callEveryHandler_->add(std::bind(&CanisterFillSensor::cycle, this),
                         sample_rate, &cookie_);
  status_ = HardwareStatus::CONNECTED;
}

CanisterFillSensor::~CanisterFillSensor() {
  callEveryHandler_->remove(cookie_);
}

void CanisterFillSensor::cycle() {
  uint8_t data;
  uint8_t* datap = &data;
  if (i2c_->get(address_, datap, 1) == 0x00) {
    status_ = HardwareStatus::CONNECTED;
    fill_percentage_ = estimate(*datap);
  }
}

double CanisterFillSensor::estimate(uint8_t raw_value) {
  double est = 0;
  for (int i = interpolationCoefficients_.size() - 1; i >= 0; i--) {
    est = est * raw_value + interpolationCoefficients_[i];
  }

  return est;
}

HardwareStatus CanisterFillSensor::getStatus() { return status_; }

uint8_t CanisterFillSensor::getData(float& Data) {
  if (status_ == HardwareStatus::CONNECTED) {
    Data = fill_percentage_;
    return 0x00;
  } else
    return 0xFF;
}
}  // hal
}  // wer