#ifndef POWER_MODULE_H_
#define POWER_MODULE_H_


#include <cstring>
#include <vector>

#include "ads1X15.h"

namespace wer {
namespace hal {

class PowerModule {

 public:
	
	//Constructor
	PowerModule(std::shared_ptr<Ads1115> ads);
	//Destructor
	~PowerModule();

	/**
	* [Gets the current battery state of charge.]
	* @param    []
	* @return   []
	*/
	double GetBatteryCharge();

	/**
	* [Gets the current battery temperature.]
	* @param    []
	* @return   []
	*/
	double GetBatteryTemperature();
	
	/**
	* [Reads, converts and saves in batteryTemperature one temperature measurement.]
	* @param    []
	* @return   []
	*/
	void TemperatureMeasurement();

	/**
	* [Reads, converts and saves in batteryVoltage one voltage measurement.]
	* @param    []
	* @return   []
	*/
	void VoltageMeasurement();

	/**
	* [Calls a home-made function in the ADS library to send the right parameters to the ADS1115.
	*  Also, creates the log files in which voltages and temperatures will be written.]
	* @param    []
	* @return   []
	*/
	void Setup(void);


	const uint8_t kChannel1 = 0;
	const uint8_t kChannel2 = 1;

	const double kAdcVoltageToBatteryVoltage = 4.9;
	const double kMaxVoltage = 6.144;


 private:

 	/**
	* [Creates a file with the current date and time (obtained via ethernet).
	*  If no ethernet connection is available, the time will not be accurate]
	* @param    []	
	* @return   [0 or 1] 0 if the file was correctly created, 1 if not
	*/
 	// uint8_t CreateFile();

 	/**
	* [Compute the state of charge based on the battery voltage]
	* @param    []	
	* @return   []
	*/
 	void ComputeStateOfCharge();

 	/**
	* [Converts the 15-bit int data value read by the ADC to its equivalent voltage.]
	* @param    [data]	the received data
	* @return   [double voltage] The equivalent voltage
	*/
	double IntToVoltage(uint16_t data);

 	/**
	* [Starts the comparator, used to light up the LED if the temperature is too high. NOT WORKING AT THE MOMENT.]
	* @param    [low_threshold]	if that value is reached, the LED will turn OFF
	* 			[high_treshold]	if that value is reached, the LED will turn ON
	* @return   []
	*/
	void StartComparator(uint16_t low_threshold, uint16_t high_treshold);

	 /**
	* [Update time information and store it in the member timeVect.]
	* @param    [start]	boolean indicating whether the whole time vector
	* 			 needs to be changed, or if only the fields h, min & sec need
	* 			 to be changed
	* @return   []
	*/
 	void UpdateTime(bool start);

	/**
	* [Converts the voltage measured by the ADC to its equivalent temperature.
	*  From the voltage, the thermistor value is deduced, and the temperature
	*  is then computed based on fixed values stored in the class.]
	* @param    [voltage]	the voltage measured by the ADC
	* @return   [double temperature] The temperature of the thermistor
	*/
	double VoltageToTemperature(double voltage);

 	/**
	* [Writes all the information to the LOG file. ]
	* @param    [data_raw]	the received raw data from ADC
	* 			[data_volt]	voltage measured by ADC after conversion
	* 			[data_physical]	the physical value (battery temp or volt) after converting dataVolt
	* 			[data_type]	the type of data (used to write the correct sentence in the log file).
	* 						0 for voltage, 1 for temperature
	* @return   [int] 0 if succeeded, 1 if failed
	*/
	// uint8_t WriteToFile(uint16_t data_raw, double data_volt, double data_physical, uint8_t data_type);

	//ADS class for ads1115
	std::shared_ptr<Ads1115> ads_;

 	std::string fileName_; // to create file with unique name


	std::vector<uint16_t> adc_val_; //to store values on both channels
	std::vector<double> adc_voltage_; //stores adcVal converted to Voltage
 	std::vector<int> time_vect_; //to store time for logging ADC info

	//hold ADC values after all the conversions
	double battery_temperature_;
	double battery_voltage_; 		 // in [V]
	double battery_state_of_charge_; // in [%]

	bool verbose_;


	const std::vector<double> kThermistorValues_ = {1252.0, 1476.0, 1748.0, 2079.0, 2484.0, 2982.0, 3599.0, 4365.0, 5324.0,
												  6530.0, 8086.0, 10000.0, 12493.0, 15711.0, 19897.0, 25381.0, 32624.0};
	const std::vector<double> kTemperatureValues_ = {80.0, 75.0, 70.0, 65.0, 60.0, 55.0, 50.0, 45.0, 40.0, 35.0, 30.0,
												   25.0, 20.0, 15.0, 10.0, 5.0, 0.0};
};

}}

#endif // POWER_MODULE_H_