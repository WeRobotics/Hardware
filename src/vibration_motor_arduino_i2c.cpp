#include <vibration_motor_arduino_i2c.h>

namespace wer {
namespace hal {
VibrationMotorArduinoI2c::VibrationMotorArduinoI2c(uint8_t address,
                                                   uint8_t arduino_address,
                                                   std::shared_ptr<I2c> i2c) {
  i2c_ = i2c;
  address_ = address;
  arduino_address_ = arduino_address;
}
void VibrationMotorArduinoI2c::setVibration(uint8_t on_off, double intensity) {
  uint8_t data[2] = {arduino_address_, on_off};
  i2c_->set(address_, data, 2);
}

VibrationMotorArduinoI2c::~VibrationMotorArduinoI2c() {}
}  // hal
}  // wer