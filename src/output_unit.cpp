#include "output_unit.h"

#include <boost/thread.hpp>

#include <log.h>

namespace wer {
namespace hal {
OutputUnit* OutputUnit::createOutputUnit(Heatlock* heatlock,
                                         OutputSensor* output_sensor) {
  if (output_sensor && heatlock) {
    return new OutputUnit(heatlock, output_sensor);
  } else {
    return nullptr;
  }
}

OutputUnit::OutputUnit(Heatlock* heatlock, OutputSensor* output_sensor) {
  heatlock_ = heatlock;
  output_sensor_ = output_sensor;
  command_open_unit_ = 0x00;
  command_loading_finished_ = 0x00;
  state_ = OutputUnitState::READY;
  status_ = OutputUnitStatus::UNIT_READY;
  startThread();
}

OutputUnit::~OutputUnit() {
  stopThread();
  delete heatlock_;
  delete output_sensor_;
}

uint8_t OutputUnit::commandOpenUnit() {
  uint8_t return_status = 0x00;
  if (state_ == OutputUnitState::READY) {
    command_open_unit_ = 0x01;
    return_status = 0x00;
  } else {
    return_status = 0x01;
  }
  return return_status;
}

uint8_t OutputUnit::commandLoadingFinished() {
  uint8_t return_status = 0x00;
  if (state_ == OutputUnitState::LOADING) {
    command_loading_finished_ = 0x01;
    return_status = 0x00;
  } else {
    return_status = 0x01;
  }
  return return_status;
}

OutputUnitStatus OutputUnit::sendRequest() {
  request_received_ = 0x01;
  if (state_ == OutputUnitState::READY) {
    return OutputUnitStatus::UNIT_READY;
  } else {
    return OutputUnitStatus::UNIT_BUSY;
  }
}

void OutputUnit::startThread() {
  if (state_machine_thread_ == NULL) {
    LogInfo() << "Output unit thread started";
    run_thread_ = 0x01;
    state_machine_thread_ =
        new boost::thread(&OutputUnit::runStateMachine, this);
  }
}

void OutputUnit::stopThread() {
  if (state_machine_thread_) {
    run_thread_ = 0x00;
    state_machine_thread_->join();
    delete state_machine_thread_;
    state_machine_thread_ = NULL;
    LogInfo() << "Output unit thread stopped";
  }
}
void OutputUnit::runStateMachine() {
  while (run_thread_) {
    switch (state_) {
      case OutputUnitState::READY:
        if (status_ != OutputUnitStatus::UNIT_READY) {
          status_ = OutputUnitStatus::UNIT_READY;
        }
        if (command_open_unit_) {
          state_ = OutputUnitState::LOADING;
          heatlock_->open();
          command_open_unit_ = 0x00;
        }
        break;
      case OutputUnitState::LOADING:
        if (status_ != OutputUnitStatus::UNIT_BUSY) {
          status_ = OutputUnitStatus::UNIT_BUSY;
        }
        if (command_loading_finished_) {
          heatlock_->close();
          state_ = OutputUnitState::FOREGROUND_PICTURE;
          command_loading_finished_ = 0x00;
        }
        break;
      case OutputUnitState::FOREGROUND_PICTURE:
        LogInfo() << "foreground pic";
        if (!(output_sensor_->foregroundProcessPicture())) {
          state_ = OutputUnitState::RELEASE;
        }
        break;
      case OutputUnitState::RELEASE:
        LogInfo() << "release";
        heatlock_->release();
        state_ = OutputUnitState::BACKGROUND_PICTURE;
        break;
      case OutputUnitState::BACKGROUND_PICTURE:
        LogInfo() << "background pic";
        if (!(output_sensor_->backgroundProcessPicture())) {
          state_ = OutputUnitState::READY;
        }
        break;
    }
    boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
  }
}

}  // hal
}  // wer