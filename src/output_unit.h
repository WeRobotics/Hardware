#ifndef HARDWARE_INCLUDE_OUTPUT_UNIT_H
#define HARDWARE_INCLUDE_OUTPUT_UNIT_H

#include <string>

#include <boost/thread.hpp>

#include "heatlock.h"
#include "output_sensor.h"

namespace wer {
namespace hal {

enum class OutputUnitState {
  READY = 0,
  LOADING,
  FOREGROUND_PICTURE,
  RELEASE,
  BACKGROUND_PICTURE
};
/**
 * This class implements Output unit. It consists of heatlock and sensor which
 * is measuring number of mosquitos released. It is a separate thread, which is
 * working in parallel with the main thread. This unit works as a state machine,
 * it executes tasks and when it finishes, it enters the READY state.
 */
enum class OutputUnitStatus { NONINITIALIZED = 0, UNIT_READY, UNIT_BUSY };

inline const std::string OutputUnitStatusToString(const OutputUnitStatus s) {
  switch (s) {
    case OutputUnitStatus::NONINITIALIZED:
      return "Noninitilized";
    case OutputUnitStatus::UNIT_READY:
      return "Unit ready";
    case OutputUnitStatus::UNIT_BUSY:
      return "Unit busy";
  }
  return "Non existing status";
}

class OutputUnit {
 public:
  static OutputUnit* createOutputUnit(Heatlock* heatlock,
                                      OutputSensor* output_sensor);
  ~OutputUnit();
  /**
   * [commandOpenUnit sends command which will open the unit and let mosquitos
   * to get inside a heatlock and load a heatlock]
   * @return [0x00 if everything is ok, or 0x01 if there is a problem]
   */
  uint8_t commandOpenUnit();

  /**
   * [commandLoadingFinished this command should be sent to the OutputUnit when
   * loading of mosquitos inside the healock is done]
   * @return [0x00 if everything is ok, or 0x01 if there is a problem]
   */
  uint8_t commandLoadingFinished();
  /**
   * [sendRequest will send a request to the OutputUnit. The request will be
   * saved and acknowledged internally inside OutputUnit]
   * @return [OutputUnitStatus will be UNIT_READY if ready or UNIT_BUSY if it
   * is not ready]
   */
  OutputUnitStatus sendRequest();

 private:
  OutputUnit(Heatlock* heatlock, OutputSensor* output_sensor);
  void startThread();
  void stopThread();
  /**
   * [run_state_machine is a thread function]
   */
  void runStateMachine();

  Heatlock* heatlock_;
  OutputSensor* output_sensor_;

  OutputUnitStatus status_;
  OutputUnitState state_;

  volatile uint8_t command_open_unit_;
  volatile uint8_t command_loading_finished_;
  uint8_t request_received_;

  boost::thread* state_machine_thread_ = NULL;
  uint8_t run_thread_ = 0x00;
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_OUTPUT_UNIT_H