#ifndef HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_CALIBRATION_H
#define HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_CALIBRATION_H

/**
 * This is the calibration file for CanisterFillSensor. Here are stored the
 * values needed for interpolation of each canister. It is polynomial
 * interpolation of the first order. p(x)=x_1*x+x_0
 */

#include <vector>

namespace wer {
namespace hal {

struct CanisterFillSensorCalibration {
  const double Canister1_x1 = 0.01205;
  const double Canister1_x0 = -0.68685;

  const double Canister2_x1 = 1;
  const double Canister2_x0 = 0;

  const double Canister3_x1 = 0.0097;
  const double Canister3_x0 = -0.4854;

  const double Canister4_x1 = 1;
  const double Canister4_x0 = 0;
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_CANISTER_FILL_SENSOR_CALIBRATION_H