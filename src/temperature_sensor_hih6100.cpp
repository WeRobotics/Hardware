#include <temperature_sensor_hih6100.h>

#include <iostream>
#include <limits>

#include "log.h"

namespace wer {
namespace hal {

std::shared_ptr<Sensor> TemperatureSensorHih6100::createInstance(
    std::shared_ptr<Hih6100> hih6100, float sample_rate) {
  return std::make_shared<TemperatureSensorHih6100>(hih6100, sample_rate);
}

TemperatureSensorHih6100::TemperatureSensorHih6100(
    std::shared_ptr<Hih6100> hih6100, float sample_rate)
    : hih6100_(hih6100) {
  hih6100_->enable(sample_rate);
  type_ = SensorType::HUMIDITY;
  units_ = "°C";
}

TemperatureSensorHih6100::~TemperatureSensorHih6100() { hih6100_->disable(); }

uint8_t TemperatureSensorHih6100::getData(float& data) {
  if (this->getStatus() == HardwareStatus::CONNECTED) {
    data = hih6100_->temperature();
    return false;
  } else
    return true;
}

HardwareStatus TemperatureSensorHih6100::getStatus() {
  if (hih6100_->connected()) {
    return HardwareStatus::CONNECTED;
  } else {
    return HardwareStatus::DISCONNECTED;
  }
}
}  // namespace hal
}  // namespace wer
