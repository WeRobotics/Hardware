#include "connection.h"

wer::hal::Connection::Connection()
{

}

wer::hal::Connection::~Connection()
{
    _callback = {};
}

void wer::hal::Connection::registerCallback(std::function<void(uint8_t*, uint16_t)> callback) {
    _callback = callback;
}

void wer::hal::Connection::registerTimeoutCallback(std::function<void()> timeoutCallback) {
	_timeoutCallback = timeoutCallback;
}

void wer::hal::Connection::receiveMessage(uint8_t* data, uint16_t dataLength)
{
    if(_callback) {
        _callback(data, dataLength);
    }
}
