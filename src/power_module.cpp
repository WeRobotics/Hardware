#include <stdio.h>
#include <time.h>       /* time_t, struct tm, time, localtime */
#include <unistd.h>

#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>

// #include "ads1015_adafruit.h"
#include "power_module.h"

namespace wer {
namespace hal {

PowerModule::PowerModule(std::shared_ptr<Ads1115> ads){

  ads_ = ads;

  time_vect_.resize(6,0);
  adc_val_.resize(2,0);
  adc_voltage_.resize(2,0.0);

  verbose_ = false;

  Setup();
}

PowerModule::~PowerModule(){};



// uint8_t PowerModule::CreateFile(){

//   //Get time first
//   UpdateTime(true);
  
//   fileName_ = "TemperatureTest_" + std::to_string(time_vect_[2]) + "-" + std::to_string(time_vect_[1]) + "-" +  std::to_string(time_vect_[0]) + "_"
//              + std::to_string(time_vect_[3]) + "-" + std::to_string(time_vect_[4]) + "-" + std::to_string(time_vect_[5]) + ".txt";

//   if(verbose_){
//     std::cout << fileName_ << std::endl;
//   }

//   std::ofstream my_file;
//   my_file.open(fileName_);
  
//   if (my_file.is_open()){
//     return 0;
//   }else{
//     return 1;
//   }
// }


void PowerModule::ComputeStateOfCharge(){
  battery_state_of_charge_ = -1.0; //add computation when curve is determined
}



double PowerModule::GetBatteryCharge(){
  VoltageMeasurement();
  battery_state_of_charge_ = round((battery_voltage_-16.0)*5)*5; // very simple linear model 20.5V == 100%, 16.5V == 0%, 5% accuracy
  if (battery_state_of_charge_ < 0.0){
    battery_state_of_charge_ = 0.0;
  }
  else if (battery_state_of_charge_ > 100.0){
    battery_state_of_charge_ = 100.0;
  }
  return battery_state_of_charge_;
}


double PowerModule::GetBatteryTemperature(){
  TemperatureMeasurement();
  return battery_temperature_;
}


double PowerModule::IntToVoltage(uint16_t data){
  //transform the uint16_t to a voltage (assumes the gain is 2/3)
  double voltage = data / 32768.0 * double(kMaxVoltage);
  //printf("New computed voltage: %f \n", voltage);
  return voltage;
}



double PowerModule::VoltageToTemperature(double voltage){
  if (voltage == 0){ // no reading
    return 0;
  }

  double resistance = voltage * 10000.0 / (5.0 - voltage);
   if(verbose_) printf("v= %f\n", voltage);
  if(verbose_) printf("R= %f\n", resistance);


  auto lower = std::lower_bound(kThermistorValues_.begin(), kThermistorValues_.end(), resistance);

  //if too hot
  if (lower == kThermistorValues_.begin() && resistance < kThermistorValues_[0]) {
    return 80;
  }

  //if too cold
  if (lower == kThermistorValues_.end() && resistance > kThermistorValues_.back()) {
    return 0;
  }

  //since the T and thermValues are "flipped", the differences appear "flipped as well"
  double diff_x = *(lower) - *(lower - 1);
  double diff_y = 5.0;
  double slope = diff_y / diff_x;

  double temperature = kTemperatureValues_[lower-1-kThermistorValues_.begin()] - slope* (resistance - *(lower - 1));
  return temperature;
}



void PowerModule::StartComparator(uint16_t low_threshold, uint16_t high_treshold){
  // ads_->startComparator_SingleEnded(kChannel2, low_threshold, high_treshold);
}



void PowerModule::Setup(){
  //Set gain & begin
  // ads_->setGain(GAIN_TWOTHIRDS);

  //Create log file
  // CreateFile();
}



void PowerModule::TemperatureMeasurement(){
    //get raw uint16_t from ADC
    adc_val_[kChannel2] = ads_->readADC_SingleEnded(kChannel2);
    //conversion to voltage
    adc_voltage_[kChannel2] = IntToVoltage(adc_val_[kChannel2]);
    //conversion to °C
    battery_temperature_ = VoltageToTemperature(adc_voltage_[kChannel2]);

    // WriteToFile(adc_val_[kChannel2], adc_voltage_[kChannel2], battery_temperature_, kChannel2);
    if(verbose_){
      std::cout << "Computed temperature: " << battery_temperature_ << " °C" << std::endl;
    }
}



void PowerModule::UpdateTime(bool start){

  time_t t = time(NULL);
  struct tm  tms = * localtime(&t);

  //i.e. no need of year month day
  if(start == 0){
    time_vect_[3] = tms.tm_hour;
    time_vect_[4]  = tms.tm_min;
    time_vect_[5]  = tms.tm_sec;
  }else{
    time_vect_[0] = tms.tm_year+1900; 
    time_vect_[1]  = tms.tm_mon+1;
    time_vect_[2]  = tms.tm_mday;
    time_vect_[3] = tms.tm_hour;
    time_vect_[4]  = tms.tm_min;
    time_vect_[5]  = tms.tm_sec;
  }
}



void PowerModule::VoltageMeasurement(){
    //get raw uint16_t from ADC
    adc_val_[kChannel1] = ads_->readADC_SingleEnded(kChannel1);
    //conversion to voltage
    adc_voltage_[kChannel1] = IntToVoltage(adc_val_[kChannel1]);
    //conversion to °C
    battery_voltage_ = adc_voltage_[kChannel1] * kAdcVoltageToBatteryVoltage;

    // WriteToFile(adc_val_[kChannel1], adc_voltage_[kChannel1], battery_voltage_, kChannel1);
    if(verbose_){
      printf("Battery Voltage = %f\n", battery_voltage_);
    }
}



// uint8_t PowerModule::WriteToFile(uint16_t data_raw, double data_volt, double data_physical, uint8_t data_type){

//   //Get time first
//   UpdateTime(false);

//   std::ofstream my_file;
//   my_file.open(fileName_, std::ios::binary | std::ios::out | std::ios::app);

//   if (my_file.is_open()){
//     my_file <<  "Time: " << std::to_string(time_vect_[3]) + "-" + std::to_string(time_vect_[4]) + "-" +  std::to_string(time_vect_[5]);
//     if(data_type == kChannel1){
//       my_file << "  Voltage= ";
//     }else{
//       my_file << "  Temperature= ";
//     }
//     //myFile << std::to_string(b[1] << 8 | b[0]) << std::endl;
//     my_file << std::to_string(data_raw) << " adsVoltage= " << data_volt;

//     if(data_type == kChannel1){
//       my_file << " BatteryVoltage= ";
//     }else{
//       my_file << " BatteryTemperature= ";
//     }
//     my_file << std::to_string(data_physical) << std::endl;
//     my_file.close();
//   }else{
//     std::cout << "ERROR COULD NOT OPEN FILE IN writeToFile" << std::endl;
//     return 1;
//   }
//   return 0;
// }

}}