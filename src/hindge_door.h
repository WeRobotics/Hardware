#ifndef HARDWARE_INCLUDE_HINDGE_DOOR
#define HARDWARE_INCLUDE_HINDGE_DOOR

#include <door.h>

#include <stdint.h>

#include <servo_motor.h>

namespace wer {
namespace hal {
/**
 * This class is interface for the simple hindged doors.
 */
class HindgeDoor : public Door {
 public:
  static HindgeDoor* createHindgeDoor(ServoMotor* motor,
                                      int angle_position_deg_opened,
                                      int angle_position_deg_closed);

  ~HindgeDoor();

  void open() override;
  void close() override;

 private:
  HindgeDoor(ServoMotor* motor, int angle_position_deg_opened,
             int angle_position_deg_closed);
  ServoMotor* motor_;
  const int angle_position_deg_opened_;
  const int angle_position_deg_closed_;
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_HINDGE_DOOR