# Harware abstraction

In order for the Hardware git to compile:

1. Install boost libraries on the machine where the code will be used 

sudo apt install libboost-all-dev

sudo apt install aptitude

2. Install google test libraries on the machine where the code will be used

sudo apt-get install libgtest-dev

cd /usr/src/gtest

sudo cmake CMakeLists.txt

sudo make
 
copy libgtest.a and libgtest_main.a to your /usr/lib folder
sudo cp *.a /usr/lib


3. Install openCV libraries on the machine where the code will be used 

https://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html

Usually libraries from 1,2,3 should be installed in folder /usr/local/include and usr/local/lib

4. install yaml-cpp library

clone library from 
https://github.com/jbeder/yaml-cpp

compile the library and copy it inside usr/local/lib

Commands:

To compile and run tests, inside hardware folder run script run.sh:
./run.sh  - this will make a new makefile, compile and run tests
./run.sh short - this will compile and run tests

To make new makefile, delete build/ folder and:
mkdir build
cd build
cmake ..

To compile, inside build/ folder:
make all

After compilation to run tests:
./unit_tests
