/****************************************************************************
 *
 *   Copyright (c) 2016-2018 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include "iridium_sbd.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Library/logging/log.h>

static constexpr const char *satcom_state_string[4] = {"STANDBY", "SIGNAL CHECK", "SBD SESSION", "TEST"};

wer::com::IridiumSBD::IridiumSBD(wer::hal::Connection* connection, uint8_t priority, std::string name) :
Link(priority, name),
_connection(connection)
{
	_connection->registerCallback(std::bind(&wer::com::IridiumSBD::processIncomingData, this, std::placeholders::_1, std::placeholders::_2));

	_param_read_interval_s = 0;
	_param_session_timeout_s = 60;
	_param_stacking_time_ms = 0;
}

wer::com::IridiumSBD::~IridiumSBD() {
	_should_exit = true;
	
	if(_sbd_thread && _sbd_thread->joinable()) {
		_sbd_thread->join();
		delete(_sbd_thread);
		_sbd_thread = nullptr;
	}
}

void wer::com::IridiumSBD::start() {
	_sbd_thread = new std::thread(&wer::com::IridiumSBD::main_loop, this);
}

///////////////////////////////////////////////////////////////////////
// public functions                                                  //
///////////////////////////////////////////////////////////////////////

void wer::com::IridiumSBD::status()
{
	LogIridium() << "state: " << get_satcom_state_string(_state);

	auto now = std::chrono::steady_clock::now();

	LogIridium() << "TX buf written:               "  << _tx_buf_write_idx;
	LogIridium() << "Signal quality:               "  << _signal_quality;
	LogIridium() << "TX session pending:           "  << _tx_session_pending;
	LogIridium() << "RX session pending:           "  << _rx_session_pending;
	LogIridium() << "RX read pending:              "  << _rx_read_pending;
	LogIridium() << "Time since last signal check: "  << std::chrono::duration_cast<std::chrono::milliseconds>(now - _last_signal_check).count();
	LogIridium() << "Last heartbeat:               "  << std::chrono::duration_cast<std::chrono::milliseconds>(now - _last_heartbeat).count();
}

const char* wer::com::IridiumSBD::get_satcom_state_string(satcom_state state) {
	switch (state)
	{
		case satcom_state::SATCOM_STATE_STANDBY:
			return satcom_state_string[0];
			break;

		case satcom_state::SATCOM_STATE_CSQ:
			return satcom_state_string[1];
			break;

		case satcom_state::SATCOM_STATE_SBDSESSION:
			return satcom_state_string[2];
			break;

		case satcom_state::SATCOM_STATE_TEST:
			return satcom_state_string[3];
			break;

		
		default:
			return "Unknown...";
			break;
	}
}

///////////////////////////////////////////////////////////////////////
// private functions                                                 //
///////////////////////////////////////////////////////////////////////

void wer::com::IridiumSBD::main_loop()
{
	// disable flow control
	bool command_executed = false;

	for (int counter = 0; (counter < 20) && !command_executed; counter++) {
		write_at("AT&K0");

		if (read_at_command() != satcom_result_code::SATCOM_RESULT_OK) {
			
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(100ms);

		} else {
			LogIridium() << "Satellite AT&K0....   ok";
			command_executed = true;
		}
	}

	if (!command_executed) {
		LogIridium() << "Modem not responding";
		_should_exit = true;
		return;
	}

	// disable command echo
	command_executed = false;

	for (int counter = 0; (counter < 10) && !command_executed; counter++) {
		write_at("ATE0");

		if (read_at_command() != satcom_result_code::SATCOM_RESULT_OK) {
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(100ms);

		} else {
			command_executed = true;
		}
	}

	if (!command_executed) {
		LogIridium() << "Modem not responding";
		_should_exit = true;
		return;
	}

	//_param_read_interval_s
	//_param_session_timeout_s
	//_param_stacking_time_ms

	if (_param_session_timeout_s < 0) {
		_param_session_timeout_s = 60;
	}

	if (_param_stacking_time_ms < 0) {
		_param_stacking_time_ms = 0;
	}

	LogIridium() << "read interval: " << _param_read_interval_s << " s";
	LogIridium() << "SBD session timeout: " << _param_session_timeout_s << " s";
	LogIridium() << "SBD stack time: " << _param_stacking_time_ms << " ms";

	_start_completed = true;

	while(_should_exit == false) {

		switch (_state) {
			case satcom_state::SATCOM_STATE_STANDBY:
				standby_loop();
				break;

			case satcom_state::SATCOM_STATE_CSQ:
				csq_loop();
				break;

			case satcom_state::SATCOM_STATE_SBDSESSION:
				sbdsession_loop();
				break;

			case satcom_state::SATCOM_STATE_TEST:
				test_loop();
				break;
		}

		if (_new_state != _state) {
			LogIridium() << "SWITCHING STATE FROM " << get_satcom_state_string(_state) << " TO " << get_satcom_state_string(_new_state);
			_state = _new_state;
			//publish_iridium_status();

		} else {
			//publish_iridium_status();
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(100ms);	// 100ms
		}
	}
}

void wer::com::IridiumSBD::standby_loop(void)
{
	if (_test_pending) {
		_test_pending = false;

		if (!strcmp(_test_command, "s")) {
			write((const uint8_t*) "kreczmer", 8);

		} else if (!strcmp(_test_command, "read")) {
			_rx_session_pending = true;

		} else {
			_test_timer = std::chrono::steady_clock::now();
			start_test();
			return;
		}
	}

	// check for incoming SBDRING, handled inside read_at_command()
	read_at_command();

	if (_param_read_interval_s > 0
	    && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _last_read_time).count() > _param_read_interval_s * 1000)) {
		_rx_session_pending = true;
	}

	// write the MO buffer when the message stacking time expires
	if (_tx_buf_write_pending && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _last_write_time).count() > _param_stacking_time_ms)) {
		write_tx_buf();
	}

	// do not start an SBD session if there is still data in the MT buffer, or it will be lost
	if ((_tx_session_pending || _rx_session_pending) && !_rx_read_pending) {
		if (_signal_quality > 0) {
			// clear the MO buffer if we only want to read a message
			if (_rx_session_pending && !_tx_session_pending) {
				if (clear_mo_buffer()) {
					start_sbd_session();
					return;
				}

			} else {
				start_sbd_session();
				return;
			}

		} else {
			start_csq();
			return;
		}
	}

	// start a signal check if requested and not a switch to another mode is scheduled
	auto now = std::chrono::steady_clock::now();
	
	if ((std::chrono::duration_cast<std::chrono::milliseconds>(now - _last_signal_check).count() > SATCOM_SIGNAL_REFRESH_DELAY)
	    && (_new_state == satcom_state::SATCOM_STATE_STANDBY)) {
		start_csq();

		return;
	}

	// only read the MT buffer if the higher layer (mavlink app) read the previous message
	if (_rx_read_pending && (_rx_msg_read_idx == _rx_msg_end_idx) && (_new_state == satcom_state::SATCOM_STATE_STANDBY)) {
		read_rx_buf();
		return;
	}
}

void wer::com::IridiumSBD::csq_loop(void)
{
	satcom_result_code res = read_at_command();

	if (res == satcom_result_code::SATCOM_RESULT_NA) {
		return;
	}

	if (res != satcom_result_code::SATCOM_RESULT_OK) {
		LogIridium() << "UPDATE SIGNAL QUALITY: ERROR";

		_new_state = satcom_state::SATCOM_STATE_STANDBY;
		return;
	}

	if (strncmp((const char *)_rx_command_buf, "+CSQ:", 5)) {
		LogIridium() << "UPDATE SIGNAL QUALITY: WRONG ANSWER:";
		LogIridium() << _rx_command_buf;

		_new_state = satcom_state::SATCOM_STATE_STANDBY;
		return;
	}

	uint8_t new_quality = _rx_command_buf[5] - 48;

	if(new_quality != _signal_quality) {
		LogInfo() << "Iridium signal quality update, now :" << int(new_quality);
	}

	_signal_quality = new_quality;

	LogIridium() << "SIGNAL QUALITY: " << int(_signal_quality);

	_new_state = satcom_state::SATCOM_STATE_STANDBY;
}

void wer::com::IridiumSBD::sbdsession_loop(void)
{
	satcom_result_code res = read_at_command();

	if (res == satcom_result_code::SATCOM_RESULT_NA) {
		if ((_param_session_timeout_s > 0)
		    && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _session_start_time).count()
			> _param_session_timeout_s * 1000)) {

			LogIridium() << "SBD SESSION: TIMEOUT!";
			++_failed_sbd_sessions;
			_new_state = satcom_state::SATCOM_STATE_STANDBY;
			_tx_buf_mutex.unlock();
		}

		return;
	}

	if (res != satcom_result_code::SATCOM_RESULT_OK) {
		LogIridium() << "SBD SESSION: ERROR. RESULT: " << int(res);

		++_failed_sbd_sessions;
		_new_state = satcom_state::SATCOM_STATE_STANDBY;
		_tx_buf_mutex.unlock();
		return;
	}

	if (strncmp((const char *)_rx_command_buf, "+SBDIX:", 7)) {

		LogIridium() << "SBD SESSION: WRONG ANSWER: " << _rx_command_buf;

		_new_state = satcom_state::SATCOM_STATE_STANDBY;
		++_failed_sbd_sessions;
		_tx_buf_mutex.unlock();
		return;
	}

	int mo_status, mt_status, mt_len, mt_queued;
	const char *p = (const char *)_rx_command_buf + 7;
	char **rx_buf_parse = (char **)&p;

	mo_status = strtol(*rx_buf_parse, rx_buf_parse, 10);
	(*rx_buf_parse)++;
	strtol(*rx_buf_parse, rx_buf_parse, 10); // MOMSN, ignore it
	(*rx_buf_parse)++;
	mt_status = strtol(*rx_buf_parse, rx_buf_parse, 10);
	(*rx_buf_parse)++;
	strtol(*rx_buf_parse, rx_buf_parse, 10); // MTMSN, ignore it
	(*rx_buf_parse)++;
	mt_len = strtol(*rx_buf_parse, rx_buf_parse, 10);
	(*rx_buf_parse)++;
	mt_queued = strtol(*rx_buf_parse, rx_buf_parse, 10);

	//VERBOSE_INFO("MO ST: %d, MT ST: %d, MT LEN: %d, MT QUEUED: %d", mo_status, mt_status, mt_len, mt_queued);
	LogIridium() << "SBD session duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _session_start_time).count() / 1000.0;

	switch (mo_status) {
	case 0:
	case 2:
	case 3:
	case 4:
		LogIridium() << "SBD SESSION: SUCCESS mo:" << mo_status;

		_ring_pending = false;
		_tx_session_pending = false;
		_last_read_time = std::chrono::steady_clock::now();
		_last_heartbeat = _last_read_time;
		++_successful_sbd_sessions;

		if (mt_queued > 0) {
			_rx_session_pending = true;

		} else {
			_rx_session_pending = false;

		}

		if (mt_len > 0) {
			_rx_read_pending = true;
		}

		// after a successful session reset the tx buffer
		_tx_buf_write_idx = 0;
		break;

	case 1:
		LogIridium() << "SBD SESSION: MO SUCCESS, MT FAIL";
		_last_heartbeat = std::chrono::steady_clock::now();

		// after a successful session reset the tx buffer
		_tx_buf_write_idx = 0;
		++_successful_sbd_sessions;

		_tx_session_pending = false;
		break;

	case 32:
		LogIridium() << "SBD SESSION: NO NETWORK SIGNAL";

		++_failed_sbd_sessions;
		_signal_quality = 0;
		break;

	default:
		++_failed_sbd_sessions;
		LogIridium() << "SBD SESSION: FAILED : " << mo_status;
	}

	_new_state = satcom_state::SATCOM_STATE_STANDBY;
	_tx_buf_mutex.unlock();
}

void wer::com::IridiumSBD::test_loop(void)
{
	satcom_result_code res = read_at_command();

	if (res != satcom_result_code::SATCOM_RESULT_NA) {
		//PX4_INFO("TEST RESULT: %d, LENGTH %d\nDATA:\n%s", res, _rx_command_len, _rx_command_buf);
		//PX4_INFO("TEST DONE, TOOK %lld MS", (std::chrono::steady_clock::now() - _test_timer) / 1000);
		_new_state = satcom_state::SATCOM_STATE_STANDBY;
	}

	// timeout after 60 s in the test state
	if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _test_timer).count() > 60000) {
		//PX4_WARN("TEST TIMEOUT AFTER %lld S", (std::chrono::steady_clock::now() - _test_timer) / 1000000);
		_new_state = satcom_state::SATCOM_STATE_STANDBY;
	}
}

void wer::com::IridiumSBD::start_csq(void)
{
	if ((_state != satcom_state::SATCOM_STATE_STANDBY) || (_new_state != satcom_state::SATCOM_STATE_STANDBY)) {
		//VERBOSE_INFO("CANNOT ENTER CSQ STATE");
		return;

	} else {
		//VERBOSE_INFO("UPDATING SIGNAL QUALITY");
	}

	_last_signal_check = std::chrono::steady_clock::now();

	if (!is_modem_ready()) {
		//VERBOSE_INFO("UPDATE SIGNAL QUALITY: MODEM NOT READY!");
		return;
	}

	write_at("AT+CSQ");
	_new_state = satcom_state::SATCOM_STATE_CSQ;
}

void wer::com::IridiumSBD::start_sbd_session(void)
{
	if ((_state != satcom_state::SATCOM_STATE_STANDBY) || (_new_state != satcom_state::SATCOM_STATE_STANDBY)) {
		//VERBOSE_INFO("CANNOT ENTER SBD SESSION STATE");
		return;

	} else {
		//VERBOSE_INFO("STARTING SBD SESSION");
	}

	if (!is_modem_ready()) {
		//VERBOSE_INFO("SBD SESSION: MODEM NOT READY!");
		return;
	}

	if (_ring_pending) {
		write_at("AT+SBDIXA");

	} else {
		write_at("AT+SBDIX");
	}

	_new_state = satcom_state::SATCOM_STATE_SBDSESSION;

	_tx_buf_mutex.lock();

	_session_start_time = std::chrono::steady_clock::now();
}

void wer::com::IridiumSBD::start_test(void)
{
	if ((_state != satcom_state::SATCOM_STATE_STANDBY) || (_new_state != satcom_state::SATCOM_STATE_STANDBY)) {
		//PX4_INFO("CANNOT ENTER TEST STATE");
		return;
	}

	satcom_result_code res = read_at_command();

	if (res != satcom_result_code::SATCOM_RESULT_NA) {
		//PX4_WARN("SOMETHING WAS IN BUFFER");
		//printf("TEST RESULT: %d, LENGTH %d\nDATA:\n%s\nRAW DATA:\n", res, _rx_command_len, _rx_command_buf);

		for (int i = 0; i < _rx_command_len; i++) {
			//printf("%d ", _rx_command_buf[i]);
		}

		//printf("\n");
	}

	if (!is_modem_ready()) {
		//PX4_WARN("MODEM NOT READY!");
		return;
	}

	if (strlen(_test_command) != 0) {
		if ((strstr(_test_command, "AT") != nullptr) || (strstr(_test_command, "at") != nullptr)) {
			//PX4_INFO("TEST %s", _test_command);
			write_at(_test_command);
			_new_state = satcom_state::SATCOM_STATE_TEST;

		} else {
			//PX4_WARN("The test command does not include AT or at: %s, ignoring it.", _test_command);
			_new_state = satcom_state::SATCOM_STATE_STANDBY;
		}

	} else {
		//PX4_INFO("TEST DONE");
	}
}

uint8_t wer::com::IridiumSBD::broadcast(uint8_t* data, uint16_t dataLength) {
	return this->write(data, dataLength);
}

uint8_t wer::com::IridiumSBD::unicast(uint8_t* data, uint16_t dataLength, uint16_t target) {
	return broadcast(data, dataLength);
}

void wer::com::IridiumSBD::broadcastDiagnose(uint8_t* data, uint16_t dataLength) {
	broadcast(data, dataLength);
}

int32_t wer::com::IridiumSBD::write(const uint8_t *buffer, size_t buflen)
{
	// general check if the incoming message would be too large (the buffer should not reset in that case)
	if (buflen > SATCOM_TX_BUF_LEN) {
		return -1;
	}

	_tx_buf_mutex.lock();

	// parsing the size of the message to write
	if (!_writing_mavlink_packet) {
		if (buflen < 3) {
			_packet_length = buflen;

		} else if ((unsigned char)buffer[0] == 253 && (buflen == 10)) { // mavlink 2
			const uint8_t payload_len = buffer[1];
			const uint8_t incompat_flags = buffer[2];
			_packet_length = payload_len + 12;
			_writing_mavlink_packet = true;

			if (incompat_flags & 0x1) { //signing
				_packet_length += 13;
			}

		} else if ((unsigned char)buffer[0] == 254 && (buflen == 6)) { // mavlink 1
			const uint8_t payload_len = buffer[1];
			_packet_length = payload_len + 8;
			_writing_mavlink_packet = true;

		} else {
			_packet_length = buflen;
		}
	}

	// check if there is enough space to write the message
	if (SATCOM_TX_BUF_LEN - _tx_buf_write_idx - _packet_length < 0) {
		_tx_buf_write_idx = 0;
		++_num_tx_buf_reset;
	}

	// keep track of the remaining packet length and if the full message is written
	_packet_length -= buflen;

	if (_packet_length == 0) {
		_writing_mavlink_packet = false;
	}

	LogIridium() << "WRITE: LEN " << buflen << ", TX WRITTEN: " << _tx_buf_write_idx;

	memcpy(_tx_buf + _tx_buf_write_idx, buffer, buflen);

	_tx_buf_write_idx += buflen;
	_last_write_time = std::chrono::steady_clock::now();
	_tx_buf_write_pending = true;

	_tx_buf_mutex.unlock();

	return buflen;
}

int32_t wer::com::IridiumSBD::read(char *buffer, size_t buflen)
{
	_rx_buf_mutex.lock();
	LogIridium() << "READ: LEN " << buflen << ", RX: " << _rx_msg_read_idx << " RX END: " << _rx_msg_end_idx;

	if (_rx_msg_read_idx < _rx_msg_end_idx) {
		size_t bytes_to_copy = _rx_msg_end_idx - _rx_msg_read_idx;

		if (bytes_to_copy > buflen) {
			bytes_to_copy = buflen;
		}

		memcpy(buffer, &_rx_msg_buf[_rx_msg_read_idx], bytes_to_copy);

		_rx_msg_read_idx += bytes_to_copy;

		_rx_buf_mutex.unlock();
		return bytes_to_copy;

	} else {
		_rx_buf_mutex.unlock();
		return -EAGAIN;
	}
}

void wer::com::IridiumSBD::write_tx_buf()
{
	if (!is_modem_ready()) {
		LogIridium() << "WRITE SBD: MODEM NOT READY!";
		return;
	}

	_tx_buf_mutex.lock();

	char command[13];
	sprintf(command, "AT+SBDWB=%d", _tx_buf_write_idx);
	write_at(command);

	if (read_at_command() != satcom_result_code::SATCOM_RESULT_READY) {
		LogIridium() << "WRITE SBD: MODEM NOT RESPONDING!";
		_tx_buf_mutex.unlock();
		return;
	}

	_connection->sendMessage(_tx_buf, _tx_buf_write_idx);

	int sum = 0;
	for (int i = 0; i < _tx_buf_write_idx; i++) {
		sum += _tx_buf[i];
	}

	uint8_t checksum[2] = {(uint8_t)(sum / 256), (uint8_t)(sum & 255)};
	_connection->sendMessage(checksum, 2);


	LogIridium() << "SEND SBD: CHECKSUM " << checksum[0] << " " << checksum[1];

	if (read_at_command(250) != satcom_result_code::SATCOM_RESULT_OK) {
		LogIridium() << "WRITE SBD: ERROR WHILE WRITING DATA TO MODEM!";

		_tx_buf_mutex.unlock();
		return;
	}

	if (_rx_command_buf[0] != '0') {

		LogIridium() << "WRITE SBD: ERROR WHILE WRITING DATA TO MODEM! (" <<  _rx_command_buf[0] - '0' << ")";

		_tx_buf_mutex.unlock();
		return;
	}

	LogIridium() << "WRITE SBD: DATA WRITTEN TO MODEM";

	_tx_buf_write_pending = false;

	_tx_buf_mutex.unlock();

	_tx_session_pending = true;
}

void wer::com::IridiumSBD::read_rx_buf(void)
{
	if (!is_modem_ready()) {
		LogIridium() << "READ SBD: MODEM NOT READY!";
		return;
	}

	_rx_buf_mutex.lock();


	write_at("AT+SBDRB");

	if (read_at_msg() != satcom_result_code::SATCOM_RESULT_OK) {
		LogIridium() << "READ SBD: MODEM NOT RESPONDING!";
		_rx_msg_read_idx = _rx_msg_end_idx;
		_rx_buf_mutex.unlock();
		return;
	}

	int data_len = (_rx_msg_buf[0] << 8) + _rx_msg_buf[1];

	// rx_buf contains 2 byte length, data, 2 byte checksum and /r/n delimiter
	if (data_len != _rx_msg_end_idx - 6) {
		LogIridium() << "READ SBD: WRONG DATA LENGTH";
		_rx_msg_read_idx = _rx_msg_end_idx;
		_rx_buf_mutex.unlock();
		return;
	}

	int checksum = 0;

	for (int i = 2; i < data_len + 2; i++) {
		checksum += _rx_msg_buf[i];
	}

	if ((checksum / 256 != _rx_msg_buf[_rx_msg_end_idx - 4]) || ((checksum & 255) != _rx_msg_buf[_rx_msg_end_idx - 3])) {
		LogIridium() << "READ SBD: WRONG DATA CHECKSUM";
		_rx_msg_read_idx = _rx_msg_end_idx;
		_rx_buf_mutex.unlock();
		return;
	}

	_rx_msg_read_idx = 2;	// ignore the length
	_rx_msg_end_idx -= 4;	// ignore the checksum and delimiter
	_rx_read_pending = false;

	if (_rx_msg_read_idx < _rx_msg_end_idx) {
		uint16_t buflen = _rx_msg_end_idx - _rx_msg_read_idx;

		std::cout << "Msg hex : ";
		for(int i = 0; i < buflen; i++) {
			std::cout << ":" << std::hex << int(_rx_msg_buf[i+_rx_msg_read_idx]);
		}
		std::cout << std::endl;

		if(_recvCallback) {
			_recvCallback(&(_rx_msg_buf[_rx_msg_read_idx]), buflen, 0);
		}

		_rx_msg_read_idx += buflen;
	}

	_rx_buf_mutex.unlock();
	LogIridium() << "READ SBD: SUCCESS, LEN: " << data_len;
}

void wer::com::IridiumSBD::write_at(const char *command)
{
	LogIridium() << "WRITING AT COMMAND: " << command;
	_connection->sendMessage((const uint8_t*) command, strlen(command));
	_connection->sendMessage((const uint8_t*) "\r", 1);
}

wer::com::satcom_result_code wer::com::IridiumSBD::read_at_command(int16_t timeout)
{
	return read_at(_rx_command_buf, &_rx_command_len, timeout);
}

wer::com::satcom_result_code wer::com::IridiumSBD::read_at_msg(int16_t timeout)
{
	return read_at(_rx_msg_buf, &_rx_msg_end_idx, timeout);
}


void wer::com::IridiumSBD::processIncomingData(uint8_t* data, uint16_t dataLength) {
	std::lock_guard<std::mutex> lock(_rx_result_mutex);
	for(int i = 0; i < dataLength; i++) {
		if (_rx_buf_pos == 0 && (data[i] == '\r' || data[i] == '\n')) {
			// ignore the leading \r\n
			continue;
		}

		if(_rx_buf_pos >= 270) {
			LogIridium() << "Error during reception index to big.";
			_last_rn_idx = 0;
			_rx_buf_pos = 0;
		}

		_rx_buf[_rx_buf_pos++] = data[i];

		if (_rx_buf[_rx_buf_pos - 1] == '\n' && _rx_buf[_rx_buf_pos - 2] == '\r') {
			// found the \r\n delimiter
			if (_rx_buf_pos == _last_rn_idx + 2) {
				//if (verbose) { //PX4_INFO("second in a row, ignore it");}
				; // second in a row, ignore it

			} else if (!strncmp((const char *)&_rx_buf[_last_rn_idx], "OK\r\n", 4)) {
				_rx_buf[_rx_len] = 0; 	// null terminator after the information response for printing purposes

				_last_rn_idx = 0;
				_rx_buf_pos = 0;
				_new_result_available = true;
				_new_result = satcom_result_code::SATCOM_RESULT_OK;
				_new_result_condition.notify_one();

			} else if (!strncmp((const char *)&_rx_buf[_last_rn_idx], "ERROR\r\n", 7)) {
				_last_rn_idx = 0;
				_rx_buf_pos = 0;
				_new_result_available = true;
				_new_result = satcom_result_code::SATCOM_RESULT_ERROR;
				_new_result_condition.notify_one();

			} else if (!strncmp((const char *)&_rx_buf[_last_rn_idx], "SBDRING\r\n", 9)) {
				_ring_pending = true;
				_rx_session_pending = true;

				LogIridium() << "GET SBDRING";

				_last_rn_idx = 0;
				_rx_buf_pos = 0;
				_new_result_available = true;
				_new_result = satcom_result_code::SATCOM_RESULT_SBDRING;
				_new_result_condition.notify_one();

			} else if (!strncmp((const char *)&_rx_buf[_last_rn_idx], "READY\r\n", 7)) {
				_last_rn_idx = 0;
				_rx_buf_pos = 0;
				_new_result_available = true;
				_new_result = satcom_result_code::SATCOM_RESULT_READY;
				_new_result_condition.notify_one();

			} else if (!strncmp((const char *)&_rx_buf[_last_rn_idx], "HARDWARE FAILURE", 16)) {
				
				LogIridium() << "HARDWARE FAILURE!";

				_last_rn_idx = 0;
				_rx_buf_pos = 0;
				_new_result_available = true;
				_new_result = satcom_result_code::SATCOM_RESULT_HWFAIL;
				_new_result_condition.notify_one();
			} else {
				_rx_len = _rx_buf_pos;	// that was the information response, result code incoming
			}

			_last_rn_idx = _rx_buf_pos;
		}
	}
}

wer::com::satcom_result_code wer::com::IridiumSBD::read_at(uint8_t *rx_buf, int *rx_len, int16_t timeout)
{
	std::unique_lock<std::mutex> lock(_rx_result_mutex);
	auto now = std::chrono::steady_clock::now();

	std::cv_status status = std::cv_status::no_timeout;

	while(status == std::cv_status::no_timeout && _new_result_available == false) {
		status = _new_result_condition.wait_until(lock, now + std::chrono::milliseconds(timeout));
	}

	if(_new_result_available) {
		memcpy(rx_buf, _rx_buf, _rx_len);
		*rx_len = _rx_len;
		_new_result_available = false;
		return _new_result;
	} else {
		//Timeout
		_last_rn_idx = 0;
		_rx_buf_pos = 0;
		return satcom_result_code::SATCOM_RESULT_NA;
	}
}

void wer::com::IridiumSBD::schedule_test(void)
{
	_test_pending = true;
}

bool wer::com::IridiumSBD::clear_mo_buffer()
{
	write_at("AT+SBDD0");

	if (read_at_command() != satcom_result_code::SATCOM_RESULT_OK || _rx_command_buf[0] != '0') {
		LogIridium() << "CLEAR MO BUFFER: ERROR";
		return false;
	}

	return true;
}

bool wer::com::IridiumSBD::is_modem_ready(void)
{
	write_at("AT");

	if (read_at_command() == satcom_result_code::SATCOM_RESULT_OK) {
		return true;

	} else {
		return false;
	}
}