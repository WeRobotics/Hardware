#include "pca9632.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define Pca9632_MODE1_VALUE   0b00000000 // (NO AUTO-INCREMENT/SUBADDRESSES/ALLCALL)
#define Pca9632_MODE2_VALUE   0b00010101 // (DIMMING, INVERT, CHANGE ON STOP,TOTEM)

/* Register addresses */
#define Pca9632_MODE1       0x00		// Mode register 1
#define Pca9632_MODE2       0x01		// Mode register 2
#define Pca9632_PWM0        0x02		// brightness control LED0
#define Pca9632_PWM1        0x03		// brightness control LED1
#define Pca9632_PWM2        0x04		// brightness control LED2
#define Pca9632_PWM3        0x05		// brightness control LED3
#define Pca9632_GRPPWM      0x06		// group duty cycle
#define Pca9632_GRPFREQ     0x07		// group frequency
#define Pca9632_LEDOUT      0X08		// LED output state
#define Pca9632_SUBADR1     0x09		// I2C-bus subaddress 1
#define Pca9632_SUBADR2     0x0A		// I2C-bus subaddress 2
#define Pca9632_SUBADR3     0x0B		// I2C-bus subaddress 3
#define Pca9632_ALLCALLADDR 0x0C		// LED All I2C-bus address

#define Pca9632_NO_AUTOINC  0x00
#define Pca9632_AUTO_ALL    0x80
#define Pca9632_AUTO_IND    0xA0
#define Pca9632_AUTOGLO     0xC0
#define Pca9632_AUTOGI      0xE0

#define LED_OFF   0x00
#define LED_ON    0x01
#define LED_PWM   0x02
#define LED_GRP   0x03

#define Pca9632_NB_OUTPUT 4

namespace wer {
namespace hal {

// std::list<Pca9632*> Pca9632::_connectedDevices;

Pca9632::Pca9632(std::shared_ptr<I2c> i2c,uint8_t address) :
	i2c_(i2c),
    address_(address),
	_outputState(0x00)
{
	writeReg(Pca9632_MODE1, Pca9632_MODE1_VALUE);
	writeReg(Pca9632_MODE2, Pca9632_MODE2_VALUE);
	writeReg(Pca9632_LEDOUT, _outputState);
}

// Pca9632* Pca9632::getByAddress(uint8_t address) {
// 	std::list<Pca9632*>::iterator item;
	
// 	for(item = _connectedDevices.begin(); item != _connectedDevices.end(); item++) {
// 		if((*item)->getI2cAddress() == address) {
// 			return *item;
// 		}
// 	}

// 	Pca9632* instance = new Pca9632(address);
// 	_connectedDevices.push_back(instance);
// 	return instance;
// }

// void Pca9632::deleteAll() {
// 	std::list<Pca9632*>::iterator item;
	
// 	for(item = _connectedDevices.begin(); item != _connectedDevices.end(); item++) {
// 		(*item)->writeReg(Pca9632_PWM0, 0x00);
// 		(*item)->writeReg(Pca9632_PWM1, 0x00);
// 		(*item)->writeReg(Pca9632_PWM2, 0x00);
// 		(*item)->writeReg(Pca9632_PWM3, 0x00);
// 		delete *item;
// 	}

// 	_connectedDevices.clear();
// }

void Pca9632::setIndividualPwm(uint8_t channel, uint8_t pwm) {
	if((channel < Pca9632_NB_OUTPUT) && (pwm < 256)) {
		writeReg(Pca9632_PWM0 + channel, pwm);
	//	BOOST_LOG_SEV(_slg, info) << "PWM of channel " << unsigned(channel) << " set to " << unsigned(pwm);
	}
}

void Pca9632::setGlobalPwm(uint8_t pwm) {
	if(pwm < 256) {
		writeReg(Pca9632_GRPPWM, pwm);
	//	BOOST_LOG_SEV(_slg, info) << "Global PWM set to " << unsigned(pwm);
	}
}

void Pca9632::setGlobalFrequency(uint8_t frequency) {
	if(frequency < 256) {
		writeReg(Pca9632_GRPFREQ, frequency);
	//	BOOST_LOG_SEV(_slg, info) << "Global frequency set to " << unsigned(frequency);
	}
}

void Pca9632::setMode(uint8_t channel, uint8_t mode) {
	if(channel < Pca9632_NB_OUTPUT) {
		switch(mode) {
			case LED_OFF:	// 0x00
			{
				setBit(_outputState, 2*channel + 1, 0);
				setBit(_outputState, 2*channel, 0);
				break;
			}

			case LED_ON:	// 0x01
			{
				setBit(_outputState, 2*channel + 1, 0);
				setBit(_outputState, 2*channel, 1);
				break;
			}

			case LED_PWM:	// 0x10
			{
				setBit(_outputState, 2*channel + 1, 1);
				setBit(_outputState, 2*channel, 0);
				break;
			}

			case LED_GRP:	// 0x11
			{
				setBit(_outputState, 2*channel + 1, 1);
				setBit(_outputState, 2*channel, 1);
				break;
			}
		}

		writeReg(Pca9632_LEDOUT, _outputState);
	//	BOOST_LOG_SEV(_slg, info) << "Mode of channel " << unsigned(channel) << " set to " << unsigned(mode);
	}
}

void Pca9632::reset() {
	//Pca9632_WriteRegister(Pca9632_ADDRESS,Pca9632_MODE1, Pca9632_MODE1_VALUE);
	writeReg(Pca9632_MODE1, Pca9632_MODE1_VALUE);
	writeReg(Pca9632_MODE2, Pca9632_MODE2_VALUE);
	writeReg(Pca9632_LEDOUT, _outputState);
}

void Pca9632::channelOn(uint8_t channel) {
	setMode(channel, LED_ON);
}

void Pca9632::channelPwm(uint8_t channel) {
	setMode(channel, LED_PWM);
}

void Pca9632::channelOff(uint8_t channel) {
	setMode(channel, LED_OFF);
}

/*------------------------------------------------------------------------------*/

void Pca9632::setBit(uint8_t &byte, uint8_t bit, uint8_t value){
	if(value == 1)
		byte |= (1 << bit);
	else if(value == 0)
		byte &= ~(1 << bit);
}

void Pca9632::writeReg(uint8_t registerAddr, uint8_t data) {
	// std::cout << "write to pca9632"<<std::endl;
	uint8_t buffer[2] = {registerAddr, data};
	//I2cInterface::write(buffer, 2);
	i2c_->set(address_, buffer, 2);
}

uint8_t Pca9632::readReg(uint8_t registerAddr) {
	uint8_t buffer[1] = {registerAddr};
	//I2cInterface::write(buffer, 1);
	i2c_->set(address_, buffer, 1);

	usleep(50);
  
  	if(!(i2c_->get(address_, buffer, 1))) {
  		return buffer[0];
  	}
  	else {
  		return 0xFF;
  	}
}


}  // namespace hal
}  // namespace wer