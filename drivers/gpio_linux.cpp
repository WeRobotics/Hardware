#include "gpio.h"

#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <iostream>

#define BUFFER_MAX 64

WER::HW::DRIVER::Gpio::Gpio() :
_thread(nullptr),
_shouldExit(false)
{
  _thread = new std::thread(&WER::HW::DRIVER::Gpio::task, this);
}

WER::HW::DRIVER::Gpio::~Gpio() {
    _shouldExit = true;

    if(_thread->joinable()) {
        _thread->join();
        delete(_thread);
        _thread = nullptr;
    }

    std::lock_guard<std::mutex> lock(_pinsMutex);

    for(auto it : _pins) {
      if(it.second.fd) {
          close(it.second.fd);
        }
    }

    _pins.clear();
}

uint8_t WER::HW::DRIVER::Gpio::init(WER::HW::IO gpio, WER::HW::INTERFACE::GpioDirection direction) {
  //Check gpio not already configured (exported, direction, event)
  bool exist = true;
  _pinsMutex.lock();
  if(_pins.find(gpio) == _pins.end()) {
    exist = false;
  }
  _pinsMutex.unlock();

  if(exist) {
    return 1;
  }
  
  char buffer[BUFFER_MAX];

  ssize_t bytes_written = snprintf(buffer, BUFFER_MAX, "%d", static_cast<int>(gpio));
  int fd = open("/sys/class/gpio/export", O_WRONLY);
  if(fd < 0) {
    return 1;
  }

  //Do no check for errors as if the pin was alredy exported it will return an error but we actually don't mind.
  write(fd, buffer, bytes_written);

  close(fd);

  //Give some time to the system to make required changes
  usleep(100000);

  snprintf(buffer, BUFFER_MAX, "/sys/class/gpio/gpio%d/direction", static_cast<int>(gpio));

  fd = open(buffer, O_RDWR);
  if (fd < 0) {
    return 3;
  }

  char c;
  read(fd, &c, 1);

  if(direction == WER::HW::INTERFACE::GpioDirection::INPUT) {
    if(c == 'i') {
      //std::cout << "Already good direction. PIN:" << unsigned(static_cast<int>(gpio)) << std::endl;
    }
    else if(write(fd, "in", 2) == -1) {
      close(fd);
      return 4;
    }
  }
  else {
    if(c == 'o') {
      //std::cout << "Already good direction." << unsigned(static_cast<int>(gpio)) << std::endl;
    }
    else if(write(fd, "out", 3) == -1) {
      close(fd);
      return 4;
    }
  }

  close(fd);

  snprintf(buffer, BUFFER_MAX, "/sys/class/gpio/gpio%d/value", static_cast<int>(gpio));

  if(direction == WER::HW::INTERFACE::GpioDirection::INPUT) {
    if((fd = open(buffer, O_RDONLY)) == -1) {
      return 5;
    }
  }
  else {
    if((fd = open(buffer, O_WRONLY)) == -1) {
      return 5;
    }
  }
  
  GpioPin pin;
  pin.direction = direction;
  pin.callback = nullptr;
  pin.event = WER::HW::INTERFACE::GpioEvent::NONE;
  pin.fd = fd;

  _pinsMutex.lock();
  _pins[gpio] = pin;
  _pinsMutex.unlock();

  return 0;
}

uint8_t WER::HW::DRIVER::Gpio::digitalWrite(WER::HW::IO gpio, uint8_t value) {
  std::lock_guard<std::mutex> lock(_pinsMutex);

  auto pin = _pins.find(gpio);

  if(pin != _pins.end() && pin->second.direction == WER::HW::INTERFACE::GpioDirection::OUTPUT) {
    if (value == 0) {
      if(write(pin->second.fd, "0\n", 2) == -1) {
        return 2;
      }
    }
    else {
      if(write(pin->second.fd, "1\n", 2) == -1) {
        return 2;
      }
    }
    return 0;
  }
  else {
    return 1;
  }
}

uint8_t WER::HW::DRIVER::Gpio::digitalRead(WER::HW::IO gpio, uint8_t* value) {
  std::lock_guard<std::mutex> lock(_pinsMutex);

  auto pin = _pins.find(gpio);

  if(pin != _pins.end() && pin->second.direction == WER::HW::INTERFACE::GpioDirection::INPUT) {
    char c;
    lseek(pin->second.fd, 0L, SEEK_SET);
    read(pin->second.fd, &c, 1);

    *value = (c == '0') ? 0 : 1;

    return 0;
  }
  else {
    return 1;
  }
}

uint8_t WER::HW::DRIVER::Gpio::registerCallback(WER::HW::IO gpio, WER::HW::INTERFACE::GpioEvent event, std::function<void(WER::HW::IO, WER::HW::INTERFACE::GpioEvent)> callback) {
    //Check gpio not already configured (exported, direction, event)
  bool exist = true;
  bool input = false;

  _pinsMutex.lock();
  if(_pins.find(gpio) == _pins.end()) {
    exist = false;
  }
  else {
    auto pin = _pins.find(gpio);
    if(pin->second.direction == WER::HW::INTERFACE::GpioDirection::INPUT && pin->second.event == WER::HW::INTERFACE::GpioEvent::NONE) {
      close(pin->second.fd);
      input = true;
      _pins.erase(pin);
    }
  }
  _pinsMutex.unlock();

  if(exist && !input) {
    return 1;
  }

  char buffer[BUFFER_MAX];
  int fd = 0;

  if(!input) {
    ssize_t bytes_written = snprintf(buffer, BUFFER_MAX, "%d", static_cast<int>(gpio));
    
    //Let's not trigger any error on the export, pin could already be exported !
    fd = open("/sys/class/gpio/export", O_WRONLY);
    if(fd >= 0) {
      write(fd, buffer, bytes_written);
      close(fd);
    }


    snprintf(buffer, BUFFER_MAX, "/sys/class/gpio/gpio%d/direction", static_cast<int>(gpio));
    fd = open(buffer, O_WRONLY);
    if (fd < 0) {
      return 3;
    }

    if(write(fd, "in", 2) == -1) {
      return 4;
    }
    close(fd);
  }


  snprintf(buffer, BUFFER_MAX, "/sys/class/gpio/gpio%d/edge", static_cast<int>(gpio));
  fd = open(buffer, O_WRONLY);
  if(fd < 0) {
    return 5;
  }

  if(event == WER::HW::INTERFACE::GpioEvent::RISING_EDGE) {
    if(write(fd, "falling", 7) == -1) {
      return 6;
    }

  }
    else if(event == WER::HW::INTERFACE::GpioEvent::FALLING_EDGE) {
    if(write(fd, "rising", 6) == -1) {
      return 6;
    }
    }
    else if(event == WER::HW::INTERFACE::GpioEvent::BOTH_EDGE) {
    if(write(fd, "both", 4) == -1) {
      return 6;
    }
    }
    close(fd);


  snprintf(buffer, BUFFER_MAX, "/sys/class/gpio/gpio%d/value", static_cast<int>(gpio));
  fd = open(buffer, O_RDWR);

  if(fd < 0) {
    return 7;
  }

  // Clear any initial pending interrupt
  uint8_t c;
  int count;
  ioctl(fd, FIONREAD, &count);
  for(int i = 0; i < count; ++i) {
    read(fd, &c, 1);
  }

  GpioPin pin;
  pin.direction = WER::HW::INTERFACE::GpioDirection::INPUT;
  pin.callback = callback;
  pin.event = event;
  pin.fd = fd;
  pin.lastValue = c;

  _pinsMutex.lock();
  _pins[gpio] = pin;
  _pinsMutex.unlock();

  return 0;
}

void WER::HW::DRIVER::Gpio::task() {
  
  while(!_shouldExit) {
    // Setup poll structure
    _pinsMutex.lock();

    uint8_t n = _pins.size();
    struct pollfd polls[n];
    WER::HW::IO gpio[n];

    int i = 0;
    for(auto it : _pins) {
      if(it.second.event != WER::HW::INTERFACE::GpioEvent::NONE) {
        polls[i].fd = it.second.fd;
        polls[i].events = POLLPRI | POLLERR;
        gpio[i] = it.first;
        i++;
      }
    }

    _pinsMutex.unlock();

    int x = poll(polls, i, 100);

    if(x > 0) {
      for(int j = 0; j < i; j++) {
        if(polls[j].revents & POLLPRI) {
          _pinsMutex.lock();
          auto pin = _pins.find(gpio[j]);
          if(pin != _pins.end()) {
            auto callback = pin->second.callback;
            auto event = pin->second.event;
            _pinsMutex.unlock();

            uint8_t value;
            digitalRead(gpio[j], &value);
            if(event == WER::HW::INTERFACE::GpioEvent::BOTH_EDGE) {
              if(value == true) {
                event = WER::HW::INTERFACE::GpioEvent::RISING_EDGE;
                //std::cout << "Risisng edge on pin " << unsigned(gpio[j]) << " with value " << unsigned(value) << std::endl;
              }
              else {
                event = WER::HW::INTERFACE::GpioEvent::FALLING_EDGE;
                //std::cout << "Falling edge on pin " << unsigned(gpio[j]) << " with value " << unsigned(value) << std::endl;
              }
            }
            callback(gpio[j], event);
          }
          else {
            _pinsMutex.unlock();
          }
        }
        
        if(polls[j].revents & POLLERR) {
          //std::cout << "Poll error on pin " << unsigned(gpio[j]) << std::endl;
          //TODO error management...
        }

      } // End For
    }
/*
    for(int j = 0; j < i; j++) {
      uint8_t value;
      if(digitalRead(gpio[j], &value)) {
        std::cout << "Error while reading pin." << std::endl;
      }

      if(_pins[gpio[j]].lastValue != value) {
        std::cout << "Value on pin " << unsigned(gpio[j]) << " changed to " << unsigned(value) << std::endl;
        _pins[gpio[j]].lastValue = value;
      }
      
    }
*/

  } // End While
}
