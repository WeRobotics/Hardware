#include "hih6100.h"

#include <iostream>
#include <limits>

#include "log.h"

namespace wer {
namespace hal {

using namespace std;

Hih6100::Hih6100(std::shared_ptr<lib::CallEveryHandler> call_every_handler,
                 std::shared_ptr<I2c> i2c, uint8_t address)
    : temperature_(std::numeric_limits<float>::quiet_NaN()),
      humidity_(std::numeric_limits<float>::quiet_NaN()),
      connected_(false),
      enable_(false),
      collect_phase_(false),
      faulty_(false),
      call_every_handler_(call_every_handler),
      i2c_(i2c),
      address_(address) {
  log_ = lib::LogManager::createLogMsg(
      true,
      "/home/pi/WMP-Belt/communication/javascript_webinterface/interface/logs/"
      "cooling");

  measure_time_ = std::chrono::steady_clock::now();
  log_->info_ << "Hih6100 created" << '\n';
}

void Hih6100::enable(float sample_rate) {
  log_->info_ << "enable hih6100" << '\n';
  if (enable_ == false) {
    call_every_handler_->add(std::bind(&Hih6100::cycle, this), sample_rate,
                             &cookie_);
    enable_ = true;
  }
}

void Hih6100::disable() {
  if (enable_) {
    call_every_handler_->remove(cookie_);
    enable_ = false;
  }
}

void Hih6100::cycle() {
  if (collect_phase_) {
    data_fetch();
  } else {
    measurement_request();
  }
}

void Hih6100::data_fetch() {
  uint8_t buffer[4];
  auto dt = std::chrono::steady_clock::now() - measure_time_;
  if ((std::chrono::duration_cast<std::chrono::milliseconds>(dt)).count() >
      600) {
    if (i2c_->get(address_, buffer, 4) == 0x01) {
      if (!faulty_) {
        //  LogErr() << "Error while reading the  Hih6100 unit.";
        faulty_ = true;
      }
    } else {
      faulty_ = false;
      collect_phase_ = false;
      /* Humidity is located in first two bytes */
      int reading_hum = (buffer[0] << 8) + buffer[1];
      float humidity = reading_hum / 16382.0 * 100.0;
      if (humidity > 0.0 && humidity < 100.0){
        humidity_ = humidity;
      }
      // log_->info_ << "humidity_ " << humidity_ << '\n';
      /* Temperature is located in next two bytes, padded by two trailing bits
       */
      int reading_temp = (buffer[2] << 6) + (buffer[3] >> 2);
      float temperature = reading_temp / 16382.0 * 165.0 - 40;
      if (temperature > 0.0 && temperature < 60.0){
        temperature_ = temperature;
      }
      // log_->info_ << "temperature_ " << temperature_<< '\n';
      // LogSensor() << "T:" << temperature_ << "C\tH:" << humidity_ << "%";
    }
  }
}

void Hih6100::measurement_request() {
  uint8_t buffer = 0;

  if (i2c_->set(address_, &buffer, 1) == 0x01) {
    if (connected_ == true) {
      // LogWarn() << "Sensor disconnected";
      connected_ = false;
    }
  } else {
    if (connected_ == false) {
      // LogInfo() << "Sensor connected";
      connected_ = true;
    }
    collect_phase_ = true;
    measure_time_ = std::chrono::steady_clock::now();
  }
}

}  // namespace hal
}  // namespace wer