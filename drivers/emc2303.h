#ifndef EMC2303_H
#define EMC2303_H

#include <sys/types.h>

#include <iostream>
#include <list>
#include <memory>

#include "Hardware/include/i2c.h"
#include "Hardware/include/pwm.h"

namespace wer {
namespace hal {

/**
 * This class implements the EMC2303, a 3 channel fan driver, used to
 * drive fans and vibration motors in the release mechanism. Each fan and
 * motor is connected on a separate channel.
 */

class Emc2303 : public Pwm {
 public:
  Emc2303(std::shared_ptr<I2c> i2c, uint8_t address_);

  /**
   * [setChannel() is used to set the PWM duty cycle to 100% on the
   * desired channel. The PWM frequency does not change.]
   * @param	channel [desired channel]
   * @param	dutyCycle [desired duty cycle (0-255)]
   * @return	[]
   */
  void setChannel(uint8_t channel, uint8_t dutyCycle);

  /**
   * [enable() enables the whole device (EMC2303). Upon enabling,
   * the previous PWM duty cycle and frequency values are used.]
   * @param	[]
   * @return	[]
   */
  void enable();

  /**
   * [disable() disables the whole device (EMC2303). If disabled,
   * the PWM duty cycle and PWM frequency cannot be changed. The
   * device is disabled until the function enable() is called.]
   * @param	[]
   * @return	[]
   */
  void disable();

  /**
   * [Sets the PWM duty cycle to 100% for the desired channel]
   * @param	channel []
   * @return	[]
   */
  void setChannelOn(uint8_t channel);

  /**
   * [Sets the PWM duty cycle to 0% for the desired channel]
   * @param	channel []
   * @return	[]
   */
  void setChannelOff(uint8_t channel);

  /**
   * [Sets the frequency of the desired channel.]
   * @param	channel []
   * @param	desiredFreq []
   * @return	[]
   */
  void setFrequency(uint8_t channel, uint16_t desiredFreq);

  /**
   * [Sets the Spin up routine.]
   * @param	channel []
   * @param	kick [Determines if the routine will drive the fan
   * 			to 100% duty cycle for 1/4 of the programmed spin up
   * 			routine duration]
   * @param	driveLevel [in % ! spin up drive level (30% - 65%) during
   * 			the routine duration]
   * @param	spinUpTime [in ms ! sets the routine duration. 4
   * 			options: 250, 500, 1000 or 2000 ms.]
   * @return  []
   */
  void setSpinUpRoutine(uint8_t channel, bool kick, uint8_t driveLevel,
                        uint16_t spinUpTime);

 private:
  /**
   * [Sets the prescaler of the desired channel. The prescaler
   * divides the base frequency of the channel, thus giving the
   * "final frequency". The prescaler is a value between 0 and 255]
   * @param	channel []
   * @param	prescaler []
   * @return	[]
   */
  void setPrescaler(uint8_t channel, uint8_t prescaler);

  /**
   * [Sets the base frequency of the channel. This frequency is
   * chosen based on the desired frequency and a prescaler.
   * e.g. if the user chooses 20Hz, the base frequency will be set
   * to 2441 Hz, and the prescaler to 122 since 2441/122 is roughly
   * 20. The base frequency is chosen among 4 options (2441, 4882,
   * 19531, 26000) based on the frequency asked by the user, and the
   * prescaler is then computed to obtain it]
   * @param	channel []
   * @param	prescaler []
   * @return	[]
   */
  void setBaseFrequency(uint8_t channel, uint16_t baseFrequency);

  const uint8_t kFreqRegChannel1_ = 0x31;  // addresses are 0x1F, 0x29, 0x33
  const uint8_t kPwmRegChannel1_ = 0x30;   // addresses are 0x1E, 0x28, 0x32
  const uint8_t kBaseFreqReg_ = 0x2D;

  // Spin-up routine config
  const uint8_t kSpinUpRegChannel1_ = 0x36;  // addresses are 0x36, 0x46, 0x56

  const uint16_t kNbAvailableFreq_ = 4;
  const uint16_t kAvailableFreq[4] = {2441, 4882, 19531, 26000};

  uint8_t kSpinUpConfig_;

  uint8_t emc2303NbChannels_;

  uint8_t pwmValues[3];
  uint8_t freqValues[3];

  std::shared_ptr<I2c> i2c_;
  uint8_t address_;

  uint8_t currentBaseFreqReg;

  bool disabled_;
  bool enabled_;

  void writeReg(uint8_t registerAddr, uint8_t data);
};

}  // namespace hal
}  // namespace wer

#endif
