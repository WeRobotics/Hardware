#include "dynamixel_xm_extended_steps.h"

namespace wer {
namespace hal {

DynamixelXmExtendedSteps::DynamixelXmExtendedSteps(uint8_t id,
                                                   const char* portName,
                                                   uint32_t baudrate)
    : DynamixelXmSeries(id, portName, baudrate) {
  actual_step_ = 0;
  nb_step_ = 0;
}

void DynamixelXmExtendedSteps::SetStepList(std::vector<int32_t> step_list,
                                           uint16_t nb_step,
                                           uint16_t actual_step) {
  step_list_ = step_list;
  nb_step_ = nb_step;
  actual_step_ = actual_step;
}

bool DynamixelXmExtendedSteps::SetResetPosition() {
  int32_t position;

  SetControlMode(EXT_POS_CTRL_MODE);

  if (ClearMultiTurn()) {
    log_->error_ << "ERROR: ClearMultiTurn failed !" << '\n';
    return true;
  }
  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  } else {
    log_->info_ << "Reset position set: " << position << '\n';
    reset_position_ = position;
    return false;
  }
}

uint8_t DynamixelXmExtendedSteps::StepInc(bool synchro, uint16_t velocity) {
  log_->info_ << "Starting StepInc (synchro: " << synchro << ")" << '\n';
  uint8_t error_flag = false;
  uint16_t next_step;
  int32_t next_position;

  if (actual_step_ < (nb_step_ - 1)) {
    next_step = (actual_step_ + 1) % nb_step_;
    next_position = step_list_[next_step];

    error_flag = SetPosition(next_position, synchro, velocity);

    if (error_flag != false) {
      log_->error_
          << "ERROR: failure in corresponding 'SetPosition' function call"
          << '\n';
      log_->error_ << "ERROR: stepInc not possible !" << '\n';
      log_->info_ << "State: step_mode Partial(ABS),"
                  << " acutalSTep " << (unsigned)actual_step_ << " nbStep "
                  << (unsigned)nb_step_ << '\n';
      return true;
    }
    actual_step_ = next_step;
  } else {
    log_->info_ << "Belt end has been reached" << '\n';
  }

  log_->info_ << "Finished stepInc: actual step is " << (unsigned)actual_step_
              << '\n';
  return error_flag;
}

uint8_t DynamixelXmExtendedSteps::StepDec(bool synchro, uint16_t velocity) {
  uint8_t error_flag = false;
  uint16_t next_step;
  int32_t next_position;

  log_->info_ << "Starting stepDec (synchro: " << synchro << ")" << '\n';

  if (actual_step_ > 0) {
    next_step = actual_step_ - 1;
    next_position = step_list_[next_step];

    error_flag = SetPosition(next_position, synchro, velocity);

    if (error_flag != false) {
      log_->error_
          << "ERROR: failure in corresponding 'SetPosition' function call !"
          << '\n';
      log_->error_ << "ERROR: stepDec not possible !" << '\n';
      log_->info_ << "State: step_mode Partial(ABS),"
                  << " / acutalStep " << (unsigned)actual_step_ << " / nbStep "
                  << (unsigned)nb_step_ << '\n';
      return true;
    }

    actual_step_ = next_step;
  } else {
    log_->info_ << "Belt beginnig has been reached" << '\n';
  }

  log_->info_ << "Finished stepDec: actual step is "
              << (unsigned int)actual_step_ << '\n';
  return error_flag;
}

uint8_t DynamixelXmExtendedSteps::SetPositionToStep(uint16_t step,
                                                    bool synchro) {
  uint8_t error_flag = false;
  log_->info_ << "Going to step " << (unsigned)step << " (synchro: " << synchro
              << ")" << '\n';

  if (step < nb_step_) {
    error_flag = SetPosition(step_list_[step], synchro);

    if (error_flag == false) {
      actual_step_ = step;
      log_->info_ << "Arrived to desired step !" << '\n';
      return error_flag;
    }
    log_->error_
        << "ERROR: failure in corresponding 'SetPosition' function call"
        << '\n';
    log_->error_ << "ERROR: wrong motor mode" << '\n';
    return error_flag;
  } else {
    log_->error_ << "ERROR: step higher than max" << '\n';
    return false;
  }
}

uint16_t DynamixelXmExtendedSteps::GetActualStep() { return actual_step_; }

int32_t DynamixelXmExtendedSteps::GetResetPosition() { return reset_position_; }

uint8_t DynamixelXmExtendedSteps::IsOnStep() {
  int32_t position;
  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }
  if (position - step_list_[actual_step_] < 30 ||
      position - step_list_[actual_step_] > -30) {
    log_->info_ << "DynamixelMotor is on step " << (unsigned int)actual_step_
                << '\n';
    return false;
  }
  log_->error_ << "ERROR: motor is NOT on step " << (unsigned)actual_step_
               << " (" << step_list_[actual_step_] << "), current position is "
               << position << ")" << '\n';
  return true;
}

}  // namespace hal
}  // namespace wer