#pragma once

#include <chrono>

#include <Library/timer/call_every_handler.h>

#include <Hardware/include/distance_sensor.h>
#include <Hardware/include/i2c.h>

namespace wer {
	namespace hal {

		/* Configuration Constants */
		static constexpr uint8_t TREVO_BASEADDR =      			0x31; /* 7-bit address */

		/* TERARANGER Registers addresses */
		static constexpr uint8_t TERARANGER_MEASURE_REG =		0x00;		/* Measure range register */
		static constexpr uint8_t TERARANGER_WHO_AM_I_REG =  	0x01;        /* Who am I test register */
		static constexpr uint8_t TERARANGER_WHO_AM_I_REG_VAL = 	0xA1;

		/* Device limits */
		static constexpr float TREVO_MIN_DISTANCE = 			(0.50f);
		static constexpr float TREVO_MAX_DISTANCE = 			(60.0f);

		static constexpr uint32_t TERARANGER_CONVERSION_INTERVAL = 50000; /* 50ms */
		static constexpr uint8_t NUMBER_OF_STORED_VALUE = 2;

		class TeraRanger : public DistanceSensor
		{
		public:
			TeraRanger(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<I2c> i2c, int address = TREVO_BASEADDR);
			~TeraRanger();

			lib::ProcessResult init();
			lib::ProcessResult status() {return lib::ProcessResult::SUCCESS;};
			lib::ProcessResult enable();
			lib::ProcessResult disable();

			float getMeasure();

		private:
			uint8_t	_address;
			float _min_distance;
			float _max_distance;

			struct distance_report_s _distance[NUMBER_OF_STORED_VALUE];
			uint8_t _index;

			bool _sensor_ok;
			uint8_t _valid;
			bool _collect_phase;

			uint64_t _comms_errors;

			bool _enable;

			std::shared_ptr<CallEveryHandler> _call_every_handler;
			void* _timer_cookie;

			std::shared_ptr<I2c> _i2c;

			lib::ProcessResult	probe();
			void cycle();

			/**
			* Perform a poll cycle; collect from the previous measurement
			* and start a new one.
			*/
			int					measure();
			int					collect();
		};
	}
}