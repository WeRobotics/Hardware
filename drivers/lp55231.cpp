#include "lp55231.h"

#include <unistd.h>
#include <iostream>

// register stuff
#define REG_CNTRL1 0x00
#define REG_CNTRL2 0x01
#define REG_RATIO_MSB 0x02
#define REG_RATIO_LSB 0x03
#define REG_OUTPUT_ONOFF_MSB 0x04
#define REG_OUTPUT_ONOFF_LSB 0x05

// Per LED control channels - fader channel assig, log dimming enable, temperature compensation
#define REG_D1_CTRL 0x06
#define REG_D2_CTRL 0x07
#define REG_D3_CTRL 0x08
#define REG_D4_CTRL 0x09
#define REG_D5_CTRL 0x0a
#define REG_D6_CTRL 0x0b
#define REG_D7_CTRL 0x0c
#define REG_D8_CTRL 0x0d
#define REG_D9_CTRL 0x0e

// 0x0f to 0x15 reserved

// Direct PWM control registers
#define REG_D1_PWM  0x16
#define REG_D2_PWM  0x17
#define REG_D3_PWM  0x18
#define REG_D4_PWM  0x19
#define REG_D5_PWM  0x1a
#define REG_D6_PWM  0x1b
#define REG_D7_PWM  0x1c
#define REG_D8_PWM  0x1d
#define REG_D9_PWM  0x1e

// 0x1f to 0x25 reserved

// Drive current registers
#define REG_D1_I_CTL 0x26
#define REG_D2_I_CTL  0x27
#define REG_D3_I_CTL  0x28
#define REG_D4_I_CTL  0x29
#define REG_D5_I_CTL  0x2a
#define REG_D6_I_CTL  0x2b
#define REG_D7_I_CTL  0x2c
#define REG_D8_I_CTL  0x2d
#define REG_D9_I_CTL  0x2e

// 0x2f to 0x35 reserved

#define REG_MISC     0x36
#define REG_PC1      0x37
#define REG_PC2      0x38
#define REG_PC3      0x39
#define REG_STATUS_IRQ 0x3A
#define REG_INT_GPIO   0x3B
#define REG_GLOBAL_VAR 0x3C
#define REG_RESET      0x3D
#define REG_TEMP_CTL   0x3E
#define REG_TEMP_READ  0x3F
#define REG_TEMP_WRITE 0x40
#define REG_TEST_CTL   0x41
#define REG_TEST_ADC   0x42

// 0x43 to 0x44 reserved

#define REG_ENGINE_A_VAR 0x45
#define REG_ENGINE_B_VAR 0x46
#define REG_ENGINE_C_VAR 0x47

#define REG_MASTER_FADE_1 0x48
#define REG_MASTER_FADE_2 0x49
#define REG_MASTER_FADE_3 0x4A

// 0x4b Reserved

#define REG_PROG1_START 0x4C
#define REG_PROG2_START 0x4D
#define REG_PROG3_START 0x4E
#define REG_PROG_PAGE_SEL 0x4f

// Memory is more confusing - there are 6 pages, sel by addr 4f
#define REG_PROG_MEM_BASE 0x50
//#define REG_PROG_MEM_SIZE 0x;/
#define REG_PROG_MEM_END  0x6f

#define REG_ENG1_MAP_MSB 0x70
#define REG_ENG1_MAP_LSB 0x71
#define REG_ENG2_MAP_MSB 0x72
#define REG_ENG2_MAP_LSB 0x73
#define REG_ENG3_MAP_MSB 0x74
#define REG_ENG3_MAP_LSB 0x75

#define REG_GAIN_CHANGE 0x76


wer::hal::lp55231::lp55231(std::shared_ptr<wer::hal::I2c> i2c, uint8_t address) :
wer::hal::Led(),
_address(address),
_i2c(i2c)
{
}

wer::hal::lp55231::~lp55231() {
  for (int i = 0; i < 9; ++i)
  {
    setBrightness(i, 0);
  }
}

uint8_t wer::hal::lp55231::set(WER::HW::Color color, WER::HW::BlinkType blinkType) {
  uint8_t blue = (static_cast<uint32_t>(color) & 0x000000FF);
  uint8_t green = (static_cast<uint32_t>(color) & 0x0000FF00) >> 8;
  uint8_t red = (static_cast<uint32_t>(color) & 0x00FF0000) >> 16;

  setBrightness(0, green);
  setBrightness(1, blue);
  setBrightness(2, red);

  //std::cout << "R(" << unsigned(red) << ") G(" << unsigned(green) << ") B(" << unsigned(blue) << ")" << std::endl;

  //setBrightness(3, red);
  //setBrightness(4, green);
  //setBrightness(5, blue);

  return 0;
}

void wer::hal::lp55231::enable()
{
  // and re-enable
  writeReg(REG_CNTRL1, 0x40);

  // enable internal clock & charge pump & auto increment
  writeReg(REG_MISC, 0x5B);
}

void wer::hal::lp55231::disable()
{
  uint8_t val;

  val = readReg(REG_CNTRL1);
  val &= ~0x40;
  writeReg(REG_CNTRL1, val);
}

void wer::hal::lp55231::reset()
{
  // force reset
  writeReg(REG_RESET, 0xff);
}

bool wer::hal::lp55231::setBrightness(uint8_t channel, uint8_t value)
{
  if(channel >= 9)
  {
    return false;
  }

  writeReg(REG_D1_PWM + channel, value);
  return true;
}

bool wer::hal::lp55231::setLogBrightness(uint8_t channel)
{
  if(channel >= 9)
  {
    return false;
  }

  writeReg(REG_D1_CTRL + channel, 20);
  return true;
}

bool wer::hal::lp55231::setDriveCurrent(uint8_t channel, uint8_t value)
{
  if(channel > 9)
  {
    return false;
  }

  // tbd...
  writeReg(REG_D1_I_CTL + channel, value);
  return true;
}

int8_t wer::hal::lp55231::readDegC()
{
  uint8_t status;
  int8_t  temperature;

  writeReg(REG_TEMP_CTL, 0x04);

  do
  {
    status = readReg(REG_TEMP_CTL);
  }while(status & 0x80);

  temperature = (int8_t)readReg(REG_TEMP_READ);

  return temperature;
}

float  wer::hal::lp55231::readLEDADC(uint8_t channel)
{
  uint8_t reading;
  float volts;

  reading = readADCInternal(channel & 0x0f);

  volts = (reading * 0.03) - 1.478;

  return volts;
}

float  wer::hal::lp55231::readVoutADC()
{
  uint8_t reading;
  float volts;

  reading = readADCInternal(0x0f);

  volts = (reading * 0.03) - 1.478;
  return volts;
}

float  wer::hal::lp55231::readVddADC()
{
  uint8_t reading;
  float volts;

  reading = readADCInternal(0x10);

  volts = (reading * 0.03) - 1.478;
  return volts;
}

float  wer::hal::lp55231::readIntADC()
{
  uint8_t reading;
  float volts;

  reading = readADCInternal(0x11);

  volts = (reading * 0.03) - 1.478;
  return volts;
}

void wer::hal::lp55231::overrideIntToGPO(bool overrideOn )
{
  uint8_t regVal;

  if(overrideOn)
  {
      regVal = 0x04;
  }
  else
  {
    regVal = 0;
  }

  writeReg(REG_INT_GPIO, regVal);
}

bool wer::hal::lp55231::setIntGPOVal(bool value)
{
  uint8_t regVal;

  regVal = readReg(REG_INT_GPIO);

  if (!(regVal & 0x04))
  {
    return false;
  }

  if(value)
  {
    regVal |= 0x01;
  }
  else
  {
    regVal &= ~0x01;
  }

  writeReg(REG_INT_GPIO, regVal);
  return true;
}




bool wer::hal::lp55231::setMasterFader(uint8_t engine, uint8_t value)
{
  if((engine == 0) || (engine > 3))
  {
    return false;
  }

  writeReg(REG_MASTER_FADE_1 + (engine - 1 ), value);
  return true;
}

void wer::hal::lp55231::showControls()
{
}

bool wer::hal::lp55231::loadProgram(const uint16_t* prog, uint8_t len)
{
  uint8_t page;

  if(len > 96)
  {
    // TBD - support multiple pages

    return false;
  }

  // temp - try to tell it what to reset to?
  //writeReg(REG_PROG1_START, 0x08);

  // set up program write
  // start in execution disabled mode (0b00)
  // required to get into load mode.
  // "Load program mode can be entered from the disabled mode only.  be
  // entered from the disabled mode only."
  writeReg(REG_CNTRL2, 0x00);
  writeReg(REG_CNTRL2, 0x15);

  waitForBusy();

  // try to write program from example
  // datasheet says MSB of each instruction is in earlier address
  // TBD: could optimize with a sequence of byte writes, using auto increment

  // use auto-increment of chip - enabled in MISC.
  // If it gets turned off, this breaks.  TBD: set it explicitly?

  // Write complete pages, setting page reg for each.
  for(page = 0; page < (len/16); page++)
  {
    writeReg(REG_PROG_PAGE_SEL, page);

    for(uint8_t i = 0; i < 16; i++)
    {
      uint8_t buffer[3];
      buffer[0] = (REG_PROG_MEM_BASE + (i*2));
      buffer[1] = (prog[(i + (page*16))]>> 8) & 0xff;
      buffer[2] = prog[i + (page*16)] & 0xff;

      _i2c->set(_address, buffer, 3, nullptr);
    }
  }

  // plus any incomplete pages
  page = len/16;
  writeReg(REG_PROG_PAGE_SEL, page);
  for(uint8_t i = 0; i < (len%16); i++)
  {
    uint8_t buffer[3];
    buffer[0] = (REG_PROG_MEM_BASE + (i*2));
    buffer[1] = (prog[i + (page*16)]>> 8) & 0xff;
    buffer[2] = prog[i + (page*16)] & 0xff;

    _i2c->set(_address, buffer, 3, nullptr);
  }

  writeReg(REG_CNTRL2, 0x00);

  return true;
}

bool wer::hal::lp55231::verifyProgram(const uint16_t* prog, uint8_t len)
{
  uint8_t page;

  if(len > 96)
  {
    // TBD - support multiple pages

    return false;
  }

  writeReg(REG_CNTRL2, 0x00);// engines into disable mode - required for entry to program mode.
  writeReg(REG_CNTRL2, 0x15);// engines into program mode?
  //try to read  program from chip,
  // datasheet says MSB of each instruction is in earlier address
  // TBD: could optimize with a sequence of byte writes, using auto increment

  // Auto-increment may not work for sequential reads...
  for(page = 0; page < (len/16); page++)
  {
    writeReg(REG_PROG_PAGE_SEL, page);

    for(uint8_t i = 0; i < 16; i++)
    {
      uint16_t msb, lsb;
      uint8_t addr = (REG_PROG_MEM_BASE + (i*2));

      msb = readReg(addr);
      lsb = readReg(addr + 1);

      lsb |= (msb << 8);

      if(lsb != prog[i + (page*16)])
      {
        return false;
      }
    }
  }

  // plus any incomplete pages
  page = len/16;
  writeReg(REG_PROG_PAGE_SEL, page);
  for(uint8_t i = 0; i < (len%16); i++)
  {
    uint16_t msb, lsb;
    uint8_t addr = (REG_PROG_MEM_BASE + (i*2));

    msb = readReg(addr);
    lsb = readReg(addr + 1);

    lsb |= (msb << 8);

    if(lsb != prog[i + (page*16)])
    {
      return false;
    }
  }

  writeReg(REG_CNTRL2, 0x00);

  return true;
}

bool wer::hal::lp55231::setEngineEntryPoint(uint8_t engine, uint8_t addr)
{

  if(engine > 2)
  {
    return false;
  }

  writeReg(REG_PROG1_START + engine, addr);

  return true;
}

bool wer::hal::lp55231::setEnginePC(uint8_t engine, uint8_t addr)
{
  uint8_t control_val, control2_val, temp;;

  if(engine > 2)
  {
    return false;
  }

  // There are 6 pages of 16 instructions each (0..95)
  if(addr >= 96)
  {
    return false;
  }

  // In Ctl1 descriptions:
  //00 = hold: Hold causes the execution engine to finish the current instruction and then stop. Program counter
  //(PC) can be read or written only in this mode.

  control_val = readReg(REG_CNTRL1);
  control2_val = readReg(REG_CNTRL2);

  temp = (control_val & ~(0x30 >> (engine * 2)));

  //writeReg(REG_CNTRL2, 0x3ff); // halt engines immediately.
  writeReg(REG_CNTRL2, 0xff); // halt engines immediately.
  writeReg(REG_CNTRL1, temp);// put engine in load mode

  writeReg(REG_PC1 + engine, addr);

  // restore prev mode?
  writeReg(REG_CNTRL1, control_val);
  writeReg(REG_CNTRL2, control2_val);

  return true;
}

uint8_t wer::hal::lp55231::getEnginePC(uint8_t engine)
{
  // must set Hold to touch PC...
  uint8_t pc_val;

  if(engine > 2)
  {
    return -1;
  }

  pc_val = readReg(REG_PC1 + engine);

  return(pc_val);
}

uint8_t wer::hal::lp55231::getEngineMap(uint8_t engine)
{
  if(engine > 2)
  {
    return -1;
  }

  return(readReg(REG_ENG1_MAP_LSB + engine));
}

bool wer::hal::lp55231::setEngineModeHold(uint8_t engine)
{
  uint8_t val;

  if(engine > 2)
  {
    return false;
  }

  // Set the enghine to "free running" execution type
  // bits to 0b00
  val = readReg(REG_CNTRL1);
  val &= ~(0x30 >> (engine * 2));
  //val |= (0x10 >> (engine * 2));
  writeReg(REG_CNTRL1, val );

  return(true);
}

bool wer::hal::lp55231::setEngineModeStep(uint8_t engine)
{
  uint8_t val;

  if(engine > 2)
  {
    return false;
  }

  // Set the enghine to "single step" execution type
  // bits to 0b01
  val = readReg(REG_CNTRL1);
  val &= ~(0x30 >> (engine * 2));
  val |= (0x10 >> (engine * 2));
  writeReg(REG_CNTRL1, val );

  return(true);
}

bool wer::hal::lp55231::setEngineModeOnce(uint8_t engine)
{
  uint8_t val;

  // This mode might not be the most useful.
  // It executes the pointed instruction, then
  // sets exec mode to hold, and resets the PC.
  // It's an astringent form of step (which advances the PC, instead)

  if(engine > 2)
  {
    return false;
  }

  // Set the enghine to "one shot" execution type
  // Bits to 0b11
  val = readReg(REG_CNTRL1);
  val |= (0x30 >> (engine * 2));


  writeReg(REG_CNTRL1, val );

  return(true);

}

bool wer::hal::lp55231::setEngineModeFree(uint8_t engine)
{
  uint8_t val;

  if(engine > 2)
  {
    return false;
  }

  // Set the enghine to "free running" execution type
  val = readReg(REG_CNTRL1);
  val &= ~(0x30 >> (engine * 2));
  val |= (0x20 >> (engine * 2));


  writeReg(REG_CNTRL1, val );

  return(true);
}

uint8_t wer::hal::lp55231::getEngineMode(uint8_t engine)
{
  uint8_t val;

  if(engine > 2)
  {
    return false;
  }

  val = readReg(REG_CNTRL1);
  val >>= (engine * 2);
  val &= 0x03;
  return(val);

}


bool wer::hal::lp55231::setEngineRunning(uint8_t engine)
{
  uint8_t val;

  if(engine > 2)
  {
    return false;
  }

  // This assumes that a suitable run mode in CNTRL1 was already selected.
  // start execution by setting "run program" mode
  val = readReg(REG_CNTRL2);
  val &= ~(0x30 >> (engine * 2));
  val |= (0x20>> (engine * 2));
  writeReg(REG_CNTRL2, val);

  return true;
}



uint8_t wer::hal::lp55231::clearInterrupt()
{
  // TBD: make this more channel specific?
  return( readReg(REG_STATUS_IRQ) & 0x07);
}

void wer::hal::lp55231::waitForBusy()
{
  uint8_t val;

  // then wait to change modes
  do
  {
    val = readReg(REG_STATUS_IRQ) & 0x10; // engine busy bit
  }
  while(val);

}

/////////////////////////////////////////////////////////////

uint8_t wer::hal::lp55231::readADCInternal(uint8_t channel)
{
  writeReg(REG_TEST_CTL, 0x80 |(channel & 0x1f));

  // No reg bit to poll for completing - simply delay.
  usleep(3000);

  return(readReg(REG_TEST_ADC));

  //volts = (reading * 0.03) - 1.478;


}

uint8_t wer::hal::lp55231::readReg(uint8_t registerAddr)
{
  uint8_t buffer[1] = {registerAddr};
  _i2c->set(_address, buffer, 1, nullptr);

  usleep(50);
  
  if(_i2c->get(_address, buffer, 1) == 0) {
    return buffer[0];
  }
  else {
    return 0xFF;
  }
}

void wer::hal::lp55231::writeReg(uint8_t registerAddr, uint8_t data)
{
  uint8_t buffer[2] = {registerAddr, data};
  _i2c->set(_address, buffer, 2, nullptr);
}