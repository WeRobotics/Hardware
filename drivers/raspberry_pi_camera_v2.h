#ifndef HARDWARE_INCLUDE_RASPBERRY_PI_CAMERA_V2_H
#define HARDWARE_INCLUDE_RASPBERRY_PI_CAMERA_V2_H

#include <camera.h>

#include <raspicam/raspicam_cv.h>

namespace wer {
namespace hal {
class RaspberryPiCameraV2 : public Camera {
 public:
  RaspberryPiCameraV2();
  ~RaspberryPiCameraV2();

  uint8_t init();
  uint8_t takePicture(cv::Mat& picture, uint8_t save_pictures = 0,
                      char* path = "");
  /**
   * [setBrightness description]
   * @param brightness [value between 0 and 100]
   */
  void setBrightness(uint8_t brightness);
  /**
   * [setContrast description]
   * @param contrast [value between 0 and 100]
   */
  void setContrast(uint8_t contrast);
  /**
   * [setISO description]
   * @param ISO [value between 0 and 100]
   */
  void setISO(uint8_t ISO);
  /**
   * [setExposure description]
   * @param exposure [value between 0 and 100, where 100 is 33ms]
   */
  void setExposure(uint8_t exposure);
  /**
   * [setGrayscale function will setup camera to take only grayslace
   * photos]
   */
  void setGrayscale();

 private:
  uint8_t picture_number_ = 1;
  raspicam::RaspiCam_Cv camera_;
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_RASPBERRY_PI_CAMERA_V2_H