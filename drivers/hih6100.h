#ifndef HARDWARE_INCLUDE_Hih6100_H
#define HARDWARE_INCLUDE_Hih6100_H

#include <sys/types.h>

#include <atomic>
#include <chrono>

#include <call_every_handler.h>
#include <Library/log/log_manager.h>

#include "i2c.h"
#include "sensor.h"

namespace wer {
namespace hal {

/**
 * This class is the implementation of the sensor component Honeywell Humidicon
 * HIH6100.
 */
class Hih6100 {

 public:
  Hih6100(std::shared_ptr<lib::CallEveryHandler> call_every_handler,
          std::shared_ptr<hal::I2c> i2c, uint8_t address);

  float temperature() { return temperature_; };

  float humidity() { return humidity_; };

  uint8_t connected() { return connected_; };

  uint8_t isEnabled() { return enable_; }

  void enable(float sample_rate);
  void disable();

 private:
  lib::LogMsg* log_;

  float temperature_;
  float humidity_;

  uint8_t connected_;

  bool enable_;
  bool collect_phase_;
  bool faulty_;

  std::chrono::time_point<std::chrono::steady_clock> measure_time_;

  std::shared_ptr<wer::lib::CallEveryHandler> call_every_handler_;
  uint8_t address_;

  void* cookie_;

  std::shared_ptr<wer::hal::I2c> i2c_;

  void data_fetch();
  void measurement_request();
  void cycle();
};
}  // hal
}  // wer
#endif  // HARDWARE_INCLUDE_Hih6100_H