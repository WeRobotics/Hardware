#ifndef IO_EXPANDER_H
#define IO_EXPANDER_H

#define IO_NB_PIN 8

#include <sys/types.h>
#include <iostream>
#include <list>

#include "Hardware/include/gpio.h"
#include "Hardware/include/i2c.h"

namespace wer {
namespace hal {

class Tca9534 : public Gpio {
 public:
  Tca9534(std::shared_ptr<I2c> i2c, uint8_t address);

  uint8_t init(uint8_t pin, GpioMode mode);

  uint8_t digitalRead(uint8_t pin, uint8_t* value);
  uint8_t digitalWrite(uint8_t pin, uint8_t value);

  uint8_t registerCallback(IoPin gpio, GpioEvent event,
                           std::function<void(IoPin, GpioEvent)> callback);

 private:
  uint8_t pin_mode_;
  uint8_t pin_input_;
  uint8_t pin_output_;
  uint8_t pin_new_;

  std::shared_ptr<I2c> i2c_;
  uint8_t address_;

  void send(uint8_t data, uint8_t registerAddr);
};
}  // namespace hal
}  // namespace wer

#endif