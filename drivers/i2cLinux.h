#ifndef HARDWARE_INCLUDE_I2CLINUX_H_
#define HARDWARE_INCLUDE_I2CLINUX_H_

#include "../include/i2c.h"

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

namespace wer {
namespace hal {

class I2cLinux : public I2c {
 public:
  I2cLinux(const char* path = "/dev/i2c-1");
  ~I2cLinux();

  uint8_t set(uint8_t address, uint8_t data);
  uint8_t set(uint8_t address, const uint8_t* data, uint8_t dataLength);
  uint8_t get(uint8_t address, uint8_t* data, uint8_t dataLength);

  void set(uint8_t address, uint8_t data,
           std::function<void(uint8_t)> callback);
  void set(uint8_t address, const uint8_t* data, uint8_t dataLength,
           std::function<void(uint8_t)> callback);
  void get(uint8_t address, uint8_t dataLength,
           std::function<void(uint8_t, uint8_t*, uint8_t)> callback);

 private:
  struct Tasks {
    uint8_t address;
    uint8_t* data;
    uint8_t dataLength;

    std::function<void(uint8_t)> setCallback;
    std::function<void(uint8_t, uint8_t*, uint8_t)> getCallback;
  };

  int _fd;
  std::mutex _fdMutex;

  std::thread* _thread;
  std::atomic<bool> _shouldExit;

  std::queue<Tasks> _tasks;
  std::mutex _tasksMutex;

  std::condition_variable _pendingCondition;
  bool _pendingSpurious;

  void task();
};

}  // hal
}  // wer

#endif  // HARDWARE_INCLUDE_I2CLINUX_H_