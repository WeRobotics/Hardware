#include "max31790.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// #include <cmath>

namespace wer {
namespace hal {

// CONSTRUCTOR
Max31790::Max31790(std::shared_ptr<I2c> i2c, uint8_t address_)
    : i2c_(i2c), address_(address_) {
  max31790NbChannels_ = 6;
  maxPwm_ = 255;
  minPwm_ = 0;

  // currentBaseFreqReg = 0b00000000;

  // kSpinUpConfig_ = 0b00000000;

  for (uint8_t i = 0; i < max31790NbChannels_; i++) {
    pwmValues[i] = minPwm_;
    freqValues[i] = minPwm_;
  }

  // disabled_ = true;
  enabled_ = false;
}

void Max31790::enable() {
  if (!enabled_) {
    enabled_ = true;
    // disabled_ = false;
    for (uint8_t i = 0; i < max31790NbChannels_; i++) {
      Max31790::setChannel(i, pwmValues[i]);
    }
  }
}

void Max31790::disable() {
  if (enabled_) {
    // disabled_ = true;
    enabled_ = false;
  }
  for (uint8_t i = 0; i < max31790NbChannels_; i++) {
    Max31790::setChannel(i, minPwm_);
  }
}

// set PWM of channel
void Max31790::setChannel(uint8_t channel, uint8_t dutyCycle) {
  if (enabled_ && dutyCycle < 256 && channel >= 0 &&
      channel < max31790NbChannels_) {
    writeReg(kPwmRegChannel0_ + (channel) * 2, dutyCycle);
    pwmValues[channel] = dutyCycle;
  }
}

// Set prescaler of channel. The prescaler is the register divider: it
// divides the base frequency to get the final PWM frequency
// void Max31790::setPrescaler(uint8_t channel, uint8_t prescaler) {
//   if (prescaler < 256 && channel > 0 && channel < max31790NbChannels_ + 1) {
//     writeReg(kFreqRegChannel1_ + (channel - 1) * 16, prescaler);
//     freqValues[channel - 1] = prescaler;
//   }
// }

// Set BASE FREQUENCY of channel
// available options: 26000, 19531, 4882 or 2441 Hz
// void Max31790::setBaseFrequency(uint8_t channel, uint16_t baseFrequency) {
//   if (enabled_ && channel > 0 && channel < max31790NbChannels_ + 1) {
//     uint8_t mask = 0b00000000;
//     uint8_t freqMask = 0b00000000;
//     switch (channel) {
//       case 1:
//         mask = 0b00000011;
//         break;
//       case 2:
//         mask = 0b00001100;
//         break;
//       case 3:
//         mask = 0b00110000;
//         break;
//       default:
//         break;
//     }

//     switch (baseFrequency) {
//       case 2441:
//         freqMask = 0b00111111;
//         break;
//       case 4882:
//         freqMask = 0b00101010;
//         break;
//       case 19531:
//         freqMask = 0b00010101;
//         break;
//       case 26000:
//         freqMask = 0b00000000;
//         break;
//       default:
//         break;
//     }
//     // mask & freqMask: isolate the two bits of interest for a given channel
//     // currentBaseFreqReg & ~mask to reset the two corresponding bits in
//     // currentBaseFreqReg
//     // -> the two bits of interest are placed inside currentBaseFreqReg
//     currentBaseFreqReg = (mask & freqMask) | (currentBaseFreqReg & ~mask);
//     writeReg(kBaseFreqReg_, currentBaseFreqReg);
//   }
// }

// put PWM to max value
void Max31790::setChannelOn(uint8_t channel) {
  setChannel(channel, maxPwm_);
}

// put pwm to min value
void Max31790::setChannelOff(uint8_t channel) {
  setChannel(channel, minPwm_);
}

// selects the frequency at which the fans will spin. First, finds an
// approximate match for the desired frequency then, sets the corresponding base
// frequency and prescaler to make the fans spin at that approximated frequency
// CAREFUL: for high freq (e.g. 15kHz) the error is way bigger than for small
// freq.
void Max31790::setFrequency(uint8_t channel, uint16_t desiredFreq) {
  // if (enabled_ && channel > 0 && channel < max31790NbChannels_ + 1) {
  //   for (uint8_t i = 0; i < kNbAvailableFreq_; i++) {
  //     if (desiredFreq <
  //         uint16_t(round(kAvailableFreq[i] /
  //                        2))) {  // '/2' to make sure there is an OK approx.
  //                                // freq. for that base freq
  //       uint8_t prescaler = uint8_t(round(kAvailableFreq[i] / desiredFreq));
  //       setBaseFrequency(channel, kAvailableFreq[i]);
  //       setPrescaler(channel, prescaler);
  //       break;
  //     }
  //     if (i == kNbAvailableFreq_ -
  //                  1) {  // i.e. freq > 13kHz choose the nearest base frequency
  //       uint8_t prescaler = 1;  // no need of a prescaler

  //       if (abs(kAvailableFreq[3] - desiredFreq) <
  //           abs(kAvailableFreq[2] - desiredFreq)) {
  //         setBaseFrequency(channel, kAvailableFreq[3]);
  //       } else {
  //         setBaseFrequency(channel, kAvailableFreq[2]);
  //       }

  //       setPrescaler(channel, prescaler);
  //       break;
  //     }
  //   }
  // }
}

// void Max31790::setSpinUpRoutine(uint8_t channel, bool kick, uint8_t driveLevel,
//                                uint16_t spinUpTime) {
//   kSpinUpConfig_ &= 0b00000000;

//   // remove kick
//   if (kick == false) {
//     kSpinUpConfig_ |= 0b00100000;
//   }

//   if (channel > 0 && channel < max31790NbChannels_ + 1) {
//     switch (driveLevel) {
//       case 30:
//         // kSpinUpConfig_ |= 0b00000000;
//         break;
//       case 35:
//         kSpinUpConfig_ |= 0b00000100;
//         break;
//       case 40:
//         kSpinUpConfig_ |= 0b00001000;
//         break;
//       case 45:
//         kSpinUpConfig_ |= 0b00001100;
//         break;
//       case 50:
//         kSpinUpConfig_ |= 0b00010000;
//         break;
//       case 55:
//         kSpinUpConfig_ |= 0b00010100;
//         break;
//       case 60:
//         kSpinUpConfig_ |= 0b00011000;
//         break;
//       case 65:
//         kSpinUpConfig_ |= 0b00011100;
//         break;
//       default:
//         std::cout << "ERROR ! INCORRECT SPIN UP LEVEL\n"
//                   << std::endl;  // replace with real ERROR
//         break;
//     }

//     switch (spinUpTime) {
//       case 250:
//         // kSpinUpConfig_ |= 0b00000000;
//         break;
//       case 500:
//         kSpinUpConfig_ |= 0b00000001;
//         break;
//       case 1000:
//         kSpinUpConfig_ |= 0b00000010;
//         break;
//       case 2000:
//         kSpinUpConfig_ |= 0b00000011;
//         break;
//       default:
//         std::cout << "ERROR ! INCORRECT SPIN UP DURATION\n"
//                   << std::endl;  // replace with real ERROR
//         break;
//     }

//     writeReg(kSpinUpRegChannel1_ + (channel - 1) * 16, kSpinUpConfig_);
//   }
// }

void Max31790::writeReg(uint8_t registerAddr, uint8_t data) {
  uint8_t buffer[2] = {registerAddr, data};
  i2c_->set(address_, buffer, 2);
}

}  // namespace hal
}  // namespace wer
