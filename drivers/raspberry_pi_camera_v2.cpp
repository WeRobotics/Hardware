#include <raspberry_pi_camera_v2.h>

#include <sys/statvfs.h>

#include <string>

namespace wer {
namespace hal {
RaspberryPiCameraV2::RaspberryPiCameraV2() {
  status_ = CameraStatus::NONINITIALIZED;
  camera_model_ = "RaspberryPiCameraV2";
}

RaspberryPiCameraV2::~RaspberryPiCameraV2() {}

uint8_t RaspberryPiCameraV2::init() {
  if (camera_.open()) {
    status_ = CameraStatus::CONNECTED;
    return 0x00;
  } else {
    status_ CameraStatus::NONINITIALIZED;
    return 0x01;
  }
}

uint8_t RaspberryPiCameraV2::takePicture(cv::Mat& picture,
                                         uint8_t save_pictures = 0,
                                         char* path = "") {
  if (status_ == CameraStatus::CONNECTED) {
    if (save_pictures) {
      save_pictures_ = 1;
      path_ = path;
    }
    camera_.grab();
    camera_.retrieve(picture);
    if (save_pictures_) {
      struct statvfs buffer;
      int ret = statvfs(".", &buffer);
      double MB = 1024 * 1024;

      if (!ret) {
        const double available =
            (double)(buffer.f_bavail * buffer.f_bsize) / MB;
        if (available > 100)  // more than 100MB available
        {
          std::string full_name =
              camera_model_ + "_" + std::to_string(picture_number_) + ".jpg";
          imwrite(full_name, image);
          picture_number_++;
        }
      }
    }
    return 0x00;
  } else {
    return 0x01;
  }
}

void RaspberryPiCameraV2::setBrightness(uint8_t brightness) {
  camera_.set(CAP_PROP_BRIGHTNESS, brightness);
}
void RaspberryPiCameraV2::setContrast(uint8_t contrast) {
  camera_.set(CAP_PROP_CONTRAST, contrast);
}
void RaspberryPiCameraV2::setISO(uint8_t ISO) {
  camera_.set(CAP_PROP_GAIN, ISO);
}
void RaspberryPiCameraV2::setExposure(uint8_t exposure) {
  camera_.set(CAP_PROP_EXPOSURE, exposure);
}
void RaspberryPiCameraV2::setGreyscale() {
  camera_.set(CAP_PROP_FORMAT, CV_8UC1);
}

}  // hal
}  // wer