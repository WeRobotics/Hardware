#ifndef Pca9632_H
#define Pca9632_H

#include <iostream>
#include <list>
#include <sys/types.h>
#include <memory>

#include "Hardware/include/i2c.h"

namespace wer {
namespace hal {

class Pca9632 {
	public:	
		Pca9632(std::shared_ptr<I2c> i2c, uint8_t address);
		// static Pca9632* getByAddress(uint8_t address);
		// static void deleteAll();

		void setIndividualPwm(uint8_t channel, uint8_t pwm);
		void setGlobalPwm(uint8_t pwm);
		void setGlobalFrequency(uint8_t frequency);
		void setMode(uint8_t channel, uint8_t mode);
		void reset();

		void channelOn(uint8_t channel);
		void channelPwm(uint8_t channel);
		void channelOff(uint8_t channel);
		
	private:
		// static std::list<Pca9632*> _connectedDevices;

		std::shared_ptr<I2c> i2c_;
  		uint8_t address_;

		void writeReg(uint8_t registerAddr, uint8_t data);
		uint8_t readReg(uint8_t registerAddr);

		uint8_t _outputState;

		void setBit(uint8_t &byte, uint8_t bit, uint8_t value);
};

}  // namespace hal
}  // namespace wer

#endif
