#include "dynamixel_xm_looped_steps.h"

namespace wer {
namespace hal {

DynamixelXmLoopedSteps::DynamixelXmLoopedSteps(uint8_t id, const char* portName,
                                               uint32_t baudrate)
    : DynamixelXmSeries(id, portName, baudrate) {
  actual_step_ = 0;
  nb_step_ = 0;
}

void DynamixelXmLoopedSteps::SetStepList(std::vector<int32_t> step_list,
                                         uint16_t nb_step,
                                         uint16_t actual_step) {
  step_list_ = step_list;
  nb_step_ = nb_step;
  actual_step_ = actual_step;
}

bool DynamixelXmLoopedSteps::SetResetPosition() {
  int32_t position;

  SetControlMode(EXT_POS_CTRL_MODE);

  if (ClearMultiTurn()) {
    log_->error_ << "ERROR: ClearMultiTurn failed !" << '\n';
    return true;
  }

  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }
  reset_position_ = position;

  return false;
}

uint8_t DynamixelXmLoopedSteps::StepInc(bool synchro, uint16_t velocity) {
  log_->info_ << "Starting StepInc (synchro: " << synchro << ")" << '\n';
  uint8_t error_flag = false;
  uint16_t next_step;
  int32_t next_position;
  int32_t position;

  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }
  if (position >= 4096 || position < 0) {
    if (ClearMultiTurn()) {
      log_->error_ << "ERROR: ClearMultiTurn failed !" << '\n';
      return true;
    }
    if (GetPosition(&position) == true) {
      log_->error_ << "ERROR: unable to read position !" << '\n';
      return true;
    }
  }

  next_step = (actual_step_ + 1) % nb_step_;
  next_position = step_list_[next_step];

  if (next_position < position) {
    next_position += 4096;
  }

  error_flag = SetPosition(next_position, synchro, velocity);

  actual_step_ = next_step;

  if (error_flag != false) {
    log_->error_
        << "ERROR: failure in corresponding 'SetPosition' function call"
        << '\n';
    log_->error_ << "ERROR: stepInc not possible !" << '\n';
    log_->info_ << "State: step_mode Complete(INF)"
                << " actualStep " << (unsigned)actual_step_ << " nbStep "
                << (unsigned)nb_step_ << '\n';
    return true;
  }

  log_->info_ << "Finished stepInc: actual step is " << (unsigned)actual_step_
              << '\n';
  return error_flag;
}

uint8_t DynamixelXmLoopedSteps::StepDec(bool synchro, uint16_t velocity) {
  uint8_t error_flag = false;
  uint16_t next_step;
  int32_t next_position;
  int32_t position;

  log_->info_ << "Starting stepDec (synchro: " << synchro << ")" << '\n';

  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }

  if (position >= 4096 || position < 0) {
    if (ClearMultiTurn()) {
      log_->error_ << "ERROR: ClearMultiTurn failed !" << '\n';
      return true;
    }
    if (GetPosition(&position) == true) {
      log_->error_ << "ERROR: unable to read position !" << '\n';
      return true;
    }
  }

  next_step = (actual_step_ == 0) ? nb_step_ - 1 : actual_step_ - 1;
  next_position = step_list_[next_step];

  if (next_position > position) {
    next_position -= 4096;
  }

  error_flag = SetPosition(next_position, synchro, velocity);

  actual_step_ = next_step;

  if (error_flag != false) {
    log_->error_
        << "ERROR: failure in corresponding 'SetPosition' function call !"
        << '\n';
    log_->error_ << "ERROR: stepDec not possible !" << '\n';
    log_->info_ << "State: step_mode Complete(INF)"
                << " / acutalStep " << (unsigned)actual_step_ << " / nbStep "
                << (unsigned)nb_step_ << '\n';
    return true;
  }

  log_->info_ << "Finished stepDec: actual step is "
              << (unsigned int)actual_step_ << '\n';
  return error_flag;
}
uint8_t DynamixelXmLoopedSteps::UpdateActualStepToClosest(uint8_t direction,
                                                          bool move,
                                                          bool synchro) {
  int32_t min_distance = 4096;
  int32_t position;
  int offset = 0;

  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }

  if (position >= 4096 || position < 0) {
    if (ClearMultiTurn()) {
      log_->error_ << "ERROR: ClearMultiTurn failed !" << '\n';
      return true;
    }
    if (GetPosition(&position) == true) {
      log_->error_ << "ERROR: unable to read position !" << '\n';
      return true;
    }
  }

  for (int i = 0; i < nb_step_; i++) {
    int32_t distance = step_list_[i] > position ? step_list_[i] - position
                                                : position - step_list_[i];
    if (distance < min_distance) {
      min_distance = distance;
      offset = i;
    }
  }

  if (direction == MOTOR_DIR_CCW) {
    if (step_list_[offset] < position + 50)
      actual_step_ = offset;
    else
      actual_step_ = (offset == 0) ? nb_step_ - 1 : offset - 1;
    ;

    if (min_distance > 50 && move) {
      if (StepInc(synchro)) {
        log_->error_
            << "ERROR: failure in corresponding 'stepInc' function call"
            << '\n';
        return true;
      }
    }
  } else {
    if (step_list_[offset] > position - 50)
      actual_step_ = offset;
    else
      actual_step_ = (offset + 1) % nb_step_;

    if (min_distance > 50 && move) {
      if (StepDec(synchro)) {
        log_->error_
            << "ERROR: failure in corresponding 'stepDec' function call"
            << '\n';
        return true;
      }
    }
  }

  log_->info_ << "updateActualStepToClosest, actual step is: "
              << (unsigned)actual_step_ << '\n';
  return false;
}

uint8_t DynamixelXmLoopedSteps::SetPositionToStep(uint16_t step, bool synchro) {
  uint8_t error_flag = false;
  log_->info_ << "Going to step " << (unsigned)step << " (synchro: " << synchro
              << ")" << '\n';

  if (step < nb_step_) {
    error_flag = SetPosition(step_list_[step], synchro);

    if (error_flag == false) {
      actual_step_ = step;
      log_->info_ << "Arrived to desired step !" << '\n';
      return error_flag;
    }
    log_->error_
        << "ERROR: failure in corresponding 'SetPosition' function call"
        << '\n';
    log_->error_ << "ERROR: wrong motor mode OR step higher than max" << '\n';
    return error_flag;
  } else {
    log_->error_ << "ERROR: step higher than max" << '\n';
    return false;
  }
}

uint16_t DynamixelXmLoopedSteps::GetActualStep() { return actual_step_; }

int32_t DynamixelXmLoopedSteps::GetResetPosition() {
  log_->info_ << "Reset position is: " << reset_position_ << '\n';
  return reset_position_;
}

uint8_t DynamixelXmLoopedSteps::IsOnStep() {
  int32_t position;
  if (GetPosition(&position) == true) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }
  if (position - step_list_[actual_step_] < 30 ||
      position - step_list_[actual_step_] > -30) {
    log_->info_ << "DynamixelMotor is on step " << (unsigned int)actual_step_
                << '\n';
    return false;
  }
  log_->error_ << "ERROR: motor is NOT on step " << (unsigned)actual_step_
               << " (" << step_list_[actual_step_] << "), current position is "
               << position << ")" << '\n';
  return true;
}

}  // namespace hal
}  // namespace wer