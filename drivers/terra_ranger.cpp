#include "terra_ranger.h"

#include <cstring>
#include <iostream>
#include <limits>

#include <Library/logging/log.h>

namespace wer {
	namespace hal {

		static constexpr uint8_t crc_table[] = {
			0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31,
			0x24, 0x23, 0x2a, 0x2d, 0x70, 0x77, 0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65,
			0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, 0xe0, 0xe7, 0xee, 0xe9,
			0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd,
			0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b, 0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1,
			0xb4, 0xb3, 0xba, 0xbd, 0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
			0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, 0xb7, 0xb0, 0xb9, 0xbe,
			0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a,
			0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16,
			0x03, 0x04, 0x0d, 0x0a, 0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42,
			0x6f, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7d, 0x7a, 0x89, 0x8e, 0x87, 0x80,
			0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
			0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8,
			0xdd, 0xda, 0xd3, 0xd4, 0x69, 0x6e, 0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c,
			0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, 0x19, 0x1e, 0x17, 0x10,
			0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34,
			0x4e, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f,
			0x6a, 0x6d, 0x64, 0x63, 0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
			0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, 0xae, 0xa9, 0xa0, 0xa7,
			0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91, 0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83,
			0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef,
			0xfa, 0xfd, 0xf4, 0xf3
		};

		static uint8_t crc8(uint8_t *p, uint8_t len)
		{
			uint16_t i;
			uint16_t crc = 0x0;

			while (len--) {
				i = (crc ^ *p++) & 0xFF;
				crc = (crc_table[i] ^ (crc << 8)) & 0xFF;
			}

			return crc & 0xFF;
		}
	}
}

wer::hal::TeraRanger::TeraRanger(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, int address) :
	_address(address),
	_min_distance(-1.0f),
	_max_distance(-1.0f),
	_index(0),
	_sensor_ok(false),
	_valid(false),
	_collect_phase(false),
	_comms_errors(0),
	_enable(false),
	_call_every_handler(call_every_handler),
	_i2c(i2c)
{
	memset(_distance, 0, sizeof _distance);
}

wer::hal::TeraRanger::~TeraRanger()
{
	disable();
}

wer::lib::ProcessResult wer::hal::TeraRanger::init()
{
    if(probe() == wer::lib::ProcessResult::SUCCESS) {
    	return wer::lib::ProcessResult::NOT_CONNECTED;
    }

    _sensor_ok = true;

	_min_distance = TREVO_MIN_DISTANCE;
	_max_distance = TREVO_MAX_DISTANCE;

	return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult wer::hal::TeraRanger::enable() {
	_enable = true;
	_call_every_handler->add(std::bind(&TeraRanger::cycle, this), 0.1f, &_timer_cookie);

	return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult wer::hal::TeraRanger::disable() {
	if(_enable) {
		_enable = false;
		_call_every_handler->remove(_timer_cookie);
	}

	return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult wer::hal::TeraRanger::probe()
{
	uint8_t who_am_i = 0;

	const uint8_t cmd = TERARANGER_WHO_AM_I_REG;

	if(_i2c->set(_address, cmd) == 0) {
		if(_i2c->get(_address, &who_am_i, 1) == 0 && who_am_i == TERARANGER_WHO_AM_I_REG_VAL) {
			if(measure()) {
				return wer::lib::ProcessResult::UNKNOWN;
			}
			else {
				return wer::lib::ProcessResult::SUCCESS;
			}
		}
	}

	LogWarn() << "WHO_AM_I byte mismatch " << unsigned(who_am_i) << " should be " << unsigned(TERARANGER_WHO_AM_I_REG_VAL);

	// not found on any address
	return wer::lib::ProcessResult::UNKNOWN;
}

int wer::hal::TeraRanger::measure()
{
	/*
	 * Send the command to begin a measurement.
	 */
	const uint8_t cmd = TERARANGER_MEASURE_REG;

	if(_i2c->set(_address, cmd)) {
		if(_sensor_ok == true) {
			_sensor_ok = false;
			LogErr() << "Terra ranger, measure error !";
		}
		
		_comms_errors++;
		return 1;
	}

	if(_sensor_ok == false) {
		_sensor_ok = true;
		LogInfo() << "Terra ranger, measure good !";
	}

	_collect_phase = true;

	return 0;
}

int
wer::hal::TeraRanger::collect()
{
	/* read from the sensor */
	uint8_t val[3] = {0, 0, 0};

	_collect_phase = false;

	if(_i2c->get(_address, val, 3)) {
		_comms_errors++;
		return 1;
	}

	uint16_t distance_mm = (val[0] << 8) | val[1];
	float distance_m = float(distance_mm) *  1e-3f;

	// This validation check can be used later
	_valid = crc8(val, 2) == val[2] && distance_m > _min_distance && distance_m < _max_distance;

	if(_valid) {
		_index = (_index + 1 ) % NUMBER_OF_STORED_VALUE;
		_distance[_index].distance = distance_m;
		_distance[_index].timestamp = std::chrono::steady_clock::now();
		return 0;
	}
	else {
		return 1;
	}
}

float wer::hal::TeraRanger::getMeasure() {
	auto dt = std::chrono::steady_clock::now() - _distance[_index].timestamp;
	if((std::chrono::duration_cast<std::chrono::milliseconds>(dt)).count() < 500)
		return _distance[_index].distance;
	else
		return std::numeric_limits<float>::quiet_NaN();
}

void
wer::hal::TeraRanger::cycle()
{
	/* collection phase? */
	if (_collect_phase) {
		collect();
		return;
	}
	else {
		measure();
		return;
	}
}
