#pragma once

#include <inttypes.h>

#include <map>
#include <mutex>
#include <atomic>
#include <thread>
#include <functional>

#include "../include/gpio.h"

namespace wer {
namespace hal {

    class GpioLinux : public Gpio {
    public:
        GpioLinux();
        ~GpioLinux();

        uint8_t init(IoPin gpio, GpioDirection direction);

        uint8_t digitalRead(IoPin gpio, uint8_t *value);
        uint8_t digitalWrite(IoPin gpio, uint8_t value);
        
        uint8_t registerCallback(IoPin gpio, GpioEvent event, std::function<void(IoPin, GpioEvent)> callback);

    private:

        struct GpioPin {
            std::function<void(IoPin, GpioEvent)> callback;
            GpioDirection direction;
            GpioEvent event;
            int fd;
            uint8_t lastValue;
        };

        std::map<IoPin, GpioPin> _pins;
        std::mutex _pinsMutex;

        std::thread* _thread;
        std::atomic<bool> _shouldExit;

        void task();

    };
}
}
