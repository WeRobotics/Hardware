#ifndef HARDWARE_DRIVERS_DYNAMIXEL_XM_LOOPED_STEPS
#define HARDWARE_DRIVERS_DYNAMIXEL_XM_LOOPED_STEPS

#include <sys/types.h>

#include <vector>

#include "dynamixel_xm_series.h"
#include "Library/log/log_manager.h"

namespace wer {
namespace hal {

class DynamixelXmLoopedSteps : public DynamixelXmSeries {
 public:
  DynamixelXmLoopedSteps(uint8_t id, const char* portName, uint32_t baudrate);

  // DynamixelMotor configuration
  void SetStepList(std::vector<int32_t> step_list, uint16_t nb_step,
                   uint16_t actual_step = 0);
  bool SetResetPosition();
  uint8_t UpdateActualStepToClosest(uint8_t direction, bool move = true,
                                    bool synchro = true);

  // Position control
  uint8_t SetPositionToStep(uint16_t step, bool synchro = true);
  uint8_t StepInc(bool synchro = true, uint16_t velocity = INVALID_SPEED);
  uint8_t StepDec(bool synchro = true, uint16_t velocity = INVALID_SPEED);

  // Getters
  uint16_t GetActualStep();
  int32_t GetResetPosition();

  // Checks
  uint8_t IsOnStep();

 private:
  int32_t reset_position_ = 0;
  uint16_t nb_step_;
  std::vector<int32_t> step_list_;
  uint16_t actual_step_;
};

}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_DRIVERS_DYNAMIXEL_XM_LOOPED_STEPS