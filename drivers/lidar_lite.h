#pragma once

#include <chrono>

#include <Library/timer/call_every_handler.h>

#include <Hardware/include/i2c.h>
#include <Hardware/include/distance_sensor.h>

namespace wer {
	namespace hal {

		/* Configuration Constants */
		static constexpr uint8_t LL40LS_BASEADDR =     			0x62; /* 7-bit address */
		static constexpr uint8_t LL40LS_BASEADDR_OLD =     		0x42; /* previous 7-bit address */

		/* Device limits */
		static constexpr float LL40LS_MAX_DISTANCE = 			40.0f;
		static constexpr float LL40LS_MIN_DISTANCE = 			0.10f;

		static constexpr uint8_t LL40LS_CONVERSION_INTERVAL = 	50;  		 /* Normal conversion wait time : 50ms */
		static constexpr uint8_t LL40LS_CONVERSION_TIMEOUT = 	100; 		 /* Maximum time to wait for a conversion to complete : 100ms */

		/* LL40LS Registers addresses */
		static constexpr uint8_t LL40LS_MEASURE_REG =      		0x00;        /* Measure range register */
		static constexpr uint8_t LL40LS_MSRREG_RESET =    		0x00;        /* reset to power on defaults */
		static constexpr uint8_t LL40LS_MSRREG_ACQUIRE =   		0x04;        /* Value to initiate a measurement, varies based on sensor revision */

		static constexpr uint8_t LL40LS_DISTHIGH_REG =     		0x0F;        /* High byte of distance register, auto increment */
		static constexpr uint8_t LL40LS_PEAK_CORR_REG =     	0x0C;        /* High byte of distance register, auto increment */
		static constexpr uint8_t LL40LS_AUTO_INCREMENT =   		0x80;
		static constexpr uint8_t LL40LS_HW_VERSION =       		0x41;
		static constexpr uint8_t LL40LS_SW_VERSION =       		0x4f;
		static constexpr uint8_t LL40LS_SIGNAL_STRENGTH_REG =  	0x5b;

		static constexpr uint8_t LL40LS_SIG_COUNT_VAL_REG =     0x02;        /* Maximum acquisition count register */
		static constexpr uint8_t LL40LS_SIG_COUNT_VAL_MAX =     0xFF;        /* Maximum acquisition count max value */

		static constexpr uint8_t LL40LS_ACQ_CONFIG_REG =		0x04;
		static constexpr uint8_t LL40LS_ACQ_MODE =				0x0C;
		static constexpr uint8_t LL40LS_REF_COUNT_VAL_REG =		0x12;
		static constexpr uint8_t LL40LS_REF_COUNT_APP =			0x05;

		class LidarLiteI2C: public DistanceSensor
		{
		public:
			LidarLiteI2C(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<I2c> i2c, int address = LL40LS_BASEADDR);
			~LidarLiteI2C();

			lib::ProcessResult init();
			lib::ProcessResult status() {return lib::ProcessResult::SUCCESS;};
			lib::ProcessResult enable();
			lib::ProcessResult disable();

			float getMeasure();

			void print_info();

		private:
			uint8_t	_address;
			float _min_distance;
			float _max_distance;

			struct distance_report_s _distance_report;

			bool _sensor_ok;
			bool _valid;
			bool _collect_phase;
			bool _enable;

			uint8_t _in_reset;
			uint64_t _sensor_resets;
			std::chrono::time_point<std::chrono::steady_clock> _reset_time;

			uint8_t _zero_counter;
			uint64_t _sensor_zero_resets;

			uint64_t _comms_errors;


			std::chrono::time_point<std::chrono::steady_clock> _acquire_time;

			std::shared_ptr<CallEveryHandler> _call_every_handler;
			void* _timer_cookie;

			std::shared_ptr<I2c> _i2c;

			lib::ProcessResult probe();
			void cycle();

			/**
			* Perform a poll cycle; collect from the previous measurement
			* and start a new one.
			*/
			void	measure();
			void	collect();

			//I2C finctions
			int read_reg(uint8_t reg, uint8_t &val);
			int write_reg(uint8_t reg, uint8_t val);
			int lidar_transfer(const uint8_t *send, unsigned send_len, uint8_t *recv, unsigned recv_len);

			//Sensor specific
			void reset_sensor();
		};
	}
}