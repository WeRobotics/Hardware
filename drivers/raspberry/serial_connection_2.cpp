#include "serial_connection_2.h"

#include <unistd.h>
#include <fcntl.h>
#include <asm/termbits.h>
#include <sys/ioctl.h>
#include <string.h>
//#include <termios.h>
#define SBUS_PACKET_SIZE 25

#include <Library/logging/log.h>

#define GET_ERROR() strerror(errno)

wer::hal::SerialConnection2::SerialConnection2(const std::string &path, int baudrate) :
    Connection(),
    _serial_node(path),
    _baudrate(baudrate)
{}

wer::hal::SerialConnection2::~SerialConnection2()
{
    // If no one explicitly called stop before, we should at least do it.
    stop();
}

wer::hal::ConnectionResult wer::hal::SerialConnection2::start()
{
    ConnectionResult ret = setupPort();
    if (ret != ConnectionResult::SUCCESS) {
        return ret;
    }

    startRecvThread();

    return ConnectionResult::SUCCESS;
}

wer::hal::ConnectionResult wer::hal::SerialConnection2::setupPort()
{
    _fd = open(_serial_node.c_str(), O_RDWR | O_NOCTTY);
    if (_fd == -1) {
        LogErr() << "open failed: " << GET_ERROR();
        return ConnectionResult::CONNECTION_ERROR;
    }

    struct termios2 tc;

	if (ioctl(_fd, TCGETS2, &tc) != 0)
	{
		LogErr() << "Could not get termios2 " << GET_ERROR();
		close(_fd);
		return ConnectionResult::CONNECTION_ERROR;
	}

    tc.c_cflag |= PARENB;
    tc.c_cflag |= CSTOPB;
    tc.c_cflag |= CS8;
    tc.c_cflag &= ~CRTSCTS;
    tc.c_cflag |= CREAD | CLOCAL;

    tc.c_lflag &= ~ICANON;
    tc.c_lflag &= ~ECHO;
    tc.c_lflag &= ~ECHOE;
    tc.c_lflag &= ~ECHONL;
    tc.c_lflag &= ~ISIG;

    tc.c_iflag &= ~(IXON | IXOFF | IXANY);
    tc.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

    tc.c_oflag &= ~OPOST;
    tc.c_oflag &= ~ONLCR;

    tc.c_cc[VTIME] = 0;
    tc.c_cc[VMIN] =  SBUS_PACKET_SIZE;

    tc.c_cflag &= ~CBAUD;
    tc.c_cflag |= BOTHER;
    tc.c_ispeed = tc.c_ospeed = _baudrate;

    if (ioctl(_fd, TCSETS2, &tc) != 0) {
        LogErr() << "Could not set terminal attributes " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

    return clear();
}

void wer::hal::SerialConnection2::startRecvThread()
{
    _recv_thread = new std::thread(&wer::hal::SerialConnection2::receive, this);
}

wer::hal::ConnectionResult wer::hal::SerialConnection2::stop()
{
    _should_exit = true;

    if (_recv_thread) {
        _recv_thread->join();
        delete _recv_thread;
        _recv_thread = nullptr;
    }

    close(_fd);

    return ConnectionResult::SUCCESS;
}

bool wer::hal::SerialConnection2::sendMessage(const uint8_t* data, uint16_t dataLength)
{
    if (_serial_node.empty()) {
        LogInfo() << "Dev Path unknown";
        return false;
    }

    if (_baudrate == 0) {
        LogInfo() << "Baudrate unknown";
        return false;
    }

    int send_len;
    send_len = write(_fd, data, dataLength);

    if (send_len != dataLength) {
        LogInfo() << "write failure: " << GET_ERROR();
        return false;
    }

    return true;
}

void wer::hal::SerialConnection2::receive()
{
    // Enough for MTU 1500 bytes.
    char buffer[2048];
    while (!_should_exit) {
        int recv_len;
        recv_len = read(_fd, buffer, sizeof(buffer));
        if (recv_len < -1) {
            LogErr() << "read failure: " << GET_ERROR();
        }
        else if (recv_len > static_cast<int>(sizeof(buffer)) || recv_len == 0) {
            continue;
        }

        receiveMessage((uint8_t*) buffer, recv_len);
    }
}

wer::hal::ConnectionResult wer::hal::SerialConnection2::clear() {
    if (ioctl(_fd, TCFLSH, TCIOFLUSH) == -1) {
        LogErr() << "Could not flush terminal " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

	return ConnectionResult::SUCCESS;
}