#include "serial_connection.h"

#include <unistd.h>
#include <fcntl.h>
//#include <asm/termbits.h>
#include <sys/ioctl.h>
#include <string.h>
#include <termios.h>

#include <Library/logging/log.h>

#define GET_ERROR() strerror(errno)

wer::hal::SerialConnection::SerialConnection(const std::string &path, int baudrate) :
    Connection(),
    _serial_node(path),
    _baudrate(baudrate)
{}

wer::hal::SerialConnection::~SerialConnection()
{
    // If no one explicitly called stop before, we should at least do it.
    stop();
}

wer::hal::ConnectionResult wer::hal::SerialConnection::start()
{
    ConnectionResult ret = setupPort();
    if (ret != ConnectionResult::SUCCESS) {
        return ret;
    }

    startRecvThread();

    return ConnectionResult::SUCCESS;
}

wer::hal::ConnectionResult wer::hal::SerialConnection::setupPort()
{
    _fd = open(_serial_node.c_str(), O_RDWR | O_NOCTTY);
    if (_fd == -1) {
        LogErr() << "open failed: " << GET_ERROR();
        return ConnectionResult::CONNECTION_ERROR;
    }

    struct termios tc;
    bzero(&tc, sizeof(tc));

    if (tcgetattr(_fd, &tc) != 0) {
        LogErr() << "Could not get termios2 " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

    tc.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    tc.c_oflag &= ~(OCRNL | ONLCR | ONLRET | ONOCR | OFILL | OPOST);
    tc.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG | TOSTOP);
    tc.c_cflag &= ~(CSIZE | PARENB | CRTSCTS);
    tc.c_cflag |= CS8;

    tc.c_cc[VMIN] = 0; // We want at least 1 byte to be available.
    tc.c_cc[VTIME] = 10; // We don't timeout but wait indefinitely.

    // CBAUD and BOTHER don't seem to be available for macOS with termios.
    tc.c_cflag &= ~(CBAUD);
    //tc.c_cflag |= BOTHER;

	tc.c_cflag |= CLOCAL;

	const int baudrate_const = define_from_baudrate(_baudrate);

	if (baudrate_const == -1) {
        return ConnectionResult::BAUDRATE_UNKNOWN;
    }

	if (cfsetispeed(&tc, baudrate_const) != 0) {
        LogErr() << "cfsetispeed failed: " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

	if (cfsetospeed(&tc, baudrate_const) != 0) {
        LogErr() << "cfsetospeed failed: " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

    if (tcsetattr(_fd, TCSANOW, &tc) != 0) {
        LogErr() << "Could not set terminal attributes " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

    if (ioctl(_fd, TCFLSH, TCIOFLUSH) == -1) {
        LogErr() << "Could not flush terminal " << GET_ERROR();
        close(_fd);
        return ConnectionResult::CONNECTION_ERROR;
    }

    return ConnectionResult::SUCCESS;
}

void wer::hal::SerialConnection::startRecvThread()
{
    _recv_thread = new std::thread(&wer::hal::SerialConnection::receive, this);
}

wer::hal::ConnectionResult wer::hal::SerialConnection::stop()
{
    _should_exit = true;

    if (_recv_thread) {
        _recv_thread->join();
        delete _recv_thread;
        _recv_thread = nullptr;
    }

    close(_fd);

    return ConnectionResult::SUCCESS;
}

bool wer::hal::SerialConnection::sendMessage(const uint8_t* data, uint16_t dataLength)
{
    if (_serial_node.empty()) {
        LogInfo() << "Dev Path unknown";
        return false;
    }

    if (_baudrate == 0) {
        LogInfo() << "Baudrate unknown";
        return false;
    }

    int send_len;
    send_len = write(_fd, data, dataLength);

    if (send_len != dataLength) {
        LogInfo() << "write failure: " << GET_ERROR();
        return false;
    }

    return true;
}

void wer::hal::SerialConnection::receive()
{
    // Enough for MTU 1500 bytes.
    char buffer[2048];
    while (!_should_exit) {
        int recv_len;
        recv_len = read(_fd, buffer, sizeof(buffer));
        if (recv_len < -1) {
            LogErr() << "read failure: " << GET_ERROR();
        }
        else if (recv_len > static_cast<int>(sizeof(buffer))) {
            continue;
        }
		else if (recv_len == 0) {
			if(_timeoutCallback) {
				_timeoutCallback();
			}
			continue;
		}

        receiveMessage((uint8_t*) buffer, recv_len);
    }
}

int wer::hal::SerialConnection::define_from_baudrate(int baudrate)
{
    switch (baudrate) {
        case 9600:
            return B9600;
        case 19200:
            return B19200;
        case 38400:
            return B38400;
        case 57600:
            return B57600;
        case 115200:
            return B115200;
        case 230400:
            return B230400;
        case 460800:
            return B460800;
        case 500000:
            return B500000;
        case 576000:
            return B576000;
        case 921600:
            return B921600;
        case 1000000:
            return B1000000;
        case 1152000:
            return B1152000;
        case 1500000:
            return B1500000;
        case 2000000:
            return B2000000;
        case 2500000:
            return B2500000;
        case 3000000:
            return B3000000;
        case 3500000:
            return B3500000;
        case 4000000:
            return B4000000;
        default: {
            LogErr() << "Unknown baudrate";
            return -1;
        }
    }
}