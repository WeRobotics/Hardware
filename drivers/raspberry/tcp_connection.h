#pragma once

#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>

#include <mutex>
#include <atomic>
#include <thread>


#include "../../include/connection.h"

namespace wer {
    namespace hal {

        class TcpConnection : public Connection {
        public:
            explicit TcpConnection(const std::string &remote_ip, int remote_port);
            ~TcpConnection();

            ConnectionResult start();
            ConnectionResult stop();

            bool sendMessage(const uint8_t* data, uint16_t dataLength);

            // Non-copyable
            TcpConnection(const TcpConnection &) = delete;
            const TcpConnection &operator=(const TcpConnection &) = delete;

        private:
            ConnectionResult setupPort();
            void startRecvThread();
            //int resolveAddress(const std::string &ip_address, int port, struct sockaddr_in *addr);
            void receive();
			void closeSocket();

            std::string _remote_ip = {};
            int _remote_port_number;

            std::mutex _mutex = {};
            int _socket_fd = -1;

            std::thread *_recv_thread{nullptr};
            std::atomic_bool _should_exit;
            std::atomic_bool _is_ok{false};
            bool _had_error;
			bool _had_connection;
        };
    }
}
