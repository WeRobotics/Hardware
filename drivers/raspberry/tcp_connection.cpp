#include "tcp_connection.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for close()
#include <string.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>

#include <cassert>

#include <Library/logging/log.h>

#define GET_ERROR(_x) strerror(_x)

/* change to remote_ip and remote_port */
wer::hal::TcpConnection::TcpConnection(const std::string &remote_ip, int remote_port) :
Connection(),
_remote_ip(remote_ip),
_remote_port_number(remote_port),
_socket_fd(-1),
_should_exit(false),
_had_error(false),
_had_connection(false)
{}

wer::hal::TcpConnection::~TcpConnection()
{
    // If no one explicitly called stop before, we should at least do it.
    stop();
}

wer::hal::ConnectionResult wer::hal::TcpConnection::start()
{
    ConnectionResult ret = setupPort();
    if (ret != ConnectionResult::SUCCESS) {
        _had_error = true;
    }

    startRecvThread();

    return ret;
}

wer::hal::ConnectionResult wer::hal::TcpConnection::setupPort()
{
	closeSocket();

    _socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (_socket_fd < 0) {
        LogErr() << "socket error" << GET_ERROR(errno);
        _is_ok = false;
        return ConnectionResult::SOCKET_ERROR;
    }

    struct sockaddr_in remote_addr {};
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(_remote_port_number);
    remote_addr.sin_addr.s_addr = inet_addr(_remote_ip.c_str());

    if (connect(_socket_fd,
                reinterpret_cast<sockaddr *>(&remote_addr),
                sizeof(struct sockaddr_in)) < 0) {
			LogErr() << "connect error: " << GET_ERROR(errno);
			_is_ok = false;
			return ConnectionResult::SOCKET_CONNECTION_ERROR;
    }

    _is_ok = true;
	_had_connection = true;
    return ConnectionResult::SUCCESS;
}

void wer::hal::TcpConnection::startRecvThread()
{
    _recv_thread = new std::thread(&wer::hal::TcpConnection::receive, this);
}

void wer::hal::TcpConnection::closeSocket() {
	if(_socket_fd >= 0) {
		// This should interrupt a recv/recvfrom call.
		shutdown(_socket_fd, SHUT_RDWR);

		// But on Mac, closing is also needed to stop blocking recv/recvfrom.
		close(_socket_fd);

		_socket_fd = -1;
	}
}

wer::hal::ConnectionResult wer::hal::TcpConnection::stop()
{
    _should_exit = true;

	closeSocket();

    if (_recv_thread) {
        _recv_thread->join();
        delete _recv_thread;
        _recv_thread = nullptr;
    }

    return ConnectionResult::SUCCESS;
}

bool wer::hal::TcpConnection::sendMessage(const uint8_t* data, uint16_t dataLength)
{
    if(_is_ok) {
        if (_remote_ip.empty()) {
            LogInfo() << "Remote IP unknown";
            return false;
        }

        if (_remote_port_number == 0) {
            LogInfo() << "Remote port unknown";
            return false;
        }

        struct sockaddr_in dest_addr {};
        dest_addr.sin_family = AF_INET;

        inet_pton(AF_INET, _remote_ip.c_str(), &dest_addr.sin_addr.s_addr);

        dest_addr.sin_port = htons(_remote_port_number);

        int send_len = sendto(_socket_fd,
                              reinterpret_cast<const char *>(data),
                              dataLength,
                              MSG_DONTWAIT,
                              reinterpret_cast<const sockaddr *>(&dest_addr),
                              sizeof(dest_addr));

        if (send_len != dataLength) {
            LogInfo() << "sendto failure: " << GET_ERROR(errno);
            _is_ok = false;
            return false;
        }
        return true;
    }
    else {
        //Avoid the logging
        return true;
    }
}

void wer::hal::TcpConnection::receive()
{
    // Enough for MTU 1500 bytes.
    char buffer[2048];

	struct pollfd poll_fds[1];
	int poll_timeout = 0;

    while (!_should_exit) {
        if (!_is_ok) {
            if(!_had_error) {
                LogErr() << "TCP receive error, trying to reconnect...";
                _had_error = true;
            }
			
			if(_had_connection) {
            	std::this_thread::sleep_for(std::chrono::seconds(5));
			}
			else {
				std::this_thread::sleep_for(std::chrono::seconds(30));
			}
            
            if(setupPort() != ConnectionResult::SUCCESS) {
                continue;
            }
            else {
                LogWarn() << "TCP link reconnected";
				poll_timeout = 0;
                _had_error = false;
            }
        }

		memset(poll_fds, 0 , sizeof(poll_fds));
		poll_fds[0].fd = _socket_fd;
		poll_fds[0].events = POLLIN;

		//Timeout after 10s
		int rc = poll(poll_fds, 1, 1000);

		if(_should_exit) {
			continue;
		}
	
		// Poll error
		if (rc < 0)
		{
			LogErr() << "poll failed : " << GET_ERROR(errno);
			_is_ok = false;
			continue;
		}
		else if (rc == 0)
		{
			//No data received after 10 timeouts try a reconnection
			poll_timeout++;
			if(poll_timeout > 30) {
				LogWarn() << "Poll timeout 30 times.";
				_is_ok = false;
				continue;
			}
			else {
				continue;
			}
		}
		else {
			poll_timeout = 0;

			if(poll_fds[0].revents & POLLIN) {

				int recv_len = recv(poll_fds[0].fd, buffer, sizeof(buffer), MSG_DONTWAIT);

				if (recv_len == 0) {
					// This can happen when shutdown is called on the socket,
					// therefore we check _should_exit again.
					_is_ok = false;
					continue;
				}
				else if (recv_len < 0) {
					// This happens on desctruction when close(_socket_fd) is called,
					// therefore be quiet.
					LogErr() << "recvfrom error: " << GET_ERROR(errno);
					// Something went wrong, we should try to re-connect in next iteration.
					_is_ok = false;
					continue;
				}
				else {
					Connection::receiveMessage((uint8_t*) buffer, recv_len);
				}
			}
			else {
				LogErr() << "Recv event is different of POLLIN : revents " << poll_fds[0].revents;
			}
		}


    }
}
