#pragma once

#include <mutex>
#include <atomic>
#include <thread>

#include "../../include/connection.h"

namespace wer {
    namespace hal {

        class SerialConnection2 : public Connection {
        public:
            explicit SerialConnection2(const std::string &path, int baudrate);
            ConnectionResult start();
            ConnectionResult stop();
            ~SerialConnection2();

            bool sendMessage(const uint8_t* data, uint16_t dataLength);
			ConnectionResult clear();

            // Non-copyable
            SerialConnection2(const SerialConnection2 &) = delete;
            const SerialConnection2 &operator=(const SerialConnection2 &) = delete;

        private:
            ConnectionResult setupPort();
            void startRecvThread();
            void receive();

            std::string _serial_node;
            int _baudrate;

            std::mutex _mutex = {};
            
            int _fd = -1;

            std::thread *_recv_thread = nullptr;
            std::atomic_bool _should_exit{false};
        };
    }
}