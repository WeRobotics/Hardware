#pragma once

#include <mutex>
#include <atomic>
#include <thread>

#include "../../include/connection.h"

namespace wer {
    namespace hal {

        class SerialConnection : public Connection {
        public:
            explicit SerialConnection(const std::string &path, int baudrate);
            ConnectionResult start();
            ConnectionResult stop();
            ~SerialConnection();

            bool sendMessage(const uint8_t* data, uint16_t dataLength);

            // Non-copyable
            SerialConnection(const SerialConnection &) = delete;
            const SerialConnection &operator=(const SerialConnection &) = delete;

        private:
            ConnectionResult setupPort();
            void startRecvThread();
            void receive();
			int define_from_baudrate(int baudrate);

            std::string _serial_node;
            int _baudrate;

            std::mutex _mutex = {};
            
            int _fd = -1;

            std::thread *_recv_thread = nullptr;
            std::atomic_bool _should_exit{false};
        };
    }
}