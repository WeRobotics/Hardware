#include "dynamixel_xm_series.h"

#include <bitset>
#include <chrono>
#include <thread>

namespace wer {
namespace hal {

// Control table address
#define ADDR_TORQUE_ENABLE 64
#define ADDR_GOAL_POSITION 116
#define ADDR_PRESENT_POSITION 132
#define ADDR_PRESENT_TEMPERATURE 146
#define ADDR_PRESENT_CURRENT 126
#define ADDR_CONTROL_MODE 11
#define ADDR_GOAL_SPEED 104
#define ADDR_PROFILE_SPEED 112
#define ADDR_HARDWARE_ERROR 70
#define ADDR_MOVING 122
#define ADDR_MOVING_COMPLETE 123
#define ADDR_HARDWARE_ERROR 70


#define TORQUE_ENABLE 1
#define TORQUE_DISABLE 0

#define PROTOCOL_VERSION 2.0

using std::chrono::milliseconds;
using std::this_thread::sleep_for;

DynamixelXmSeries::DynamixelXmSeries(uint8_t id, const char *portName,
                                     uint32_t baudrate) {
  id_ = id;
  control_mode_ = INVALID_CTRL_MODE;
  profile_velocity_ = 0;
  torque_mode_ = TORQUE_DISABLE;
  log_ = lib::LogManager::createLogMsg(
      true,
      "/home/pi/WMP-Belt/communication/javascript_webinterface/interface/logs/"
      "motor");

  // Initialize PortHandler instance
  port_handler_ = dynamixel::PortHandler::getPortHandler(portName);

  // Initialize PacketHandler instance
  packet_handler_ =
      dynamixel::PacketHandler::getPacketHandler(PROTOCOL_VERSION);

  log_->info_ << "motor Created" << '\n';

  // Open port
  if (port_handler_->openPort()) {
    log_->info_ << "Motor port correctly oppened." << '\n';
  } else {
    log_->error_ << "Error while oppening the motor port." << '\n';
  }

  // Set port baudrate
  if (port_handler_->setBaudRate(baudrate)) {
    log_->info_ << "Baudrate changed correctly." << '\n';
  } else {
    log_->error_ << "Error while changing the baudrate." << '\n';
  }
}

uint8_t DynamixelXmSeries::TorqueEnable() {
  log_->info_ << "Performing TorqueEnable" << '\n';

  uint8_t error_flag = false;
  uint8_t torque_mode = TORQUE_ENABLE;
  torque_mode_ = torque_mode;
  error_flag |= Write(ADDR_TORQUE_ENABLE, &torque_mode, 1);
  return error_flag;
}

uint8_t DynamixelXmSeries::TorqueDisable() {
  uint8_t error_flag = false;
  uint8_t torque_mode = TORQUE_DISABLE;
  torque_mode_ = torque_mode;
  error_flag |= Write(ADDR_TORQUE_ENABLE, &torque_mode, 1);
  return error_flag;
}

uint8_t DynamixelXmSeries::DxlErrorCheck(uint8_t dxl_error, int dxl_comm_result,
                                         uint8_t error_flag) {
  if (dxl_comm_result != COMM_SUCCESS) {
    log_->error_ << "Communication error during " << '\n';
    log_->error_ << packet_handler_->getTxRxResult(dxl_comm_result) << '\n';
    error_flag = true;
  } else if (dxl_error != 0) {
    log_->error_ << "Motor error during " << '\n';
    log_->error_ << packet_handler_->getRxPacketError(dxl_error) << '\n';
    log_->error_ << "Error ID: 0b" << std::bitset<8>(dxl_error).to_string()
                 << '\n';
    uint8_t hardware_error_status;
    dxl_comm_result = packet_handler_->read1ByteTxRx(
        port_handler_, id_, ADDR_HARDWARE_ERROR, &hardware_error_status,
        &dxl_error);
    if (dxl_comm_result != COMM_SUCCESS) {
      log_->error_ << "Hardware error ID: 0b"
                   << std::bitset<8>(hardware_error_status).to_string() << '\n';
    } else {
      log_->error_ << "Communication error to get harware error id" << '\n';
    }
    error_flag = true;
  }
  return error_flag;
}

uint8_t DynamixelXmSeries::Reboot() {
  uint8_t error_flag = false;
  log_->info_ << "Performing reboot" << '\n';

  if (port_handler_ && packet_handler_) {
    uint8_t dxl_error = 0;
    int dxl_comm_result = COMM_TX_FAIL;

    dxl_comm_result =
        packet_handler_->reboot(port_handler_, id_, &dxl_error);

    error_flag = DxlErrorCheck(dxl_error, dxl_comm_result, error_flag);
  } else {
    log_->error_ << "Object is badly implemented." << '\n';
    error_flag = true;
  }

  return error_flag;
}

uint8_t DynamixelXmSeries::Ping() {
  uint8_t error_flag = false;

  if (port_handler_ && packet_handler_) {
    uint8_t dxl_error = 0;
    int dxl_comm_result = COMM_TX_FAIL;

    dxl_comm_result = packet_handler_->ping(port_handler_, id_, &dxl_error);
    error_flag = DxlErrorCheck(dxl_error, dxl_comm_result, error_flag);

  } else {
    log_->error_ << "Object is badly implemented." << '\n';
    error_flag = true;
  }

  return error_flag;
}

bool DynamixelXmSeries::ControlModeCheck(uint8_t control_mode) {
  if (control_mode == VEL_CTRL_MODE || control_mode == POS_CTRL_MODE ||
      control_mode == EXT_POS_CTRL_MODE) {
    return true;
  }
  return false;
}

uint8_t DynamixelXmSeries::SetControlMode(uint8_t control_mode, uint8_t force) {
  uint8_t error_flag = 0;
  log_->info_ << "SetControlMode: " << (unsigned)control_mode << '\n';
  if (ControlModeCheck(control_mode) &&
      (control_mode != control_mode_ || force)) {
    control_mode_ = control_mode;

    error_flag |= TorqueDisable();

    error_flag |= Write(ADDR_CONTROL_MODE, (uint8_t *)&control_mode, 1);

    error_flag |= TorqueEnable();
  } else {
    log_->info_ << "ControlMode doesn't need to be changed" << '\n';
  }
  return error_flag;
}

uint8_t DynamixelXmSeries::SetProfileVelocity(uint16_t velocity) {
  log_->info_ << "DynamixelMotor speed set to: " << profile_velocity_ << '\n';
  if (velocity < INVALID_SPEED) {
    profile_velocity_ = velocity;
    int32_t speed_buf = velocity;
    log_->info_ << "DynamixelMotor speed set to: " << profile_velocity_ << '\n';
    return Write(ADDR_PROFILE_SPEED, (uint8_t *)&speed_buf, 4);
  }
  return 0;
}

// Method only working in EXT_POS_CTRL_MODE
uint8_t DynamixelXmSeries::ClearMultiTurn() {
  log_->info_ << "ClearMultiTurn" << '\n';
  uint8_t dxl_error = 0;
  int8_t attempts = 1;

  while (packet_handler_->clearMultiTurn(port_handler_, id_, &dxl_error) ==
             1 ||
         dxl_error == 1) {
    if (attempts < 6) {
      sleep_for(milliseconds(10));
      attempts++;
    } else {
      log_->error_ << "ClearMultiturn failed despite " << (unsigned)attempts
                   << " attempts !" << '\n';
      return true;
    }
  }

  return false;
}

uint8_t DynamixelXmSeries::SetVelocity(uint16_t velocity, uint8_t direction) {
  if (velocity < INVALID_SPEED && velocity > -INVALID_SPEED) {
    log_->info_ << "SetVelocity: velocity = " << velocity
                << " / direction = " << (unsigned)direction << '\n';

    int32_t speed_buf = velocity;
    if (direction == MOTOR_DIR_CW) {
      speed_buf = -speed_buf;
    }

    if (control_mode_ != VEL_CTRL_MODE) {
      SetControlMode(VEL_CTRL_MODE);
    }

    Write(ADDR_GOAL_SPEED, (uint8_t *)&speed_buf, 4);
  }
  return 0;
}

uint8_t DynamixelXmSeries::Stop() { return SetVelocity(0); }

uint8_t DynamixelXmSeries::SetPosition(int32_t goal, bool synchro,
                                       uint16_t velocity) {
  uint8_t error_flag = false;

  log_->info_ << "Starting SetPosition (synchro: " << synchro
              << "): goal = " << goal << '\n';

  if (control_mode_ != EXT_POS_CTRL_MODE) {
    error_flag |= SetControlMode(EXT_POS_CTRL_MODE);
    error_flag |= SetProfileVelocity(profile_velocity_);
  }

  if (velocity < INVALID_SPEED) {
    error_flag |= SetProfileVelocity(velocity);
  }

  error_flag |= Write(ADDR_GOAL_POSITION, (uint8_t *)&goal, 4);

  if (error_flag) {
    log_->error_ << "Error in SetPosition function: error_flag = "
                 << (unsigned)error_flag;
    return true;
  } else {
    if (synchro) {
      int32_t current_position;
      int8_t move_complete_check;

      if (GetPosition(&current_position) == true) {
        log_->error_ << "ERROR: unable to read position !" << '\n';
        return true;
      }
      int i = 0;
      GetMovingComplete(&move_complete_check);
      while (!move_complete_check) {
        if (GetPosition(&current_position) == true) {
          log_->error_ << "ERROR: unable to read position !" << '\n';
          return true;
        }
        sleep_for(milliseconds(200));

        GetMovingComplete(&move_complete_check);
        uint8_t moving_status;
        if ((!GetMovingStatus(&moving_status) && !moving_status)) {
          log_->info_ << "WARNING: unable to reach goal position of motor !"
                      << '\n';
          i++;
          if (i > 10) {
            log_->error_ << "ERROR: timeout to reach goal position of motor !"
                         << '\n';

            return true;
          }
        }
      }
      log_->info_ << "Finished SetPositionSynchro: current_position = "
                  << current_position << '\n';
      return false;
    }
    log_->info_ << "Finished SetPosition" << '\n';
    return false;
  }
}

uint8_t DynamixelXmSeries::GetPosition(int32_t *position) {
  uint8_t error_flag = true;

  error_flag = Read(ADDR_PRESENT_POSITION, (uint8_t *)position, 4);

  if (error_flag != false) {
    log_->error_ << "ERROR: unable to read position !" << '\n';
    return true;
  }
  return false;
}

uint8_t DynamixelXmSeries::GetTemperature(uint8_t *temperature) {
  uint8_t error_flag = true;

  error_flag = Read(ADDR_PRESENT_TEMPERATURE, (uint8_t *)temperature, 1);

  if (error_flag != false) {
    log_->error_ << "ERROR: unable to read temperature !" << '\n';
    return true;
  } else if (*temperature > 100 || *temperature < 0) {
    log_->info_ << "Warning: Temperature out-range !" << '\n';
  }
  return false;
}

uint8_t DynamixelXmSeries::GetCurrent(int16_t *current) {
  uint8_t error_flag = true;

  uint8_t dxl_error = 0;
  error_flag = Read(ADDR_PRESENT_CURRENT, (uint8_t *)current, 2);
  log_->info_ << " Current :" << (int)(*current) << '\n';
  if (error_flag != false || dxl_error != 0) {
    log_->error_ << "ERROR: unable to read Current !" << '\n';
    return true;
  }
  return false;
}

uint8_t DynamixelXmSeries::GetMovingStatus(uint8_t *moving_status) {
  uint8_t error_flag = true;

  error_flag = Read(ADDR_MOVING, (uint8_t *)moving_status, 1);

  if (error_flag != false) {
    log_->error_ << "ERROR: unable to read moving status !" << '\n';
    return true;
  }
  return false;
}

uint8_t DynamixelXmSeries::GetMovingComplete(int8_t *moving_complete) {
  uint8_t error_flag = true;

  uint8_t dxl_error = 0;
  error_flag = Read(ADDR_MOVING_COMPLETE, (uint8_t *)moving_complete, 1);
  *moving_complete &= 1;  // Check if "arrived" on MovingStatus(bit0) (doc)
  if (error_flag != false || dxl_error != 0) {
    log_->error_ << "ERROR: unable to read if moving is complete !" << '\n';
    return true;
  }
  return false;
}

uint8_t DynamixelXmSeries::Write(uint16_t address, uint8_t *data,
                                 uint16_t length) {
  int8_t attempts = 1;

  while (WriteByteTxRx(id_, address, data, length) == true) {
    if (attempts < 6) {
      sleep_for(milliseconds(10));
      attempts++;
    } else {
      log_->error_ << "write to motor failed despite " << (unsigned)attempts
                   << " attempts !" << '\n';
      return true;
    }
  }
  return false;
}

uint8_t DynamixelXmSeries::Read(uint16_t address, uint8_t *data,
                                uint16_t length) {
  int8_t attempts = 1;

  while (ReadByteTxRx(id_, address, data, length) == true) {
    if (attempts < 6) {
      sleep_for(milliseconds(10));
      attempts++;
    } else {
      log_->error_ << "read from motor failed despite " << (unsigned)attempts
                   << " attempts !" << '\n';
      return true;
    }
  }
  return false;
}

uint8_t DynamixelXmSeries::WriteByteTxRx(uint8_t id, uint16_t address,
                                         uint8_t *data, uint16_t length) {
  uint8_t error_flag = false;

  if (port_handler_ && packet_handler_) {
    uint8_t dxl_error = 0;
    int dxl_comm_result = COMM_TX_FAIL;

    if (length <= 4) {
      dxl_comm_result = packet_handler_->writeTxRx(
          port_handler_, id, address, length, data, &dxl_error);

      error_flag = DxlErrorCheck(dxl_error, dxl_comm_result, error_flag);
    }
    else {
      log_->error_ << "Unsuported data length." << '\n';
      error_flag = true;
    }

  } else {
    log_->error_ << "Object is badly implemented." << '\n';
    error_flag = true;
  }

  return error_flag;
}

uint8_t DynamixelXmSeries::ReadByteTxRx(uint8_t id, uint16_t address,
                                        uint8_t *data, uint16_t length) {
  uint8_t error_flag = false;

  if (port_handler_ && packet_handler_) {
    uint8_t dxl_error = 0;
    int dxl_comm_result = COMM_TX_FAIL;

    if (length <= 4) {
      dxl_comm_result = packet_handler_->readTxRx(port_handler_, id, address,
                                                     length, data, &dxl_error);

      error_flag = DxlErrorCheck(dxl_error, dxl_comm_result, error_flag);
    }

    else {
      log_->error_ << "Unsuported data length." << '\n';
      error_flag = true;
    }

  } else {
    log_->error_ << "Object is badly implemented." << '\n';
    error_flag = true;
  }

  return error_flag;
}

}  // namespace hal
}  // namespace wer
