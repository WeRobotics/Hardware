#ifndef HARDWARE_DRIVERS_DYNAMIXEL_XM_SERIES_H
#define HARDWARE_DRIVERS_DYNAMIXEL_XM_SERIES_H

#include <sys/types.h>

#include <dynamixel_sdk/dynamixel_sdk.h>

#include "Library/log/log_manager.h"

namespace wer {
namespace hal {

static const uint8_t VEL_CTRL_MODE = 1;
static const uint8_t POS_CTRL_MODE = 3;
static const uint8_t EXT_POS_CTRL_MODE = 4;
static const uint8_t INVALID_CTRL_MODE = 255;

static const uint8_t MOTOR_DIR_CW = 0;
static const uint8_t MOTOR_DIR_CCW = 1;

static const uint16_t INVALID_SPEED = 1024;

class DynamixelXmSeries {
 public:
  DynamixelXmSeries(uint8_t id, const char* portName, uint32_t baudrate);

  // No destructor: memory leak -> need to be solved by SDK

  // Active control of motor
  uint8_t TorqueEnable();
  uint8_t TorqueDisable();
  uint8_t Reboot();
  uint8_t Ping();

  // DynamixelMotor configuration
  uint8_t SetProfileVelocity(uint16_t velocity);

  // Speed control
  // Start turning at defined speed in defined direction
  uint8_t SetVelocity(uint16_t velocity, uint8_t direction = MOTOR_DIR_CCW);
  // Stop the moteur at current position, does not halt the position
  uint8_t Stop();

  // Position control
  // Go to Position at defined speed, joint mode
  uint8_t SetPosition(int32_t goal, bool synchro = true,
                      uint16_t velocity = INVALID_SPEED);

  // Getters
  uint8_t GetPosition(int32_t* position);
  uint8_t GetTemperature(uint8_t* temperature);
  uint8_t GetCurrent(int16_t* current);

  uint8_t ClearMultiTurn();

 protected:
  lib::LogMsg* log_;

  dynamixel::PortHandler* port_handler_;
  dynamixel::PacketHandler* packet_handler_;

  uint8_t id_;

  uint8_t control_mode_;
  uint8_t torque_mode_;

  uint16_t profile_velocity_;

  bool ControlModeCheck(uint8_t control_mode);

  uint8_t Write(uint16_t address, uint8_t* data, uint16_t length);
  uint8_t Read(uint16_t address, uint8_t* data, uint16_t length);

  // Return if the motor is moving or not
  uint8_t GetMovingStatus(uint8_t* movingStatus);
  // Return if the motor has complete its mission
  uint8_t GetMovingComplete(int8_t* moving_complete);

  uint8_t SetControlMode(uint8_t controlMode, uint8_t force = false);

 private:
  uint8_t WriteByteTxRx(uint8_t id, uint16_t address, uint8_t* data,
                        uint16_t length);
  uint8_t ReadByteTxRx(uint8_t id, uint16_t address, uint8_t* data,
                       uint16_t length);
  uint8_t DxlErrorCheck(uint8_t dxl_error, int dxl_comm_result,
                        uint8_t error_flag);
};

}  // namespace hal
}  // namespace wer

#endif  // HARDWARE_DRIVERS_DYNAMIXEL_XM_SERIES_H