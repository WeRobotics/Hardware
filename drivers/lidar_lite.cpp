#include "lidar_lite.h"

#include <cstring>
#include <iostream>
#include <limits>
#include <unistd.h>
#include <cmath>

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>

#define MEAS_IDX_PEAK_CORR 0
#define MEAS_IDX_NOISE_PEAK 1
#define MEAS_IDX_SIGNAL_STRENGTH 2
#define MEAS_IDX_DATA_HIGH 3
#define MEAS_IDX_DATA_LOW 4

#define NOISE_THRESHOLD 100

wer::hal::LidarLiteI2C::LidarLiteI2C(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, int address) :
	_address(address),
	_min_distance(LL40LS_MIN_DISTANCE),
	_max_distance(LL40LS_MAX_DISTANCE),
	_sensor_ok(false),
	_valid(true),
	_collect_phase(false),
	_enable(false),
	_in_reset(0),
	_sensor_resets(0),
	_zero_counter(0),
	_sensor_zero_resets(0),
	_comms_errors(0),
	_call_every_handler(call_every_handler),
	_i2c(i2c)
{
	_distance_report.distance = std::numeric_limits<float>::quiet_NaN();
	_distance_report.timestamp = std::chrono::steady_clock::now();
}

wer::hal::LidarLiteI2C::~LidarLiteI2C()
{
	disable();
	LogInfo() << "Lidar errors : " << unsigned(_comms_errors);
}

wer::lib::ProcessResult wer::hal::LidarLiteI2C::init()
{
	wer::lib::ProcessResult resultCode = probe();
    if(resultCode != wer::lib::ProcessResult::SUCCESS) {
    	return resultCode;
    }

    _sensor_ok = true;
    _collect_phase = false;

    std::chrono::time_point<std::chrono::steady_clock> start = std::chrono::steady_clock::now();

	enable();
    float dist;
    do {
		cycle();
		usleep(50000);
		cycle();
		dist = getMeasure();

		auto dt = std::chrono::steady_clock::now() - start;
		if((std::chrono::duration_cast<std::chrono::milliseconds>(dt)).count() > 5000) {
			//Timeout after 15 seconds
			disable();
			return wer::lib::ProcessResult::UNKNOWN;
		}

	} while(!std::isfinite(dist) || dist > 1.0f);
	disable();

	return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult wer::hal::LidarLiteI2C::enable() {
	_enable = true;
	_call_every_handler->add(std::bind(&LidarLiteI2C::cycle, this), 0.1f, &_timer_cookie);

	return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult wer::hal::LidarLiteI2C::disable() {
	if(_enable) {
		_enable = false;
		_call_every_handler->remove(_timer_cookie);
	}

	return wer::lib::ProcessResult::SUCCESS;
}

int wer::hal::LidarLiteI2C::read_reg(uint8_t reg, uint8_t &val)
{
	return lidar_transfer(&reg, 1, &val, 1);
}

int wer::hal::LidarLiteI2C::write_reg(uint8_t reg, uint8_t val)
{
	const uint8_t cmd[2] = { reg, val };
	return lidar_transfer(&cmd[0], 2, nullptr, 0);
}

int wer::hal::LidarLiteI2C::lidar_transfer(const uint8_t *send, unsigned send_len, uint8_t *recv, unsigned recv_len)
{
	if (send != nullptr && send_len > 0) {
		if(_i2c->set(_address, send, send_len)) {
			return 1;
		}
	}

	if (recv != nullptr && recv_len > 0) {
		return _i2c->get(_address, recv, recv_len);
	}

	return 0;
}

wer::lib::ProcessResult wer::hal::LidarLiteI2C::probe()
{
	// cope with both old and new I2C bus address
	const uint8_t addresses[2] = {LL40LS_BASEADDR, LL40LS_BASEADDR_OLD};
	uint8_t _hw_version, _sw_version;

	for (uint8_t i = 0; i < sizeof(addresses); i++) {
		_address = addresses[i];

		/*
		  check for hw and sw versions. It would be better if
		  we had a proper WHOAMI register
		 */
		if (read_reg(LL40LS_HW_VERSION, _hw_version) == 0 && _hw_version > 0 &&
		    read_reg(LL40LS_SW_VERSION, _sw_version) == 0 && _sw_version > 0) {

			//LogInfo() << "Lidar lite found with address " << unsigned(_address) << " hw " << unsigned(_hw_version)<< " sw " << unsigned(_sw_version);
			
			do {
				reset_sensor();
				usleep(100000);
			} while(_in_reset);

			if(_sensor_ok) {
				return wer::lib::ProcessResult::SUCCESS;
			}
			else {
				return wer::lib::ProcessResult::UNKNOWN;
			}
		}
	}

	LogErr() << "LidarLite not found.";

	return wer::lib::ProcessResult::NOT_CONNECTED;
}

void wer::hal::LidarLiteI2C::measure()
{
	/*
	 * Send the command to begin a measurement.
	 */
	const uint8_t cmd[2] = { LL40LS_MEASURE_REG, LL40LS_MSRREG_ACQUIRE };
	
	if(lidar_transfer(cmd, sizeof(cmd), nullptr, 0)) {
		LogErr() << "Sending measure command error.";
		_comms_errors++;

		// if we are getting lots of I2C transfer errors try
		// resetting the sensor
		if (_comms_errors % 10 == 0) {
			reset_sensor();
		}
	}

	// remember when we sent the acquire so we can know when the
	// acquisition has timed out
	_acquire_time = std::chrono::steady_clock::now();
	_collect_phase = true;
}

/*
  reset the sensor to power on defaults plus additional configurations
 */
void wer::hal::LidarLiteI2C::reset_sensor()
{
	switch(_in_reset) {
		case 0:
			LogWarn() << "Resetting the lidar lite.";

			write_reg(LL40LS_MEASURE_REG, LL40LS_MSRREG_RESET);
			_reset_time = std::chrono::steady_clock::now();
			_sensor_resets++;
			_in_reset = 1;
		break;
			
		case 1:
		{
			auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _reset_time).count();
			if(dt > 50) {
				if(write_reg(LL40LS_SIG_COUNT_VAL_REG, LL40LS_SIG_COUNT_VAL_MAX) == 0) {
					_in_reset = 0;
					_collect_phase = false;
					_sensor_ok = true;
					return;
				}
				else {
					LogWarn() << "Unable to apply SIG_COUNT_VAL parameter.";
				}
			}
			else {
				LogWarn() << "Wait no sufficient : " << unsigned(dt) << "ms";
			}

			LogErr() << "Unable to apply parameter after reset.";
			//Do multiple try ?
			_in_reset = 0;
			_sensor_ok = false;

		}
		break;

		default:
			_in_reset = 0;
			LogErr() << "Should never happen.";
		break;
	}
}

/*
  dump sensor registers for debugging
 */
/*
void wer::hal::LidarLiteI2C::print_registers()
{
	printf("ll40ls registers\n");
	// wait for a while to ensure the lidar is in a ready state
	usleep(50000);

	for (uint8_t reg = 0; reg <= 0x67; reg++) {
		uint8_t val = 0;
		int ret = lidar_transfer(&reg, 1, &val, 1);

		if (ret != OK) {
			printf("%02x:XX ", (unsigned)reg);

		} else {
			printf("%02x:%02x ", (unsigned)reg, (unsigned)val);
		}

		if (reg % 16 == 15) {
			printf("\n");
		}
	}

	printf("\n");
}
*/

void wer::hal::LidarLiteI2C::collect()
{
	/* read from the sensor */
	uint8_t val[5] = {0, 0, 0, 0, 0};

	// read the high and low byte distance registers
	uint8_t distance_reg = LL40LS_PEAK_CORR_REG | LL40LS_AUTO_INCREMENT;
	
	auto dt = std::chrono::steady_clock::now() - _acquire_time;

	if(lidar_transfer(&distance_reg, 1, val, 5)) {
		/*
			  NACKs from the sensor are expected when we
			  read before it is ready, so only consider it
			  an error if more than 100ms has elapsed.
		*/
		if((std::chrono::duration_cast<std::chrono::milliseconds>(dt)).count() > LL40LS_CONVERSION_TIMEOUT) {
			LogErr() << "Error reading sensor";
			_comms_errors++;

			if (_comms_errors % 10 == 0) {
				//reset_sensor();
				_sensor_ok = false;
			}
		}
	}
	else if(val[3] & 0x80) {
		// if the high bit of distance is set then the distance is invalid
		if(_valid) {
			LogErr() << "Invalid lidar reading.";
			_valid = false;
		}

		_collect_phase = false;
	}
	else {
		_valid = true;
		uint16_t distance_cm = (val[3] << 8) | val[4];
		float distance_m = float(distance_cm) * 1e-2f;

		if (distance_cm == 0) {
			_zero_counter++;

			if (_zero_counter == 20) {
				/* we have had 20 zeros in a row - reset the
				   sensor. This is a known bad state of the
				   sensor where it returns 16 bits of zero for
				   the distance with a trailing NACK, and
				   keeps doing that even when the target comes
				   into a valid range.
				*/
				_zero_counter = 0;

				_sensor_zero_resets++;

				LogWarn() << "Lidar bad state ?";


				_sensor_ok = false;
				//reset_sensor();
			}

		} else {
			_zero_counter = 0;
			_sensor_ok = true;
		}

		if(distance_m > LL40LS_MIN_DISTANCE) {

			if(val[MEAS_IDX_NOISE_PEAK] < NOISE_THRESHOLD && distance_m < LL40LS_MAX_DISTANCE) {
				_distance_report.distance = distance_m;
				_distance_report.timestamp = _acquire_time;

				//FlightLog::getInstance()->logLidar(distance_m);
			}

			LogLanding() << "LidarLite;0;" << distance_m << ";" 
						 << unsigned(val[MEAS_IDX_PEAK_CORR]) << ";" 
						 << unsigned(val[MEAS_IDX_NOISE_PEAK]) << ";" 
						 << unsigned(val[MEAS_IDX_SIGNAL_STRENGTH]);
		}

		_collect_phase = false;
	}
}

float wer::hal::LidarLiteI2C::getMeasure() {
	auto dt = std::chrono::steady_clock::now() - _distance_report.timestamp;
	if((std::chrono::duration_cast<std::chrono::milliseconds>(dt)).count() < 500 && _sensor_ok && _enable)
		return _distance_report.distance;
	else
		return std::numeric_limits<float>::quiet_NaN();
}

void wer::hal::LidarLiteI2C::cycle()
{
	if(_enable) {
		if(_in_reset) {
			reset_sensor();
		}
		else {
			/* collection phase? */
			if (_collect_phase) {
				collect();
				return;
			}
			else {
				measure();
				return;
			}
		}
	}
}

void wer::hal::LidarLiteI2C::print_info()
{
	LogInfo() << "Dist=" << getMeasure() << "m" 
			  << ", errors=" << unsigned(_comms_errors) 
			  << ", resets=" << unsigned(_sensor_resets) 
			  << ", zeros_reset=" << unsigned(_sensor_zero_resets) 
			  << ", valid=" << unsigned(_valid);
}
