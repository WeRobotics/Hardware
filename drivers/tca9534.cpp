#include "tca9534.h"

#define IO_MODE_REG_ADDR 0x03
#define IO_MODE_IN_ADDR 0x00
#define IO_MODE_OUT_ADDR 0x01

namespace wer {
namespace hal {

Tca9534::Tca9534(std::shared_ptr<I2c> i2c, uint8_t address)
    : pin_mode_(0xFF),
      pin_input_(0),
      pin_output_(0),
      pin_new_(0),
      i2c_(i2c),
      address_(address) {
  // Testing witout IO board connected
}

void Tca9534::send(uint8_t data, uint8_t registerAddr) {
  uint8_t buffer[2];
  buffer[0] = registerAddr;
  buffer[1] = data;

  i2c_->set(address_, buffer, 2);
}

uint8_t Tca9534::init(uint8_t pin, GpioMode mode) {
  if (pin < IO_NB_PIN) {
    if (mode == GpioMode::INPUT)
      pin_mode_ |= (1 << pin);
    else if (mode == GpioMode::OUTPUT)
      pin_mode_ &= (0 << pin);

    send(pin_mode_, IO_MODE_REG_ADDR);
  }
  return false;
}

uint8_t Tca9534::digitalWrite(uint8_t pin, uint8_t value) {
  if (pin < IO_NB_PIN) {
    if (value == 1)
      pin_output_ |= (1 << pin);
    else if (value == 0)
      pin_output_ &= ~(1 << pin);

    send(pin_output_, IO_MODE_OUT_ADDR);
  }
  return false;
}

uint8_t Tca9534::digitalRead(uint8_t pin, uint8_t* value) {
  // BOOST_LOG_SEV(_slg, debug) << "digitalRead called";
  if (pin < IO_NB_PIN) {
    uint8_t buffer[1] = {IO_MODE_IN_ADDR};

    i2c_->set(address_, buffer, 1);

    if (!(i2c_->get(address_, buffer, 1))) {
      pin_input_ = buffer[0];
      pin_new_ = 0xFF;
      *value = (pin_input_ & (1 << pin)) ? 1 : 0;
      return false;
    }
    return true;
  }
  return true;
}

uint8_t Tca9534::registerCallback(
    IoPin gpio, GpioEvent event,
    std::function<void(IoPin, GpioEvent)> callback) {
  return true;
}

}  // namespace hal
}  // namespace wer
