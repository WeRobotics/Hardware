#include "i2cLinux.h"

#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cstring>
#include <iostream>
#include <string>

namespace wer {
namespace hal {

I2cLinux::I2cLinux(const char* path)
    : _fd(0), _thread(nullptr), _shouldExit(false), _pendingSpurious(true) {
  _fdMutex.lock();
  _fd = open(path, O_RDWR);
  _fdMutex.unlock();

  _thread = new std::thread(&I2cLinux::task, this);
}

I2cLinux::~I2cLinux() {
  _shouldExit = true;
  _pendingCondition.notify_one();

  if (_thread->joinable()) {
    _thread->join();
    delete (_thread);
    _thread = nullptr;
  }

  if (_fd) {
    close(_fd);
    _fd = 0;
  }
}

uint8_t I2cLinux::set(uint8_t address, uint8_t data) {
  return set(address, &data, 1);
}

uint8_t I2cLinux::set(uint8_t address, const uint8_t* data,
                      uint8_t dataLength) {
  if (_fd) {
    std::lock_guard<std::mutex> lock(_fdMutex);

    if (ioctl(_fd, I2C_SLAVE, address) < 0) {
      return 3;
    }

    return write(_fd, data, dataLength) != dataLength;
  } else {
    return 2;
  }
}

uint8_t I2cLinux::get(uint8_t address, uint8_t* data, uint8_t dataLength) {
  if (_fd) {
    std::lock_guard<std::mutex> lock(_fdMutex);

    if (ioctl(_fd, I2C_SLAVE, address) < 0) {
      return 3;
    }

    return read(_fd, data, dataLength) != dataLength;
  } else {
    return 2;
  }
}

void I2cLinux::set(uint8_t address, uint8_t data,
                   std::function<void(uint8_t)> callback) {
  set(address, &data, 1, callback);
}

void I2cLinux::set(uint8_t address, const uint8_t* data, uint8_t dataLength,
                   std::function<void(uint8_t)> callback) {
  Tasks task;
  task.address = address;
  task.data = new uint8_t[dataLength];
  memcpy(task.data, data, dataLength);
  task.dataLength = dataLength;

  task.setCallback = callback;
  task.getCallback = nullptr;

  std::unique_lock<std::mutex> dataLocker(_tasksMutex);

  _tasks.push(task);

  _pendingSpurious = false;
  _pendingCondition.notify_one();
}

void I2cLinux::get(uint8_t address, uint8_t dataLength,
                   std::function<void(uint8_t, uint8_t*, uint8_t)> callback) {
  Tasks task;
  task.address = address;
  task.data = nullptr;
  task.dataLength = dataLength;

  task.setCallback = nullptr;
  task.getCallback = callback;

  std::unique_lock<std::mutex> dataLocker(_tasksMutex);

  _tasks.push(task);

  _pendingSpurious = false;
  _pendingCondition.notify_one();
}

void I2cLinux::task() {
  std::unique_lock<std::mutex> dataLocker(_tasksMutex);

  while (!_shouldExit) {
    // Wait for a notification (new data in the queue)
    while (_pendingSpurious && !_shouldExit) {
      _pendingCondition.wait(dataLocker);
    }

    if (_shouldExit) {
      break;
    }

    while (!_tasks.empty()) {
      // Get the next task
      Tasks task = _tasks.front();
      _tasks.pop();

      // Free the queue mutex to allow new task to be added while executing the
      // task
      dataLocker.unlock();

      // Execute the task
      if (task.data != nullptr) {
        // SET
        uint8_t status = set(task.address, task.data, task.dataLength);

        if (task.setCallback) {
          task.setCallback(status);
        }

        delete[] task.data;
      } else {
        // GET
        task.data = new uint8_t[task.dataLength];

        uint8_t status = get(task.address, task.data, task.dataLength);

        if (task.getCallback) {
          task.getCallback(status, task.data, task.dataLength);
        }

        delete[] task.data;
      }

      // Get the mutex to be sure no one is modifying the queue
      dataLocker.lock();
    }

    // There is no data in the queue and mutex is locked
    _pendingSpurious = true;
  }
}

}  // hal
}  // wer