#include <camera_mock.h>

#include <sys/statvfs.h>

#include <string>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include "opencv2/imgproc.hpp"

#include <hardware_status.h>

namespace wer {
namespace hal {
CameraMock* CameraMock::createCameraMock() { return new CameraMock(); }
CameraMock::CameraMock() {
  status_ = HardwareStatus::CONNECTED;
  camera_model_ = "Camera_mock";
}
CameraMock::~CameraMock() {}

uint8_t CameraMock::takePicture(cv::Mat& picture, uint8_t save_pictures = 0,
                                char* path = NULL) {
  if (status_ == HardwareStatus::CONNECTED) {
    if (save_pictures) {
      save_pictures_ = 1;
      path_ = path;
    }
    std::string picture_path;
    if (foreground_) {
      picture_path = "../../../mock/devices/RaspberryPi_camera_V2_full.bmp";
    } else {
      picture_path = "../../../mock/devices/RaspberryPi_camera_V2.bmp";
    }
    picture = cv::imread(picture_path, cv::IMREAD_GRAYSCALE);
    if (picture.rows == 0 || picture.cols == 0) {
      return 0x01;
    }
    if (save_pictures_) {
      struct statvfs buffer;
      int ret = statvfs(".", &buffer);
      double MB = 1024 * 1024;

      if (!ret) {
        const double available =
            (double)(buffer.f_bavail * buffer.f_bsize) / MB;
        if (available > 100)  // more than 100MB available
        {
          std::string full_name = std::string(path_) + "/" + camera_model_ +
                                  "_" + std::to_string(picture_number_) +
                                  ".jpg";
          imwrite(full_name, picture);
          picture_number_++;
        }
      }
    }
    return 0x00;
  } else {
    return 0x01;
  }
}

void CameraMock::SetForegroundPicture() { foreground_ = 1; }
void CameraMock::SetBackgroundPicture() { foreground_ = 0; }

}  // hal
}  // wer
