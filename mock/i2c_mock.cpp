#include <i2c_mock.h>

#include <iostream>

#include <log.h>
#include <yaml-cpp/yaml.h>

namespace wer {
namespace hal {

I2cMock::I2cMock(const char* path) {
  path_ = const_cast<char*>(path);
  Device_ = YAML::LoadFile(path);
}
uint8_t I2cMock::set(uint8_t address, uint8_t data) {
  return set(address, &data, 1);
}

uint8_t I2cMock::set(uint8_t address, const uint8_t* data, uint8_t dataLength) {
  std::string slave_address_string = Device_["slave adress"].as<std::string>();
  std::stringstream str;
  str << slave_address_string;
  unsigned int slave_address;
  str >> std::hex >> slave_address;
  if (slave_address != address) {
    return 0x01;
  } else {
    uint8_t* dataIter = const_cast<uint8_t*>(data);
    int register_address = (int)(*dataIter);
    ++dataIter;
    for (int i = register_address; i < register_address + dataLength - 1; ++i) {
      std::stringstream str_data;
      std::stringstream str_address_memory;
      str_data << "0x" << std::hex << (int)(*dataIter);
      str_address_memory << "0x" << std::hex << i;
      Device_["registers"][str_address_memory.str()] = str_data.str();
      ++dataIter;
    }
  }
  std::ofstream fout(path_);
  fout << Device_;
  return 0x00;
}
uint8_t I2cMock::get(uint8_t address, uint8_t* data, uint8_t dataLength) {
  std::string slave_address_string = Device_["slave adress"].as<std::string>();
  std::stringstream str;
  str << slave_address_string;
  unsigned int slave_address;
  str >> std::hex >> slave_address;
  if (slave_address != address) {
    return 0x01;
  } else {
    uint8_t data_address = Device_["data_registers_address"].as<int>();
    uint8_t* dataIter = data;
    for (int i = data_address; i < (data_address + dataLength); i++) {
      *dataIter = Device_["registers"][i].as<int>();
      ++dataIter;
    }
  }
  return 0x00;
}

void I2cMock::set(uint8_t address, uint8_t data,
                  std::function<void(uint8_t)> callback) {}
void I2cMock::set(uint8_t address, const uint8_t* data, uint8_t dataLength,
                  std::function<void(uint8_t)> callback) {}
void I2cMock::get(uint8_t address, uint8_t dataLength,
                  std::function<void(uint8_t, uint8_t*, uint8_t)> callback) {}
I2cMock::~I2cMock() {}

}  // hal
}  // wer