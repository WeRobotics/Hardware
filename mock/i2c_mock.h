#ifndef INCLUDE_MOCK_INC_I2C_MOCK_H
#define INCLUDE_MOCK_INC_I2C_MOCK_H

#include <i2c.h>

#include <atomic>
#include <condition_variable>
#include <fstream>

#include <yaml-cpp/yaml.h>

namespace wer {
namespace hal {
class I2cMock : public I2c {
 public:
  I2cMock(const char* path);
  ~I2cMock();
  uint8_t set(uint8_t address, uint8_t data);
  uint8_t set(uint8_t address, const uint8_t* data, uint8_t dataLength);
  uint8_t get(uint8_t address, uint8_t* data, uint8_t dataLength);

  void set(uint8_t address, uint8_t data,
           std::function<void(uint8_t)> callback);
  void set(uint8_t address, const uint8_t* data, uint8_t dataLength,
           std::function<void(uint8_t)> callback);
  void get(uint8_t address, uint8_t dataLength,
           std::function<void(uint8_t, uint8_t*, uint8_t)> callback);

 private:
  char* path_;
  YAML::Node Device_;
};

}  // hal
}  // wer

#endif  // INCLUDE_MOCK_INC_I2C_MOCK_H