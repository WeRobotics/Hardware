#ifndef HARDWARE_MOCK_INC_CAMERA_TEST_H
#define HARDWARE_MOCK_INC_CAMERA_TEST_H

#include <camera.h>

#include <opencv2/core.hpp>

namespace wer {
namespace hal {

class CameraMock : public Camera {
 public:
  static CameraMock* createCameraMock();
  ~CameraMock();

  uint8_t takePicture(cv::Mat& picture, uint8_t save_pictures,
                      char* path) override;
  void SetForegroundPicture();
  void SetBackgroundPicture();

  void setBrightness(uint8_t brightness) override {}

  void setContrast(uint8_t contrast) override {}

  void setISO(uint8_t ISO) override {}

  void setExposure(uint8_t exposure) override {}

  void setGrayscale() override {}

 private:
  CameraMock();

  uint8_t picture_number_ = 1;
  uint8_t foreground_ = 0;
};
}
}

#endif  // HARDWARE_MOCK_INC_CAMERA_TEST_H