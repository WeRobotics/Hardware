# Compiling and running tests

Test scripts are using GTest framework and libraries. Tests are done with gitlab-runner on the raspberry pi in the office. Before running any tests, there should be installed locally following libraries:\n
- yaml-cpp \n
- gtest \n
- gmock \n
- boost(only boost-system) \n
- open cv \n

1. Install gitlab-runner by following https://docs.gitlab.com/runner/install/linux-repository.html#prerequisites
2. Regsitering a runner: With a command: \n
gitlab-runner register \n
and with this explanation:
runner should be registered.
In the gitlab -> WeRobotics/Hardware/Settings/CI/CD there is a section called runners. There is a token for runner which should be copied and used with registration. In WeRobotics test CI/CD, runner is setup to be with a shell environement.
To run tests on gitlab-runner, gitlab-runner should be set and registered on the raspberry pi. After to run a gitlab-runner:\n
sudo gitlab-runner run \n

