#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>

#include <i2c.h>
#include <i2c_mock.h>
#include <servo_motor.h>
#include <servo_motor_arduino_i2c.h>

TEST(Output_unit_test, servo_motor) {
  const char* path = "../../../mock/devices/Servo_arduino.yaml";

  std::shared_ptr<wer::hal::I2c> i2c =
      std::make_shared<wer::hal::I2cMock>(path);

  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");

  uint8_t top_address =
      config["servo_arduino_heatlock"]["top_servo_address"].as<int>();

  uint8_t servo_arduino_address =
      config["servo_arduino_heatlock"]["slave_address"].as<int>();

  wer::hal::ServoMotor* servo_top = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, top_address, i2c);
  servo_top->setTargetAngle(30);

  uint8_t bottom_address =
      config["servo_arduino_heatlock"]["bottom_servo_address"].as<int>();
  wer::hal::ServoMotor* servo_bottom = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, bottom_address, i2c);
  servo_bottom->setTargetAngle(50);

  EXPECT_FALSE(false);
}