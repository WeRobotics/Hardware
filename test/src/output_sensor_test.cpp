#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <opencv2/core.hpp>

#include <camera.h>
#include <camera_mock.h>

#include <output_sensor.h>

TEST(Output_unit_test, output_sensor) {
  std::shared_ptr<wer::hal::CameraMock> pCameratest(
      wer::hal::CameraMock::createCameraMock());
  std::shared_ptr<wer::hal::Camera> pCamera = pCameratest;
  wer::hal::OutputSensor* output_sensor_test =
      wer::hal::OutputSensor::createOutputSensor(pCamera);
  pCameratest->SetForegroundPicture();
  output_sensor_test->foregroundProcessPicture();
  int data;
  output_sensor_test->getData(data);
  EXPECT_EQ(318, data);
}
