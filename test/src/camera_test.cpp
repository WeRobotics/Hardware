#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <opencv2/core.hpp>

#include <camera.h>
#include <camera_mock.h>

TEST(Camera_test, simple_camera) {
  wer::hal::CameraMock* pCameratest = wer::hal::CameraMock::createCameraMock();
  wer::hal::Camera* pCamera = pCameratest;
  cv::Mat dataPicture;
  char path_picture[] = "/home/vuk";
  pCameratest->SetBackgroundPicture();
  pCamera->takePicture(dataPicture, 1, path_picture);
  pCameratest->SetForegroundPicture();
  EXPECT_FALSE(pCamera->takePicture(dataPicture, 1, path_picture));
}
