#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>

#include <i2c.h>
#include <i2c_mock.h>
#include <vibration_motor.h>
#include <vibration_motor_arduino_i2c.h>

TEST(Output_unit_test, vibration_motor) {
  const char* path = "../../../mock/devices/Servo_arduino.yaml";
  std::shared_ptr<wer::hal::I2c> i2c =
      std::make_shared<wer::hal::I2cMock>(path);

  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");
  uint8_t arduino_vibration_motor_address =
      config["servo_arduino_heatlock"]["vibration_motor_address"].as<int>();
  uint8_t i2c_address =
      config["servo_arduino_heatlock"]["slave_address"].as<int>();
  wer::hal::VibrationMotor* vibration_motor =
      new wer::hal::VibrationMotorArduinoI2c(
          i2c_address, arduino_vibration_motor_address, i2c);
  vibration_motor->setVibration(0x01);
  EXPECT_FALSE(false);
}