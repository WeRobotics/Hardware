#include <unistd.h>

#include <iostream>
#include <string>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>

#include <canister_fill_sensor.h>
#include <canister_fill_sensor_calibration.h>
#include <i2c_mock.h>

TEST(Sensors_test, canister_fill) {
  const char* path = "../../../mock/devices/SHARP_Arduino.yaml";
  std::shared_ptr<wer::hal::I2cMock> i2c_Canister1Sensor =
      std::make_shared<wer::hal::I2cMock>(path);
  float dataCanister;

  wer::lib::Time testTime;
  std::shared_ptr<wer::lib::CallEveryHandler> pcallEveryHandler =
      std::make_shared<wer::lib::CallEveryHandler>(testTime);

  YAML::Node config = YAML::LoadFile("../../../config/hal.yaml");
  YAML::Node canister1_calibration = config["canister_fill_sensor"]["c1"];

  std::vector<double> CalibrationFactors1;

  CalibrationFactors1.push_back(
      std::stod(canister1_calibration["x0"].as<std::string>()));
  CalibrationFactors1.push_back(
      std::stod(canister1_calibration["x1"].as<std::string>()));

  YAML::Node config_address = YAML::LoadFile("../../../config/drivers.yaml");
  uint8_t address =
      config_address["sharp_arduino_1"]["slave_address"].as<int>();
  wer::hal::CanisterFillSensor* Canister1Sensor =
      wer::hal::CanisterFillSensor::createCanisterFillSensor(
          pcallEveryHandler, i2c_Canister1Sensor, address, CalibrationFactors1);

  for (int i = 0; i < 50000; i++) {
    pcallEveryHandler->run_once();
    usleep(20);
  }
  Canister1Sensor->getData(dataCanister);
  EXPECT_NEAR(0.87965, dataCanister, 0.01);
}