#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>

#include <hih6100.h>
#include <humidity_sensor_hih6100.h>
#include <i2c_mock.h>
#include <temperature_sensor_hih6100.h>

TEST(Sensors_test, humidity_temperature) {
  const char* path = "../../../mock/devices/HIH600.yaml";
  std::shared_ptr<wer::hal::I2cMock> pi2cHIH =
      std::make_shared<wer::hal::I2cMock>(path);
  float dataHum;
  float dataTemp;

  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");
  uint8_t address_hih6100 = config["hih6100_1"]["slave_address"].as<int>();
  wer::lib::Time testTime;
  std::shared_ptr<wer::lib::CallEveryHandler> pcallEveryHandler =
      std::make_shared<wer::lib::CallEveryHandler>(testTime);
  std::shared_ptr<wer::hal::HIH6100> pHIH6100 =
      std::make_shared<wer::hal::HIH6100>(pcallEveryHandler, pi2cHIH,
                                          address_hih6100);

  wer::hal::HumiditySensorHIH6100* Humidity_sensor =
      wer::hal::HumiditySensorHIH6100::createHumiditySensorHIH6100(pHIH6100,
                                                                   1.0f);

  wer::hal::TemperatureSensorHIH6100* Temperature_sensor =
      wer::hal::TemperatureSensorHIH6100::createTemperatureSensorHIH6100(
          pHIH6100, 1.0f);

  for (int i = 0; i < 50000; i++) {
    pcallEveryHandler->run_once();
    usleep(20);
  }
  Humidity_sensor->getData(dataHum);
  Temperature_sensor->getData(dataTemp);
  EXPECT_NEAR(67.9954, dataHum, 0.01);
  EXPECT_NEAR(20.4322, dataTemp, 0.01);
}