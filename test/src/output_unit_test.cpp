#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>
#include <boost/thread.hpp>
#include <opencv2/core.hpp>

#include <camera.h>
#include <camera_mock.h>
#include <door.h>
#include <heatlock.h>
#include <hindge_door.h>
#include <i2c_mock.h>
#include <output_unit.h>
#include <servo_motor.h>
#include <servo_motor_arduino_i2c.h>
#include <time_handler.h>
#include <two_door_heatlock.h>

TEST(Output_unit_test, output_unit) {
  const char* path = "../../../mock/devices/Servo_arduino.yaml";
  std::shared_ptr<wer::hal::I2c> i2c =
      std::make_shared<wer::hal::I2cMock>(path);

  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");
  uint8_t servo_arduino_address =
      config["servo_arduino_heatlock"]["slave_address"].as<int>();
  uint8_t top_address =
      config["servo_arduino_heatlock"]["top_servo_address"].as<int>();
  wer::hal::ServoMotor* servo_top = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, top_address, i2c);
  uint8_t bottom_address =
      config["servo_arduino_heatlock"]["bottom_servo_address"].as<int>();
  wer::hal::ServoMotor* servo_bottom = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, bottom_address, i2c);

  YAML::Node config_doors = YAML::LoadFile("../../../config/hal.yaml");
  config_doors = config_doors["hindge_door"];
  wer::hal::Door* topp = wer::hal::HindgeDoor::createHindgeDoor(
      servo_top, config_doors["angle_opened"].as<int>(),
      config_doors["angle_closed"].as<int>());
  wer::hal::Door* bottomp = wer::hal::HindgeDoor::createHindgeDoor(
      servo_bottom, config_doors["angle_opened"].as<int>(),
      config_doors["angle_closed"].as<int>());
  wer::hal::Heatlock* heatlock_testp =
      wer::hal::TwoDoorHeatlock::createTwoDoorHeatlock(
          topp, bottomp, config_doors["angle_closed"].as<int>());

  std::shared_ptr<wer::hal::CameraMock> pCameratest(
      wer::hal::CameraMock::createCameraMock());
  std::shared_ptr<wer::hal::Camera> pCamera = pCameratest;
  wer::hal::OutputSensor* output_sensor_testp =
      wer::hal::OutputSensor::createOutputSensor(pCamera);

  wer::hal::OutputUnit* output_unit_test =
      wer::hal::OutputUnit::createOutputUnit(heatlock_testp,
                                             output_sensor_testp);
  wer::hal::OutputUnitStatus status = output_unit_test->sendRequest();
  std::cout << wer::hal::OutputUnitStatusToString(status) << '\n';
  EXPECT_FALSE(output_unit_test->commandOpenUnit());
  boost::this_thread::sleep_for(boost::chrono::seconds(1));
  status = output_unit_test->sendRequest();
  std::cout << wer::hal::OutputUnitStatusToString(status) << '\n';
  EXPECT_FALSE(output_unit_test->commandLoadingFinished());
  boost::this_thread::sleep_for(boost::chrono::seconds(10));
  delete output_unit_test;
}