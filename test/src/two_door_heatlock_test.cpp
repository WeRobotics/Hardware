#include <unistd.h>

#include <iostream>

#include <gtest/gtest.h>
#include <yaml-cpp/yaml.h>

#include <door.h>
#include <hindge_door.h>
#include <i2c.h>
#include <i2c_mock.h>
#include <servo_motor.h>
#include <servo_motor_arduino_i2c.h>
#include <two_door_heatlock.h>

TEST(Output_unit_test, heatlock) {
  const char* path = "../../../mock/devices/Servo_arduino.yaml";
  std::shared_ptr<wer::hal::I2c> i2c =
      std::make_shared<wer::hal::I2cMock>(path);
  uint8_t address = 0x11;
  int closed_door_position_top = 120;
  int closed_door_position_bottom = 120;

  YAML::Node config = YAML::LoadFile("../../../config/drivers.yaml");
  uint8_t servo_arduino_address =
      config["servo_arduino_heatlock"]["slave_address"].as<int>();
  uint8_t top_address =
      config["servo_arduino_heatlock"]["top_servo_address"].as<int>();
  wer::hal::ServoMotor* servo_top = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, top_address, i2c);
  uint8_t bottom_address =
      config["servo_arduino_heatlock"]["bottom_servo_address"].as<int>();
  wer::hal::ServoMotor* servo_bottom = new wer::hal::ServoMotorArduinoI2c(
      servo_arduino_address, bottom_address, i2c);

  YAML::Node config_doors = YAML::LoadFile("../../../config/hal.yaml");
  config_doors = config_doors["hindge_door"];
  wer::hal::Door* topp = wer::hal::HindgeDoor::createHindgeDoor(
      servo_top, config_doors["angle_opened"].as<int>(),
      config_doors["angle_closed"].as<int>());
  wer::hal::Door* bottomp = wer::hal::HindgeDoor::createHindgeDoor(
      servo_bottom, config_doors["angle_opened"].as<int>(),
      config_doors["angle_closed"].as<int>());
  wer::hal::TwoDoorHeatlock* heatlock =
      wer::hal::TwoDoorHeatlock::createTwoDoorHeatlock(
          topp, bottomp, config_doors["angle_closed"].as<int>());
  heatlock->open();
  heatlock->close();
  heatlock->release();
  uint8_t data[2];
  i2c->get(address, data, 2);
  EXPECT_EQ(data[0], closed_door_position_top);
  EXPECT_EQ(data[1], closed_door_position_bottom);
}