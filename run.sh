if [ "$1" == "short" ]
then
	cd build
	echo "$(tput setaf 1)$(tput setab 7)Compiling $(tput sgr0)"
	make all
	echo "$(tput setaf 1)$(tput setab 7)Runing unit_tests $(tput sgr0)"
	./unit_tests
else
	echo "$(tput setaf 1)$(tput setab 7)Making Makefile $(tput sgr0)"
    rm -r build
    mkdir build
    cd build
    cmake ..
    echo "$(tput setaf 1)$(tput setab 7)Compiling $(tput sgr0)"
    make all
    echo "$(tput setaf 1)$(tput setab 7)Runing unit_tests $(tput sgr0)"
   ./unit_tests
fi